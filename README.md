

#Instruction: 

Please follow all bellow steps:

composer update

cp .env.example .env

rename .env.example with .env

configure database on .env file

create a new database with name orient_service

php artisan migrate

php artisan key:generate

php artisan db:seed --class=PermissionTableSeeder

php artisan db:seed --class=CreateAdminUserSeeder

php artisan db:seed --class=CountriesTableSeeder

php artisan db:seed --class=DivisionSeeder

php artisan db:seed --class=DistrictSeeder

php artisan db:seed --class=UpazilaSeeder

php artisan config:cache 

php artisan config:clear 

php artisan cache:clear 

php artisan view:clear

Execute: php artisan serve

default usename: admin@gmail.com/

password - 123456

