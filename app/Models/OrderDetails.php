<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
class OrderDetails extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $value = ['created_at', 'updated_at'];

    public function getCreatedAtAttribute($value)
    {
        $date = Carbon::parse($value);
        return $date->format('d-m-Y');
    }

    function order(){
        return $this->belongsTo(Order::class);
    }

    function category(){
        return $this->belongsTo(Category::class);
    }

}
