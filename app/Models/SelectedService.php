<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SelectedService extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function serviceinfo(){
        return $this->belongsTo(ServiceList::class,'service_list_id');
    }
}
