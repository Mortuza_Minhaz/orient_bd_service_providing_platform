<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Category extends Model
{
    use HasFactory;
    #protected $fillable = ['created_at'];
    protected $guarded = [];

    protected $appends = ['status_string','full_name'];
    protected $value = ['created_at', 'updated_at'];



    public function categories()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function childrenCategories()
    {
        return $this->hasMany(Category::class, 'parent_id')->with('categories');
    }

    public function parentCategory()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function subcategory(){

        return $this->hasMany('App\Models\Category', 'parent_id');

    }

    public function subsubcategory()
    {
        return $this->hasMany('App\Models\Category', 'parent_id')->with('subcategory');
    }

    public function categoryWisePrice(){

        return $this->hasMany('App\Models\CategoryWisePrice', 'category_id');

    }
    public static function tree() {

        return static::where('parent_id', 0)->get(); // or based on you question 0?

    }


    public function getStatusStringAttribute()
    {
        return $this->category_status?'Active':'Inactive';
    }

    public function getFullNameAttribute()
    {
        return ucfirst($this->name);
    }

    public function getCreatedAtAttribute($value)
    {
        $date = Carbon::parse($value);
        return $date->format('d-m-Y');
    }

}
