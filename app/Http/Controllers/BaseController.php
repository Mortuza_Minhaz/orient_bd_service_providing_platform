<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    public function uploadImage($image, $folder_name)
    {
        $name = time() . '-' . $image->getClientOriginalName();
        $image->move('admin/upload/' . $folder_name, $name);
        return '/admin/upload/' . $folder_name . '/' . $name;
    }
}
