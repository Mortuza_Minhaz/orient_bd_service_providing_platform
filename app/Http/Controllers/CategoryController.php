<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Utility\CategoryUtility;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active_category_list = Category::get();

        return view('backend/category/index', compact('active_category_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $active_category_list = Category::get();


        return view('backend/category/create', compact('active_category_list'));
    }

    public function subCategory(Request $request)
    {

        $parent_id = $request->item_id;
        $active_category_list = Category::where('category_status', 1)->where('parent_id', $parent_id)->get();
        return response()->json($active_category_list);
    }

    public function store(Request $request)
    {
        //echo '<pre>'; print_r($request->name); exit();

        $requestData = $request->all();

        if ($request->has('category_logo') && $request->file('category_logo')->isValid()) {
            $image = $request->file('category_logo');
            $folder_name = 'category_logo';
            $requestData['category_logo'] = uploadImage($image, $folder_name);
        }

        if ($request->has('category_banner_image') && $request->file('category_banner_image')->isValid()) {
            $image = $request->file('category_banner_image');
            $folder_name = 'category_banner_image';
            $requestData['category_banner_image'] = uploadImage($image, $folder_name);
        }

        if ($requestData['parent_id'] != "0") {
            $requestData['parent_id'] = $request->parent_id;

            $parent = Category::find($request->parent_id);
            $requestData['level'] = $parent->level + 1 ;
        }

        $requestData['created_by'] = Auth::id();
        $requestData['slug'] = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->name)).'-'.Str::random(5);


        if (Category::create($requestData)) {
            #return redirect()->route('create_category')->with('success', 'Successfully Added');
            return redirect()->back()->with('success', 'Successfully Added');
        } else {
            #return redirect()->route('create_category')->with('failed', 'Something went wrong. Please try again!');
        }
    }


    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories = Category::where('parent_id', 0)
            ->with('childrenCategories')
            ->whereNotIn('id', CategoryUtility::children_ids($category->id, true))->where('id', '!=' , $category->id)
            ->orderBy('name','asc')
            ->get();

        return view('backend/category/edit', compact('category', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
       //echo '<pre>'; print_r($request->all()); exit();
        $requestData = $request->all();

        $new_logo = $request->file('category_logo');

        if (isset($new_logo)) {

            $old_logo = public_path($category->category_logo);

            if (is_file($old_logo)) {
            unlink($old_logo);
            }

            $image = $request->file('category_logo');
            $folder_name = 'category_logo';
            $requestData['category_logo'] = uploadImage($image, $folder_name);
        }

        $new_banner = $request->file('category_banner_image');

        if (isset($new_banner)) {

            $old_banner = public_path($category->category_banner_image);

            if (is_file($old_banner)) {
            unlink($old_banner);
            }

            $image = $request->file('category_banner_image');
            $folder_name = 'category_banner_image';
            $requestData['category_banner_image'] = uploadImage($image, $folder_name);
        }

        $previous_level = $category->level;

        if ($requestData['parent_id'] != "0") {
            $requestData['parent_id'] = $request->parent_id;

            $parent = Category::find($request->parent_id);
            $requestData['level'] = $parent->level + 1 ;
        }
        else{
            $requestData['parent_id'] = 0;
            $requestData['level'] = 0;
        }

        if($requestData['level'] > $previous_level){
            CategoryUtility::move_level_down($category->id);
        }
        elseif ($requestData['level'] < $previous_level) {
            CategoryUtility::move_level_up($category->id);
        }

        if ($request->slug != null) {
            $requestData['slug'] = strtolower($request->slug);
        }
        else {
            $requestData['slug'] = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->name)).'-'.Str::random(5);
        }

        if($category->update($requestData)) {
            return redirect()->back()->with('success', 'Successfully updated');
        } else {
            return redirect()->back()->with('failed', 'Something went wrong. Please try again!');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {

        if ($category->delete()) {
            return redirect()->back()->with('success', 'Successfully Deleted');
        } else {
            return redirect()->back()->with('failed', 'Something went wrong. Please try again!');
        }
    }

    //for catgory popular status change purpose function
    public function popularStatus($id){
        $cateory = Category::find($id);
        if($cateory->is_popular == 1){
            $cateory->is_popular = '0';
            $cateory->update();
        }
        else{
            $cateory->is_popular = '1';
            $cateory->update();
        }
        return true;
    }

}
