<?php

namespace App\Http\Controllers;

use App\Models\Minhaz;
use Illuminate\Http\Request;

class MinhazController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pendingList()
    {
        return view('pages/backend/order/pending_order_list');
    }

    public function assignedList()
    {
        return view('pages/backend/order/assigned_order_list');
    }

    public function completedList()
    {
        return view('pages/backend/order/completed_order_list');
    }

    public function cancelList()
    {
        return view('pages/backend/order/cancel_order_list');
    }

    public function assignServiceProvider()
    {
        return view('pages/backend/order/assign_service_provider');
    }

    public function orderDetails()
    {
        return view('pages/backend/order/order_details');
    }

    public function serviceProviderList()
    {
        return view('pages/backend/service_provider/service_provider_list');
    }

    public function serviceProviderProfile()
    {
        return view('pages/backend/service_provider/service_provider_profile');
    }

    public function inHouseEngineerList()
    {
        return view('pages/backend/service_provider/inHouse_engineer_list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Minhaz  $minhaz
     * @return \Illuminate\Http\Response
     */
    public function show(Minhaz $minhaz)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Minhaz  $minhaz
     * @return \Illuminate\Http\Response
     */
    public function edit(Minhaz $minhaz)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Minhaz  $minhaz
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Minhaz $minhaz)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Minhaz  $minhaz
     * @return \Illuminate\Http\Response
     */
    public function destroy(Minhaz $minhaz)
    {
        //
    }
}
