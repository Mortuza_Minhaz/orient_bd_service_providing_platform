<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Validator;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brand_list = Brand::all();

        return view('backend/brand/index', compact('brand_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend/brand/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        if ($request->has('logo') && $request->file('logo')->isValid()) {
            $image = $request->file('logo');
            $folder_name = 'brand_logo';
            $requestData['logo'] = uploadImage($image, $folder_name);
        }

        $requestData['slug'] = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->name)).'-'.Str::random(5);

        if (Brand::create($requestData)) {
            return redirect()->back()->with('success', 'Successfully Added');
        } else {
            return redirect()->back()->with('failed', 'Something went wrong. Please try again!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        $requestData = $request->all();

        $new_image = $request->file('logo');

        if (isset($new_image)) {

            $old_image = public_path($brand->logo);

            if (is_file($old_image)) {
            unlink($old_image);
            }

            $image = $request->file('logo');
            $folder_name = 'brand_logo';
            $requestData['logo'] = uploadImage($image, $folder_name);
        }

        if ($request->slug != null) {
            $requestData['slug'] = strtolower($request->slug);
        }
        else {
            $requestData['slug'] = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->name)).'-'.Str::random(5);
        }


        if($brand->update($requestData)) {
            return redirect()->back()->with('success', 'Successfully updated');
        } else {
            return redirect()->back()->with('failed', 'Something went wrong. Please try again!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $requestData = Brand::find($id);

        if ($requestData->delete()) {
            return redirect()->back()->with('success', 'Successfully Deleted');
        } else {
            return redirect()->back()->with('failed', 'Something went wrong. Please try again!');
        }
    }
}
