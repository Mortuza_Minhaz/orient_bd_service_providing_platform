<?php

namespace App\Http\Controllers;
use App\Models\Warranty;
use Validator;

use Illuminate\Http\Request;

class WarrantyController extends Controller
{
    //for warranty create and store
    public function index(Request $request){

        if ($request->isMethod('POST')) {

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'mobile' => 'required',
                'invoice_num' => 'required',
                'serial_no' => 'required',
                'purchase_date' => 'required',
                'warranty_for' => 'required',
            ]);

            if ($validator->fails())
            {
                return redirect()->back()->with('failed','Required field need to fill up !!!');
            }else{
                if(Warranty::create($request->all())){
                    return redirect()->back()->with('success','Successfully Added !!!');
                }
                else{
                    return redirect()->back()->with('failed','Your information saved failed !!!');
                }
        }
    }

        return view('frontend/warranty/create');

    }

    public function show(){

        $warranty_info=Warranty::all();
        return view('backend/warranty/index',compact('warranty_info'));

    }

    public function delete(Warranty $id){

        $id->delete();
        return redirect()->back()->with('success', 'Successfully Delete');

    }

}
