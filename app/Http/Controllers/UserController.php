<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Customer;
use App\Models\ServiceProvider;
use App\Models\Address;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use Illuminate\Support\Arr;
use Carbon\Carbon;
use DataTables;
use Cart;
use Illuminate\Support\Facades\Validator;
use Brian2694\Toastr\Facades\Toastr;


use Illuminate\Support\Facades\URL;


class UserController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_1(Request $request)
    {
        $data = User::orderBy('id', 'DESC')->paginate(5);
        return view('users.index', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function index(Request $request)
    {
        $basenameUrl = basename(URL::current());

        if ($request->ajax()) {
            $users = User::latest()->get();

            /*Log::info($data);*/
            /*Log::info(json_encode($data);*/


            return Datatables::of($users)
                ->addIndexColumn()
                ->editColumn('role', function ($users) {
                    return ((!empty($users->getRoleNames()->first())) ? '<label class="badge badge-success">' . $users->getRoleNames()->first() . '</label>' : "NO User");
                })
                ->rawColumns(['role'])
                ->editColumn('created_at', function ($users) {
                    return Carbon::createFromFormat('Y-m-d H:i:s', $users->created_at)->diffForHumans();
                })
                ->rawColumns(['created_at'])
                ->editColumn('active', function ($users) {
                    return '<i class="fa fa-' . ($users->active ? 'check' : 'times') . '" aria-hidden="true"></i>';
                })
                ->rawColumns(['active'])
                ->addColumn('action', function ($users) {
                    $btn = '';

                    $btn .= '<a target="_blank" href="' . route('users.edit', $users->id) . '" class=" btn btn-primary ">Edit</a>';

                    $btn .= '<a href="" class=" btn btn-danger ">Delete</a>';


                    return $btn;
                })
                ->rawColumns(['action'])
                ->escapeColumns([])
                ->make(true);
        }

        return view('users.index', compact('basenameUrl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'name')->all();
        return view('users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);
        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')
            ->with('success', 'User created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $address = Address::where('user_id', $id)->get();
        $roles = Role::pluck('name', 'name')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();

        return view('users.edit', compact('user', 'roles', 'userRole', 'address'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }

        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id', $id)->delete();

        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')
            ->with('success', 'User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
            ->with('success', 'User deleted successfully');
    }

    //for customer login purpose function
    public function customer_login(Request $request)
    {

        if ($request->isMethod('POST')) {
            $credentials = $request->only('email', 'password');
            if (Auth::attempt($credentials)) {
                // Authentication passed...
                if (Cart::count() > 0) {
                    return redirect()->route('checkout');
                } else {
                    return redirect()->intended('customer');
                }
            }
        }
        return view('frontend/customer/login');
    }

    //for cnew customer registration purpose function
    public function customer_register_1(Request $request)
    {

        $new_customer = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('/');
        }


    }

    public function customer_register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required|unique:users',
            'password' => ['required', 'min:6', 'confirmed'],
        ]);

        if ($validator->fails()) {
            // Session::flash('error', $validator->messages()->first());
            //Toastr::error($validator->messages()->first(), 'Error', ["closeButton" => true, "debug" => false, "newestOnTop" => false, "progressBar" => true, "positionClass" => "toast-bottom-left", "preventDuplicates" => false, "onclick" => null, "showDuration" => "600", "hideDuration" => "1000", "timeOut" => "5000", "extendedTimeOut" => "1000", "showEasing" => "swing", "hideEasing" => "linear", "showMethod" => "fadeIn", "hideMethod" => "fadeOut"]);
            // Toastr::error('try again', 'Error');
            return redirect()->back()->with('failed', $validator->messages()->first());
            // return redirect()->back()->withInput();

            // return response()->json(array('errors' => $validator->getMessageBag()->first()));

            // return response()->json(array('errors' => $validator->getMessageBag()->toArray()));

            // return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }


        $input = $request->all();
        // echo '<pre>'; print_r($input); exit();
        $input['password'] = Hash::make($input['password']);
        $input['phone'] = $request->input('phone');
        $roles_name = 'customer';
        $input['user_type'] = $roles_name;


        DB::beginTransaction();
        try {
            $user = User::create($input);
            $user->assignRole($roles_name);

            if ($roles_name == "customer") {
                $requestData['user_id'] = $user->id;

                Customer::create($requestData);
            }



            DB::commit();
            $successOrFailed = 'success';
        } catch (\Exception $e) {
            $successOrFailed = 'failed';
            DB::rollBack();
        }

        if ($successOrFailed == 'success') {

            $credentials = $request->only('phone', 'password');
            if (Auth::attempt($credentials)) {
                // Authentication passed...
                return redirect()->route('homeView')->with($successOrFailed, 'welcome to my channel');
            }

        }else{
            return redirect()->route('homeView')->with('failed', 'please try again  ');
        }


    }
    public function service_provider_register(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'provider_type' => 'required',
            'name' => 'required',
            'phone' => 'required|unique:users',
            'password' => ['required', 'min:6', 'confirmed'],
        ]);

        if ($validator->fails()) {
            // Session::flash('error', $validator->messages()->first());
            //Toastr::error($validator->messages()->first(), 'Error', ["closeButton" => true, "debug" => false, "newestOnTop" => false, "progressBar" => true, "positionClass" => "toast-bottom-left", "preventDuplicates" => false, "onclick" => null, "showDuration" => "600", "hideDuration" => "1000", "timeOut" => "5000", "extendedTimeOut" => "1000", "showEasing" => "swing", "hideEasing" => "linear", "showMethod" => "fadeIn", "hideMethod" => "fadeOut"]);
            // Toastr::error('try again', 'Error');
            return redirect()->back()->with('failed', $validator->messages()->first());
            // return redirect()->back()->withInput();

            // return response()->json(array('errors' => $validator->getMessageBag()->first()));

            // return response()->json(array('errors' => $validator->getMessageBag()->toArray()));

            // return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }


        $input = $request->all();
       $provider_type =$request->input('provider_type');
        // echo '<pre>'; print_r($input); exit();
        $input['password'] = Hash::make($input['password']);
        $input['email'] = $request->input('email');
        $input['phone'] = $request->input('phone');
        $roles_name = $provider_type;
        $input['user_type'] = $roles_name;


        DB::beginTransaction();
        try {
            $user = User::create($input);
            $user->assignRole($roles_name);


                $requestData['user_id'] = $user->id;

                ServiceProvider::create($requestData);




            DB::commit();
            $successOrFailed = 'success';
        } catch (\Exception $e) {
            $successOrFailed = 'failed';
            DB::rollBack();
        }

        if ($successOrFailed == 'success') {

            $credentials = $request->only('phone', 'password');
            if (Auth::attempt($credentials)) {
                // Authentication passed...
                return redirect()->route('homeView')->with($successOrFailed, 'welcome to my channel');
            }

        }else{
            return redirect()->route('homeView')->with('failed', 'please try again  ');
        }


    }

    //customer logout purpose function
    public function customer_logout()
    {

        Auth::logout();
        return redirect()->intended('customer_login');
    }
}
