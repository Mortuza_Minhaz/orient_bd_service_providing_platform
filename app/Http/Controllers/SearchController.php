<?php

namespace App\Http\Controllers;
use App\Models\Category;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function autocomplete(Request $request)
    {
        $data = Category::select('name as label', 'id as value', 'category_description as desc')
            ->where("name", "LIKE", "%{$request->get('term')}%")
            ->get();


        return response()->json($data);
    }

    public function search(Request $request)
    {
        $search = $request->get('term');

        //$result = User::select('id', 'name')->where('name', 'LIKE', '%' . $search . '%')->get();
        $result = Category::select('id', 'name')->where('name', 'LIKE', '%' . $search . '%')->get();


        $response = array();
        foreach ($result as $employee) {
            $response[] = array("value" => $employee->id, "label" => $employee->name, "category_id" => $employee->category_id);
        }

        echo json_encode($response);
        exit;


    }

}
