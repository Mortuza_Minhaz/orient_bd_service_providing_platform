<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\SubSubCategory;
use Schema;
use App\Models\Product;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::orderBy('id','desc')->get();
        return view('backend.coupons.index', compact('coupons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.coupons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(count(Coupon::where('code', $request->coupon_code)->get()) > 0){
            
            return redirect()->route('coupon.index')->with('failed', 'Coupon already exist for this coupon code!');
        }
        $coupon = new Coupon;
          if ($request->coupon_type == "category_base") {
              $coupon->type = $request->coupon_type;
              $coupon->code = $request->coupon_code;
              $coupon->discount = $request->discount;
              $coupon->discount_type = $request->discount_type;
              $date_var                 = explode(" - ", $request->date_range);
              $coupon->start_date       = strtotime($date_var[0]);
              $coupon->end_date         = strtotime( $date_var[1]);
              $cupon_details = array();
              foreach($request->product_ids as $product_id) {
                  $data['category_id'] = $product_id;
                  array_push($cupon_details, $data);
              }
              $coupon->details = json_encode($cupon_details);
              if ($coupon->save()) {
                  return redirect()->route('coupon.index')->with('success','Coupon has been saved successfully');
              }
              else{
                return redirect()->route('coupon.index')->with('failed', 'Something went wrong. Please try again!');
              }
          }
          elseif ($request->coupon_type == "cart_base") {
              $coupon->type             = $request->coupon_type;
              $coupon->code             = $request->coupon_code;
              $coupon->discount         = $request->discount;
              $coupon->discount_type    = $request->discount_type;
              $date_var                 = explode(" - ", $request->date_range);
              $coupon->start_date       = strtotime($date_var[0]);
              $coupon->end_date         = strtotime($date_var[1]);
              $data                     = array();
              $data['min_buy']          = $request->min_buy;
              $data['max_discount']     = $request->max_discount;
              $coupon->details          = json_encode($data);
              if ($coupon->save()) {
                  
                  return redirect()->route('coupon.index')->with('success','coupon Insert successfully');
              }
              else{
                  return redirect()->route('coupon.index')->with('failed', 'Something went wrong. Please try again!');
              }
          }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        $coupon = Coupon::find($coupon);
        // echo "<pre>";
        // print_r($coupon);
        // exit;
        return view('backend.coupons.edit', compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon)
    {
        if(count(Coupon::where('id', '!=' , $coupon)->where('code', $request->coupon_code)->get()) > 0){
            return redirect()->route('coupon.index')->with('failed', 'Coupon already exist for this coupon code!');
        }
  
        $coupon = Coupon::find($coupon);
          if ($request->coupon_type == "product_base") {
              $coupon->type = $request->coupon_type;
              $coupon->code = $request->coupon_code;
              $coupon->discount = $request->discount;
              $coupon->discount_type  = $request->discount_type;
              $date_var                 = explode(" - ", $request->date_range);
              $coupon->start_date       = strtotime($date_var[0]);
              $coupon->end_date         = strtotime( $date_var[1]);
              $cupon_details = array();
              foreach($request->product_ids as $product_id) {
                  $data['product_id'] = $product_id;
                  array_push($cupon_details, $data);
              }
              $coupon->details = json_encode($cupon_details);
              if ($coupon->save()) {
                return redirect()->route('coupon.index')->with('success','Coupon has been Update successfully');
              }
              else{
                return redirect()->route('coupon.index')->with('failed', 'Coupon already exist for this coupon code!');
              }
          }
          elseif ($request->coupon_type == "cart_base") {
              $coupon->type           = $request->coupon_type;
              $coupon->code           = $request->coupon_code;
              $coupon->discount       = $request->discount;
              $coupon->discount_type  = $request->discount_type;
              $date_var               = explode(" - ", $request->date_range);
              $coupon->start_date     = strtotime($date_var[0]);
              $coupon->end_date       = strtotime( $date_var[1]);
              $data                   = array();
              $data['min_buy']        = $request->min_buy;
              $data['max_discount']   = $request->max_discount;
              $coupon->details        = json_encode($data);
              if ($coupon->save()) {
                return redirect()->route('coupon.index')->with('success','Coupon has been Update successfully');
              }
              else{
                return redirect()->route('coupon.index')->with('failed', 'Coupon already exist for this coupon code!');
              }
          }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        
        $coupon->delete();
        return redirect()->back()->with('success', 'Coupon Delete Successfully');
    }

    public function get_coupon_form(Request $request)
    {
        $all_product =Category::all();
       
        if($request->coupon_type == "category_base") {
            return view('backend.coupons.product_base_coupon', compact('all_product'));
        }
        elseif($request->coupon_type == "cart_base"){
            return view('backend.coupons.cart_base_coupon');
        }
    }

    public function get_coupon_form_edit(Request $request,$id)
    {
        if($request->coupon_type == "category_base") {
            $coupon = Coupon::find($request->id);
            return view('backend.coupons.product_base_coupon_edit', compact('coupon'));
        }
        elseif($request->coupon_type == "cart_base"){
            $coupon = Coupon::find($request->id);
            return view('backend.coupons.product_base_coupon_edit', compact('coupon'));
        }
    }
}
