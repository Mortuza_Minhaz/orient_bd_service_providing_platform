<?php

namespace App\Http\Controllers;
use App\Models\Rental;
use Validator;
use Illuminate\Http\Request;

class RentalController extends Controller
{
    //for rental create and store
    public function index(Request $request){

        if ($request->isMethod('POST')) {

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'mobile' => 'required',
                'rental_for' => 'required',
            ]);

            if ($validator->fails())
            {
                return redirect()->back()->with('failed','Required field need to fill up !!!');
            }else{
                if(Rental::create($request->all())){
                    return redirect()->back()->with('success','Successfully Added !!!');
                }
                else{
                    return redirect()->back()->with('failed','Your information saved failed !!!');
                }
        }
    }

        return view('frontend/rental/create');

    }

    public function show(){

        $rental_info=Rental::all();
        return view('backend/rental/index',compact('rental_info'));

    }

    //delete purpose function
    public function delete(Rental $id){

        $id->delete();
        return redirect()->back()->with('success', 'Successfully Delete');

    }
}
