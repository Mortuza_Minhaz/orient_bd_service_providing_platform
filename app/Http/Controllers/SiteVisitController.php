<?php

namespace App\Http\Controllers;
use App\Models\SiteVisit;
use Validator;
use Illuminate\Http\Request;

class SiteVisitController extends Controller
{
        //for site visit create and store
        public function index(Request $request){

            if ($request->isMethod('POST')) {
    
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'mobile' => 'required',
                    'visit_for' => 'required',
                ]);
    
                if ($validator->fails())
                {
                    return redirect()->back()->with('failed','Required field need to fill up !!!');
                }else{
                    if(SiteVisit::create($request->all())){
                        return redirect()->back()->with('success','Successfully Added !!!');
                    }
                    else{
                        return redirect()->back()->with('failed','Your information saved failed !!!');
                    }
            }
        }
    
            return view('frontend/site_visit/create');
    
        }
    
        public function show(){
    
            $site_visit_info=SiteVisit::all();
            return view('backend/site_visit/index',compact('site_visit_info'));
    
        }
    


        public function delete(SiteVisit $id){

            $id->delete();
            return redirect()->back()->with('success', 'Successfully Delete');

        }

}
