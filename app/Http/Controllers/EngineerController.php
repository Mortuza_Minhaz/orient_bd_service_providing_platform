<?php

namespace App\Http\Controllers;

use App\Models\Engineer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;


class EngineerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $in_house_engineers_list = Engineer::all();


        return view('backend/engineer/inHouse_engineer_list', compact('in_house_engineers_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend/engineer/engineer_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $requestData = $request->all();
        $requestData['expertise_on_service'] = implode(',', $request->expertise_on_service);

        $image = $request->file('engineer_image_path');
        $requestData['engineer_image_path'] = $this->uploadImage($image);


        if (Engineer::create($requestData)) {
            return redirect()->route('engineers.index')->with('success', 'Successfully Add');
        }
        return back()->with('failed', 'Something went wrong. Try again');
    }

    private function uploadImage($image)
    {
        $name = time() . '-' . $image->getClientOriginalName();
        $image->move('admin/upload/engineer', $name);
        return '/admin/upload/engineer/' . $name;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Engineer  $engineer
     * @return \Illuminate\Http\Response
     */
    public function show(Engineer $engineer)
    {
        return view('backend/engineer/engineer_profile');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Engineer  $engineer
     * @return \Illuminate\Http\Response
     */
    public function edit(Engineer $engineer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Engineer  $engineer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Engineer $engineer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Engineer  $engineer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Engineer $engineer)
    {
        //
    }
}
