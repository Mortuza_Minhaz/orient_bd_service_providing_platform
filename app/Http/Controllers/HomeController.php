<?php

namespace App\Http\Controllers;

use Response;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Models\Category;
use DB;
use App\Models\Address;
use App\Models\Coupon;
use App\Models\CouponUsage;
use App\Models\Order;
use App\Models\OrderDetails;
use App\Models\Service;

use Illuminate\Support\Carbon;
use Validator;
use Session;
use Illuminate\Support\Str;
use App\Utility\Utility;

class HomeController extends Controller
{

    public function customer_login()
    {
       /* $intended_url = redirect()->intended('/')->getTargetUrl();
        # $intended_url = redirect()->intended(route('user.dashboard'))->getTargetUrl();


        if (!session()->has('url.intended')) {

            redirect()->setIntendedUrl($intended_url);

        }*/
        return view('frontend.user.login');

    }
    public function altest(){
        echo 'hiii'; exit();
    }

    public function customer_registration()
    {
        return view('frontend.user.registration');
    }

    public function user_dashboard()
    {
       /* if (!Utility::permissionCheck('user_dashboard')) {
            return back()->with('failed', Utility::getPermissionMsg());
        }*/
        // if(Auth::user()->user_type == 'seller'){
        //     return view('frontend.user.seller.dashboard');
        // }
        // elseif(Auth::user()->user_type == 'customer'){
        //     return view('frontend.user.customer.dashboard');
        // }
        // else {
        //     // abort(404);
        // return view('frontend.contact_us');
        // }

        return view('frontend.user.service_provider.dashboard');
    }

    public function service_provider_registration()
    {
        return view('frontend.user.service_provider.service_provider_registration');
    }

    public function provider_services_list()
    {
        return view('frontend.user.service_provider.my_service');
    }



    public function service_provider_orders()
    {
        return view('frontend.user.service_provider.orders.index');
    }


    public function index()
    {

        return view('home');
    }

    public function homeView()
    {

        $home = 'home';
        $parentCategories = Category::tree();
        $categories_info = Category::where('is_popular', 1)->get();
        $service = Service::get();

        #return view('frontend/layouts/app', compact('home','parentCategories'));
        return view('frontend/index', compact('home', 'parentCategories', 'categories_info','service' ));
    }

    public function homeView2()
    {

        $parentCategories = Category::tree();
        return view('pages/backend/home_view', compact('parentCategories'));
    }
}
