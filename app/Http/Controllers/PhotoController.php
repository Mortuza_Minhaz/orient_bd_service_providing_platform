<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController as BaseController;
use App\Models\Photo;
use Illuminate\Http\Request;
use Validator;

class PhotoController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $photo_info=Photo::all();
        return view('backend/image_file/index',["photo_info"=>$photo_info]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend/image_file/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     //Varities Image save purpose function
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image_title' => 'required',
            'image_type' => 'required',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->with('failed','Required field need to fill up !!!');
        }else{

        $image_data = new Photo;
        $image_data = $request->only(['image_title','image_type','image_meta_keywords','image_meta_description']);

        $image = $request->file('image_file');
        $folder_name = 'all_file';
        $image_data['image_path'] = $this->uploadImage($image, $folder_name);

        if(!empty(Photo::create($image_data))){
            return redirect()->route('photos.index')->with('success', 'Successfully Added');
        }
        else{
            return redirect()->route('photos.index')->with('failed','Your image saved failed !!!');
        }

    }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function show(Photo $photo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function edit(Photo $photo)
    {
        echo "hii";
        exit();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Photo $photo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function delete(Photo $photo)
    {

        $old_image = public_path($photo->image_path);
        if (is_file($old_image)) {
            unlink($old_image);
        }

        $photo->delete();
        return redirect()->back()->with('success', 'Successfully Delete');

    }
}
