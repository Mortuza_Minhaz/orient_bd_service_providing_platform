<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    /*protected $redirectTo = RouteServiceProvider::HOME;*/

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function username__1()
    {
        return 'email';
    }
    public function username()
    {
        if(is_numeric(request()->get('email'))){
            $loginType = request()->input('email');
        }elseif(filter_var(request()->get('email'), FILTER_VALIDATE_EMAIL)){
            $loginType = request()->input('email');
        }


        $this->phone = filter_var($loginType, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
        request()->merge([$this->phone => $loginType]);

        return property_exists($this, 'phone') ? $this->phone : 'email';
    }
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
