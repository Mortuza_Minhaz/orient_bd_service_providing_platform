<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController as BaseController;

use App\Models\Category;
use App\Models\User;
use App\Models\ServiceProvider;
use App\Models\ServiceList;
use App\Models\SelectedService;
// use App\Models\OtherSelectedService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use Devfaysal\BangladeshGeocode\Models\Division;
use Devfaysal\BangladeshGeocode\Models\District;
use Devfaysal\BangladeshGeocode\Models\Upazila;
use Brian2694\Toastr\Facades\Toastr;

class ServiceProviderController extends BaseController
{

    public function index(Request $request)
    {
        //$service_provider_list = ServiceProvider::orderBy('id', 'DESC')->get();

        $district_id = $request->input('service_district');


        $districts_list = District::all();


        if (!empty($district_id)) {

            $service_provider_list = DB::table('service_providers')
                ->select(
                    'service_providers.*',
                    'districts.name'
                )
                ->join('districts', 'districts.id', '=', 'service_providers.service_district')
                ->Where('service_district', $district_id)
                ->orderBy('id', 'DESC')
                ->get();
        } else {
            $service_provider_list = DB::table('service_providers')
                ->select(
                    'service_providers.*',
                    'districts.name'
                )
                ->join('districts', 'districts.id', '=', 'service_providers.service_district')
                ->orderBy('id', 'DESC')
                ->get();
        }


        return view('backend/service_provider/service_provider_list', compact('service_provider_list', 'districts_list'));
    }



    public function serviceProviderProfileCreate()
    {

        $districts = DB::table('districts')->get();

        $upazilas = DB::table('upazilas')->get();

        if(Auth::user()->user_type == 'customer'){
            return view('frontend.user.customer.customer_profile');
        }
        elseif(Auth::user()->user_type == 'inhouse' || Auth::user()->user_type == 'outside'){
            return view('frontend.user.service_provider.service_provider_profile', compact('districts', 'upazilas'));
        }

    }
    public function upazilas_name(Request $request)
    {
        $request_data = $request->service_district_id;

        $district = District::find($request_data);
        $upazilas = $district->upazilas;


        return response()->json([
            'upazilas_name' => $upazilas
        ]);
    }


    public function serviceProviderStore(Request $request)
    {

        $service_provider_id = ServiceProvider::where('id', $request->provider_id)->first();
        $service_provider_user_id = User::where('id', Auth::id())->first();

        // echo '<pre>';
        // print_r($service_provider_user_id);
        // exit();

        $requestUserData['name'] = $request->name;
        $requestUserData['email'] = $request->email;
        $requestUserData['phone'] = $request->phone;
        if($request->new_password != null){
            $requestUserData['password'] = Hash::make($request->new_password);
        }

        $user_new_prof_pic = $request->file('profile_pic');

        if (isset($user_new_prof_pic)) {

            $old_file = public_path(Auth::user()->profile_pic);

            if (is_file($old_file)) {
            unlink($old_file);
            }

            $image = $request->file('profile_pic');
            $folder_name = 'profile_pic';
            $requestUserData['profile_pic'] = uploadImage($image, $folder_name);
        }
        
        $service_provider_user_id->update($requestUserData);

        $requestData = $request->except(['_token','provider_id','name','email','phone', 'profile_pic' ,'new_password']);

        $new_individual_provider_cv = $request->file('individual_provider_cv');
        if (isset($new_individual_provider_cv)) {

            $old_file = public_path(Auth::user()->service_provider->individual_provider_cv);

            if (is_file($old_file)) {
            unlink($old_file);
            }

            $image = $request->file('individual_provider_cv');
            $folder_name = 'individual_provider_cv';
            $requestData['individual_provider_cv'] = uploadImage($image, $folder_name);
        }

        $new_individual_provider_nid = $request->file('individual_provider_nid');
        if (isset($new_individual_provider_nid)) {

            $old_file = public_path(Auth::user()->service_provider->individual_provider_nid);

            if (is_file($old_file)) {
            unlink($old_file);
            }

            $image = $request->file('individual_provider_nid');
            $folder_name = 'individual_provider_nid';
            $requestData['individual_provider_nid'] = uploadImage($image, $folder_name);
        }

        $new_individual_provider_tin = $request->file('individual_provider_tin');
        if (isset($new_individual_provider_tin)) {

            $old_file = public_path(Auth::user()->service_provider->individual_provider_tin);

            if (is_file($old_file)) {
            unlink($old_file);
            }

            $image = $request->file('individual_provider_tin');
            $folder_name = 'individual_provider_tin';
            $requestData['individual_provider_tin'] = uploadImage($image, $folder_name);
        }

        $new_individual_provider_testimonial = $request->file('individual_provider_testimonial');
        if (isset($new_individual_provider_testimonial)) {

            $old_file = public_path(Auth::user()->service_provider->individual_provider_testimonial);

            if (is_file($old_file)) {
            unlink($old_file);
            }

            $image = $request->file('individual_provider_testimonial');
            $folder_name = 'individual_provider_testimonial';
            $requestData['individual_provider_testimonial'] = uploadImage($image, $folder_name);
        }

        $new_individual_provider_experience = $request->file('individual_provider_experience');
        if (isset($new_individual_provider_experience)) {

            $old_file = public_path(Auth::user()->service_provider->individual_provider_experience);

            if (is_file($old_file)) {
            unlink($old_file);
            }

            $image = $request->file('individual_provider_experience');
            $folder_name = 'individual_provider_experience';
            $requestData['individual_provider_experience'] = uploadImage($image, $folder_name);
        }

        $new_individual_provider_noc = $request->file('individual_provider_noc');
        if (isset($new_individual_provider_noc)) {

            $old_file = public_path(Auth::user()->service_provider->individual_provider_noc);

            if (is_file($old_file)) {
            unlink($old_file);
            }

            $image = $request->file('individual_provider_noc');
            $folder_name = 'individual_provider_noc';
            $requestData['individual_provider_noc'] = uploadImage($image, $folder_name);
        }

        $new_company_profile_details = $request->file('company_profile_details');
        if (isset($new_company_profile_details)) {

            $old_file = public_path(Auth::user()->service_provider->company_profile_details);

            if (is_file($old_file)) {
            unlink($old_file);
            }

            $image = $request->file('company_profile_details');
            $folder_name = 'company_profile_details';
            $requestData['company_profile_details'] = uploadImage($image, $folder_name);
        }

        $new_company_nid = $request->file('company_nid');
        if (isset($new_company_nid)) {

            $old_file = public_path(Auth::user()->service_provider->company_nid);

            if (is_file($old_file)) {
            unlink($old_file);
            }

            $image = $request->file('company_nid');
            $folder_name = 'company_nid';
            $requestData['company_nid'] = uploadImage($image, $folder_name);
        }

        $new_company_business_license = $request->file('company_business_license');
        if (isset($new_company_business_license)) {

            $old_file = public_path(Auth::user()->service_provider->company_business_license);

            if (is_file($old_file)) {
            unlink($old_file);
            }

            $image = $request->file('company_business_license');
            $folder_name = 'company_business_license';
            $requestData['company_business_license'] = uploadImage($image, $folder_name);
        }

        $new_company_tin = $request->file('company_tin');
        if (isset($new_company_tin)) {

            $old_file = public_path(Auth::user()->service_provider->company_tin);

            if (is_file($old_file)) {
            unlink($old_file);
            }

            $image = $request->file('company_tin');
            $folder_name = 'company_tin';
            $requestData['company_tin'] = uploadImage($image, $folder_name);
        }

        $new_company_contract_paper = $request->file('company_contract_paper');
        if (isset($new_company_contract_paper)) {

            $old_file = public_path(Auth::user()->service_provider->company_contract_paper);

            if (is_file($old_file)) {
            unlink($old_file);
            }

            $image = $request->file('company_contract_paper');
            $folder_name = 'company_contract_paper';
            $requestData['company_contract_paper'] = uploadImage($image, $folder_name);
        }

        $requestData['service_location'] = $request->has('service_location') ? implode(',', $request->service_location) : '';

        $requestData['is_verified_service_provider'] = Auth::id();
        $requestData['user_id'] = Auth::id();


        $provider_id = $service_provider_id->update($requestData);

        if ($provider_id) {
            $name = "Dear Mr. " . $request->name . ' ' . 'Thanks for your application. We will contact you as soon as possible.';
            Toastr::success($name, 'Success', ["closeButton" => true, "debug" => false, "newestOnTop" => false, "progressBar" => true, "positionClass" => "toast-bottom-center", "preventDuplicates" => false, "onclick" => null, "showDuration" => "300", "hideDuration" => "1000", "timeOut" => "8000", "extendedTimeOut" => "1000", "showEasing" => "swing", "hideEasing" => "linear", "showMethod" => "fadeIn", "hideMethod" => "fadeOut"]);
            return redirect()->back();
        } else {

            Toastr::error('try again', 'Error');
            return redirect()->back();
        }
        # return redirect()->route('service_provider_create')->with('success', 'Successfully Submitted');
    }

    public function create()
    {
        $categories = Category::all();

        return view('backend/service_provider/service_provider_create', compact('categories'));
    }

    public function store(Request $request)
    {
        echo '<pre>';
        print_r($request->all());
        exit();

        $requestData = $request->all();
        $requestData['expertise_on_service'] = implode(',', $request->expertise_on_service);


        if (ServiceProvider::create($requestData)) {
            return redirect()->route('service-providers.index')->with('success', 'Successfully Add');
        }
        return back()->with('failed', 'Something went wrong. Try again');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ServiceProvider  $serviceProvider
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceProvider $serviceProvider, $id)
    {
        $service_providers = DB::table('service_providers')->where('id', $id)->first();
        $service_list = SelectedService::where('provider_id', $id)->get();

        return view('backend/service_provider/service_provider_profile', compact('service_providers', 'service_list'));
    }



    public function details(ServiceProvider $serviceProvider, $id)
    {
        $service_providers = DB::table('service_providers')->where('id', $id)->first();

        $service_district_list = explode(',', $service_providers->service_district);

        $provider_service_district = DB::table('districts')
            ->select('name', 'bn_name')
            ->whereIn('id', $service_district_list)
            ->get();

        $service_upazila_list = explode(',', $service_providers->service_location);

        $provider_service_upazila = DB::table('upazilas')
            ->select('name', 'bn_name')
            ->whereIn('id', $service_upazila_list)
            ->get();

        $service_list = SelectedService::where('provider_id', $id)->get();
        // $other_service_list = DB::table('other_selected_services')
        //     ->select('other_service_list_title', 'other_investigation_charge', 'other_repairing_charge')
        //     ->where('provider_id', $id)
        //     ->get();

        return view('backend/service_provider/service_provider_details', compact('service_providers', 'service_list', 'provider_service_district', 'provider_service_upazila'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ServiceProvider  $serviceProvider
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceProvider $serviceProvider)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ServiceProvider  $serviceProvider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceProvider $serviceProvider)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ServiceProvider  $serviceProvider
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceProvider $serviceProvider, $id)
    {
        $provider = ServiceProvider::findOrFail($id);
        ServiceProvider::where('id', $provider->id)->delete();
        return redirect()->back()->with('success', 'Successfully Deleted');
    }

  /*  Sobuj*/

    public function checkout()
    {

        $districts = DB::table('districts')->get();

        $upazilas = DB::table('upazilas')->get();
        return view('frontend.checkout', compact('districts', 'upazilas'));


    }
}
