<?php

namespace App\Http\Controllers;

use App\Models\ServiceList;
use Illuminate\Http\Request;

class ServiceListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service_list = ServiceList::all();

        return view('backend/service_list/index', compact('service_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend/service_list/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        if (ServiceList::create($requestData)) {
            return redirect()->back()->with('success', 'Successfully Added');
        } else {
            return redirect()->back()->with('failed', 'Something went wrong. Please try again!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ServiceList  $serviceList
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceList $serviceList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ServiceList  $serviceList
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceList $serviceList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ServiceList  $serviceList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = serviceList::find($id);

        $requestData['service_title'] = $request->service_title;
        $requestData['service_status'] = $request->service_status;

        if ($requestData->update()) {
            return redirect()->back()->with('success', 'Successfully updated');
        } else {
            return redirect()->back()->with('failed', 'Something went wrong. Please try again!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ServiceList  $serviceList
     * @return \Illuminate\Http\Response
     */
    public function delete(ServiceList $serviceList, $id)
    {
        $requestData = serviceList::find($id);

        if ($requestData->delete()) {
            return redirect()->back()->with('success', 'Successfully Deleted');
        } else {
            return redirect()->back()->with('failed', 'Something went wrong. Please try again!');
        }
    }
}
