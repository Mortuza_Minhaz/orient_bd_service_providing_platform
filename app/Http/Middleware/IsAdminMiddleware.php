<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IsAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
       /* if (Auth::check() && (Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'staff')) {
            return $next($request);
        }
        else{
            abort(404);
        }*/


      /*  if(!Auth::check() || Auth::user()->user_type != 'admin'){
            return redirect()->route('home');
        }
        return $next($request);*/

        if ($request->user() && $request->user()->user_type == 'admin') {
            return $next($request);
        } elseif ($request->user() && $request->user()->user_type == 'customer') {
            return redirect()->route('user.dashboard');
        }elseif ($request->user() && $request->user()->user_type == 'outside') {
            return redirect()->route('user.dashboard');
        }elseif ($request->user() && $request->user()->user_type == 'inhouse') {
            return redirect()->route('user.dashboard');
        }
        return redirect()->route('homeView');

    }
}
