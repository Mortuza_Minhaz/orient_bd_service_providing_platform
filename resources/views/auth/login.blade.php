<!DOCTYPE html>

<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>AEL Admin Dashboard</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #1 for " name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css" />
    <link href="/admin/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/admin/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="/admin/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="/admin/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="/admin/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="/admin/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="/admin/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="/admin/assets/pages/css/login.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->

<body class=" login">

    <!-- BEGIN LOGIN -->
    <div class="content">
        <div class="logo">
            <a href="index.html">
                <img src="/admin/assets/layouts/layout/img/belalogoo.png" alt="" /> </a>
        </div>
        <!-- BEGIN LOGIN FORM -->

        {{-- <form method="POST" action="{{ route('login') }}" id="wrapper">
        @csrf
        <div id="box" class="animated bounceIn">
            <div id="top_header">
                <img src="/admin/img/logo.png" alt="Arise Admin Dashboard Logo" />



                <!-- show errors -->

                <!-- / show errors -->

            </div>
            <div id="inputs">
                <div class="form-block">
                    <input type="email" name="email" placeholder="Email">
                    <i class="icon-user-check"></i>

                </div>
                <div class="form-block">
                    <input type="password" name="password" placeholder="Password">
                    <i class="icon-spell-check"></i>
                </div>
                <input type="submit" value="Sign In">
            </div>
            <div id="bottom" class="clearfix">
                <div class="pull-right">
                    <label class="switch pull-right">
                        <input type="checkbox" name="remember" class="switch-input" checked>
                        <span class="switch-label" data-on="Yes" data-off="No"></span>
                        <span class="switch-handle"></span>
                    </label>
                </div>

            </div>
        </div>
        </form>--}}
        <form class="login-form" action="{{ route('login') }}" method="post">
            @csrf

            <div style="color: red; clear: both;">
                @if ($errors->all())
                <div class="invalid-feedback" role="alert" style="padding-bottom: 10px;">
                    <strong>{{ $errors->first() }}</strong></div>
                @else
                <h5>
                    <br>
                </h5>
                @endif
            </div>

            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Email</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" value="{{ old('email') }}" placeholder="email" name="email" /> </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
            <div class="form-actions">
                <button type="submit" class="btn green uppercase">Login</button>

            </div>


        </form>
        <!-- END LOGIN FORM -->


    </div>
    <div class="copyright"> {{date("Y")}} &copy; amarbebsha.com</div>
    <!-- BEGIN CORE PLUGINS -->
    <script src="/admin/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="/admin/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/admin/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="/admin/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="/admin/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="/admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
    <script src="/admin/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="/admin/assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="/admin/assets/pages/scripts/login.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <!-- END THEME LAYOUT SCRIPTS -->
    <script>
        $(document).ready(function() {
            $('#clickmewow').click(function() {
                $('#radio1003').attr('checked', 'checked');
            });
        })
    </script>


    <!-- End -->
</body>



<!-- Mirrored from keenthemes.com/preview/metronic/theme/admin_1/page_user_login_1.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 22 Jun 2019 20:46:33 GMT -->

</html>
