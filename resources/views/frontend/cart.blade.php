@extends('frontend.layouts.app')
@section('title', 'Home')
@section('meta_keywords','home,home page')
@section('meta_description', 'home page of test')
@section('js')
    <script type="text/javascript">

        function incrementValue(e) {
            e.preventDefault();
            var fieldName = $(e.target).data('field');
            var parent = $(e.target).closest('div');
            var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

            if (!isNaN(currentVal)) {
                parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
            } else {
                parent.find('input[name=' + fieldName + ']').val(0);
            }
        }

        function decrementValue(e) {
            e.preventDefault();
            var fieldName = $(e.target).data('field');
            var parent = $(e.target).closest('div');
            var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

            if (!isNaN(currentVal) && currentVal > 0) {
                parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
            } else {
                parent.find('input[name=' + fieldName + ']').val(0);
            }
        }

        $('.input-group').on('click', '.button-plus', function(e) {
            incrementValue(e);
        });

        $('.input-group').on('click', '.button-minus', function(e) {
            decrementValue(e);
        });
    </script>

@endsection

@section('css')

    <style>
        .cart-main-area .table-content table{
            border:1px solid #ebebeb;
            width:100%;
            background-color:#fff;
        }
        .cart-main-area .table-content table thead>tr{
            background-color:#f9f9f9;
            border:1px solid #ebebeb
        }
        .cart-main-area .table-content table thead>tr>th{
            border-top:medium none;
            color:#1d1d1d;
            font-size:14px;
            font-weight:700;
            padding:21px 45px 22px;
            text-align:center;
            text-transform:uppercase;
            vertical-align:middle;
            white-space:nowrap
        }
        .cart-main-area .table-content table tbody>tr{
            border-bottom:1px solid #ebebeb
        }
        .cart-main-area .table-content table tbody>tr td{
            color:#666;
            font-size:15px;
            padding:10px 0;
            text-align:center
        }
        .cart-main-area .table-content table tbody>tr td.product-thumbnail{
            width:150px
        }
        .cart-main-area .table-content table tbody>tr td.product-name{
            width:435px
        }
        .cart-main-area .table-content table tbody>tr td.product-name a{
            color:#666;
            font-size:15px;
            font-weight:500
        }
        .cart-main-area .table-content table tbody>tr td.product-price-cart{
            width:435px
        }
        .cart-main-area .table-content table tbody>tr td.product-quantity{
            width:435px
        }
        .cart-main-area .table-content table tbody>tr td.product-quantity .cart-plus-minus{
            display:inline-block;
            height:40px;
            padding:0;
            position:relative;
            width:110px
        }
        .cart-main-area .table-content table tbody>tr td.product-quantity .cart-plus-minus .dec.qtybutton{
            border-right:1px solid #e5e5e5;
            height:40px;
            left:0;
            padding-top:8px;
            top:0
        }
        .cart-main-area .table-content table tbody>tr td.product-quantity .cart-plus-minus .inc.qtybutton{
            border-left:1px solid #e5e5e5;
            height:40px;
            padding-top:9px;
            right:0;
            top:0
        }
        .cart-main-area .table-content table tbody>tr td.product-quantity .cart-plus-minus .qtybutton{
            color:#666;
            cursor:pointer;
            float:inherit;
            font-size:16px;
            margin:0;
            position:absolute;
            transition:all .3s ease 0s;
            width:20px;
            text-align:center
        }
        .cart-main-area .table-content table tbody>tr td.product-quantity .cart-plus-minus input.cart-plus-minus-box{
            color:#666;
            float:left;
            font-size:14px;
            height:40px;
            margin:0;
            width:110px;
            background:transparent none repeat scroll 0 0;
            border:1px solid #e1e1e1;
            padding:0;
            text-align:center
        }
        .cart-main-area .table-content table tbody>tr td.product-remove{
            width:100px
        }
        .cart-main-area .table-content table tbody>tr td.product-remove a{
            color:#666;
            font-size:16px;
            margin:0 10px
        }
        .cart-main-area .table-content table tbody>tr td.product-remove a:hover{
            color:#f16522
        }
        .cart-main-area .table-content table tbody>tr td.product-wishlist-cart>a{
            background-color:#f16522;
            border-radius:5px;
            color:#fff;
            font-size:14px;
            font-weight:700;
            line-height:1;
            padding:10px 12px;
            text-transform:uppercase
        }
        .cart-main-area .table-content table tbody>tr td.product-wishlist-cart>a:hover{
            background-color:#1d1d1d
        }
        .cart-main-area .cart-shiping-update-wrapper{
            display:flex;
            justify-content:space-between;
            padding:30px 0 60px
        }
        .cart-main-area .cart-shiping-update-wrapper .cart-clear>button{
            border:medium none;
            cursor:pointer;
            margin-right:27px;
            transition:all .3s ease 0s
        }
        @media only screen and (max-width:767px){
            .cart-main-area .cart-shiping-update-wrapper{
                display:block;
                padding:30px 0 15px
            }
        }
        .cart-main-area .table-content table tbody>tr td.product-quantity .cart-plus-minus input.cart-plus-minus-box {
            color: #666;
            float: left;
            font-size: 14px;
            height: 40px;
            margin: 0;
            width: 110px;
            background: transparent none repeat scroll 0 0;
            border: 1px solid #e1e1e1;
            padding: 0;
            text-align: center;
        }
       /* cart plus minus start*/
        input,
        textarea {
            border: 1px solid #eeeeee;
            box-sizing: border-box;
            margin: 0;
            outline: none;
            padding: 10px;
        }

        input[type="button"] {
            -webkit-appearance: button;
            cursor: pointer;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
        }

        .input-group {
            clear: both;
            margin: 15px 0;
            position: relative;
            text-align: center;
        }

        .input-group input[type='button'] {
            background-color: #eeeeee;
            min-width: 27px;
            width: auto;
            transition: all 300ms ease;
        }

        .input-group .button-minus,
        .input-group .button-plus {
            font-weight: bold;
            height: 38px;
            padding: 0;
            width: 38px;
            position: relative;
        }

        .input-group .quantity-field {
            position: relative;
            height: 38px;
            left: -6px;
            text-align: center;
            width: 62px;
            display: inline-block;
            font-size: 13px;
            margin: 0 0 5px;
            resize: vertical;
        }

        .button-plus {
            left: -13px;
        }

        input[type="number"] {
            -moz-appearance: textfield;
            -webkit-appearance: none;
        }

        /* cart plus minus end*/
        .btn-info {
            color: #333;
            background-color: #dfdcdc; !important;
            border-color: #dfdcdc; !important;
        }
        .btn-info, .btn-soft-info:hover, .btn-outline-info:hover {
            background-color: #dfdcdc; !important;
            border-color: #dfdcdc; !important;
            color: #333 !important;
        }

        .btn-info:hover {
            color: #fff !important;
            background-color: #bfbdbd !important;
            border-color: #bfbdbd !important;
        }
    </style>
@endsection
@section('content')
    <section class="mb-4 mt-4" id="cart-summary">
        <div class="cart-main-area mtb-60px">
            <div class="container">
                <h3 class="cart-page-title text-center">Your cart items</h3>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <form action="#">
                            <div class="table-content table-responsive cart-table-content">
                                <table>
                                    <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Service Name</th>
                                        <th>Service charge</th>
                                        <th>Qty</th>
                                        <th>Total</th>
                                        <th>Remove</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="product-thumbnail">
                                            <a href="#"><img class="img-responsive" src="/frontend/assets/images/1.jpg" alt=""></a>
                                        </td>
                                        <td class="product-name"><a href="#">Air Conditioning Repair</a></td>
                                        <td class="product-price-cart"><span class="amount">60.00 ৳</span></td>
                                        <td class="product-quantity">
                                            <div class="input-group d-flex justify-content-center">
                                                <input type="button" value="-" class="button-minus" data-field="quantity">
                                                <input type="number" step="1" max="" value="1" name="quantity" class="quantity-field">
                                                <input type="button" value="+" class="button-plus" data-field="quantity">
                                            </div>
                                        </td>
                                        <td class="product-subtotal">70.00 ৳</td>
                                        <td class="product-remove">
                                            <div class="col-lg-auto col-6 order-5 order-lg-0 d-flex justify-content-center">
                                                <a href="#"  class="btn btn-icon btn-sm btn-soft-primary btn-circle" style="padding-top: 5px;">
                                                    <i class="las la-trash"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="cart-shiping-update-wrapper">
                                        <div>
                                        </div>
                                        <div class="cart-clear">
                                            <a href="{{route('checkout_summery')}}" class="btn btn-primary btn-sm">Checkout</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

