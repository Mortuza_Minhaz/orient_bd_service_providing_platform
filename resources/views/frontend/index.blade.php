@extends('frontend.layouts.app')
@section('title', 'Home')
@section('meta_keywords','home,home page')
@section('meta_description', 'home page of test')
@section('css')
    <style>

        .short-description ul {
            list-style: none;
            padding: 3px;
        }
        .btn-soft-primary.fw-600 {
            background-color: #4c9cf3;
            color: #ffffff;
        }
        .btn-soft-primary:hover {
            color: #fff !important;
            background-color: #277af3 !important;
            border-color: #277af3 !important;
        }
        .btn-info {
            color: #fff;
            background-color: #b5e61d !important;
            border-color:  #b5e61d !important;
        }
        .btn-info, .btn-soft-info:hover, .btn-outline-info:hover {
            background-color: #b5e61d !important;
            border-color: #b5e61d !important;
            color: var(--white) !important;
        }

        .btn-info:hover {
            color: #fff !important;
            background-color: #8db709 !important;
            border-color: #8db709 !important;
        }
        .btn-info {
            color: #fff;
            background-color: #4fcff1; !important;
            border-color: #4fcff1; !important;
        }
        .btn-success, .btn-soft-success:hover, .btn-outline-success:hover {
            background-color: #4fcff1; !important;
            border-color: #4fcff1; !important;
            color: var(--white) !important;
        }

        .btn-success:hover {
            color: #fff !important;
            background-color: #31a1bf !important;
            border-color: #31a1bf !important;
        }
        .btn-warning, .btn-soft-warning:hover, .btn-outline-warning:hover {
            background-color: #efc818; !important;
            border-color: #efc818; !important;
            color: var(--white) !important;
        }
        .btn-secondary, .btn-soft-secondary:hover, .btn-outline-secondary:hover {
            background-color: #f84581; !important;
            border-color: #f84581; !important;
            color: var(--white) !important;
        }

        .btn-secondary:hover {
            color: #fff !important;
            background-color: #df2c68 !important;
            border-color: #df2c68 !important;
        }
        .btn-warning:hover {
            color: #fff !important;
            background-color: #dcb80f !important;
            border-color: #dcb80f !important;
        }
        .btn-danger, .btn-soft-danger:hover, .btn-outline-success:hover {
            background-color: #a996e7; !important;
            border-color: #a996e7; !important;
            color: var(--white) !important;
        }
        .btn-danger:not(:disabled):not(.disabled).active, .btn-danger:not(:disabled):not(.disabled):active, .show>.btn-danger.dropdown-toggle {
            color: #fff;
            background-color: #a996e7 !important;
            border-color: #a996e7 !important;
        }
        .btn-danger:hover {
            color: #fff !important;
            background-color: #846dd0 !important;
            border-color: #846dd0 !important;
        }

    </style>
@section('content')

    <!-- category  start-->
    <div class="home-banner-area mb-4 pt-3">
        <div class="container">
            <div class="row gutters-10 position-relative">
                <div class="col-lg-3 position-static d-none d-lg-block">
                    @include('frontend.partials.category_menu')
                </div>

                <div class=" col-lg-7 ">
                    <div class="aiz-carousel dots-inside-bottom mobile-img-auto-height" data-arrows="true" data-dots="true" data-autoplay="true" data-infinite="true">
                        <div class="carousel-box">
                            <a href="">
                                <img
                                        class="d-block mw-100 lazyload img-fit rounded shadow-sm"
                                        src="/frontend/assets/images/product/placeholder-rect.jpg"
                                        data-src="frontend/assets/images/slider-image/slider1.jpg"
                                        alt="Bela O Bela promo"
                                        height="457"
                                        onerror="this.onerror=null;this.src='/frontend/assets/images/product/placeholder-rect.jpg';">
                            </a>
                        </div>
                        <div class="carousel-box">
                            <a href="">
                                <img
                                        class="d-block mw-100 lazyload img-fit rounded shadow-sm"
                                        src="/frontend/assets/images/product/placeholder-rect.jpg"
                                        data-src="frontend/assets/images/slider-image/slider2.jpg"
                                        alt="Bela O Bela promo"

                                        height="457"

                                        onerror="this.onerror=null;this.src='/frontend/assets/images/product/placeholder-rect.jpg';"
                                >
                            </a>
                        </div>
                        <div class="carousel-box">
                            <a href="">
                                <img
                                        class="d-block mw-100 lazyload img-fit rounded shadow-sm"
                                        src="/frontend/assets/images/product/placeholder-rect.jpg"
                                        data-src="frontend/assets/images/slider-image/slider3.jpg"
                                        alt="Bela O Bela promo"

                                        height="457"

                                        onerror="this.onerror=null;this.src='/frontend/assets/images/product/placeholder-rect.jpg';"
                                >
                            </a>
                        </div>
                        <div class="carousel-box">
                            <a href="">
                                <img
                                        class="d-block mw-100 lazyload img-fit rounded shadow-sm"
                                        src="/frontend/assets/images/product/placeholder-rect.jpg"
                                        data-src="frontend/assets/images/slider-image/slider4.jpg"
                                        alt="Bela O Bela promo"

                                        height="457"

                                        onerror="this.onerror=null;this.src='/frontend/assets/images/product/placeholder-rect.jpg';"
                                >
                            </a>
                        </div>

                    </div>

                </div>


                <div class="col-xs-6 col-lg-2 order-3 mt-3 mt-lg-0 bannerresponsive">

                    <a href=""><img  alt="Bela O Bela promo" class="bannerimg banner-area img-responsive lazyload bannerpaddingright" src="frontend/assets/images/banner-image/side-banner1.jpg" data-src="frontend/assets/images/banner-image/side-banner1.jpg"/></a>
                    <a href=""><img  alt="Bela O Bela promo" class="bannerimg banner-area img-responsive lazyload bannerpaddingright" src="frontend/assets/images/banner-image/side-banner2.jpg" data-src="frontend/assets/images/banner-image/side-banner2.jpg"/></a>

                </div>



            </div>
        </div>
    </div>
    <!-- category end-->


    <!-- popular Category start-->
    <section class="mb-4">
        <div class="container">
            <div class="row gutters-10">

                <div class="col-lg-12">
                    <center><div class="d-flex mb-3 align-items-baseline border-bottom">
                            <div class="col-md-12">
                                <h2 class="h5 fw-700 mb-0" style="text-algin:center">
                                    Popular By Choice
                                </h2>
                                <h6 class="py-10px"><span class="border-primary border-width-2 pb-0 d-inline-block">Categories our customers love to check out</span></h6>
                            </div>
                        </div></center>
                    <div class="row gutters-5">


                        <div class="col-sm-2 col-6">
                            <a style="min-height: 110px;" href="#" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                                <div class="row align-items-center no-gutters">
                                    <div class="col-12 text-center">
                                        <img src="frontend/assets/images/category/1.png" data-src="frontend/assets/images/category/1.png" alt="IT Products" class="img-fluid img h-60px ls-is-cached lazyloaded" onerror="this.onerror=null;this.src='/frontend/assets/images/product/placeholder-rect.jpg';">
                                        <div class="text-truncat-2 pl-3 fs-13 fw-500 text-center">IT Products</div>
                                    </div>


                                </div>
                            </a>
                        </div>


                        <div class="col-sm-2 col-6">
                            <a style="min-height: 110px;" href="#" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                                <div class="row align-items-center no-gutters">
                                    <div class="col-12 text-center">
                                        <img src="frontend/assets/images/category/2.png" data-src="frontend/assets/images/category/2.png" alt="Electronic Items" class="img-fluid img h-60px ls-is-cached lazyloaded" onerror="this.onerror=null;this.src='/frontend/assets/images/product/placeholder-rect.jpg';">
                                        <div class="text-truncat-2 pl-3 fs-13 fw-500 text-center">Electronic Items</div>
                                    </div>


                                </div>
                            </a>
                        </div>


                        <div class="col-sm-2 col-6">
                            <a style="min-height: 110px;" href="#" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                                <div class="row align-items-center no-gutters">
                                    <div class="col-12 text-center">
                                        <img src="frontend/assets/images/category/3.png" data-src="frontend/assets/images/category/3.png" alt="Office Equipment" class="img-fluid img h-60px ls-is-cached lazyloaded" onerror="this.onerror=null;this.src='/frontend/assets/images/product/placeholder-rect.jpg';">
                                        <div class="text-truncat-2 pl-3 fs-13 fw-500 text-center">Office Equipment</div>
                                    </div>


                                </div>
                            </a>
                        </div>


                        <div class="col-sm-2 col-6">
                            <a style="min-height: 110px;" href="#" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                                <div class="row align-items-center no-gutters">
                                    <div class="col-12 text-center">
                                        <img src="frontend/assets/images/category/4.png" data-src="frontend/assets/images/category/4.png" alt="Men's Fashion" class="img-fluid img h-60px ls-is-cached lazyloaded" onerror="this.onerror=null;this.src='/frontend/assets/images/product/placeholder-rect.jpg';">
                                        <div class="text-truncat-2 pl-3 fs-13 fw-500 text-center">Men's Fashion</div>
                                    </div>


                                </div>
                            </a>
                        </div>


                        <div class="col-sm-2 col-6">
                            <a style="min-height: 110px;" href="/frontend/assets/images/product/placeholder-rect.jpg" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                                <div class="row align-items-center no-gutters">
                                    <div class="col-12 text-center">
                                        <img src="frontend/assets/images/category/5.png" data-src="frontend/assets/images/category/5.png" alt="Women's Fashion" class="img-fluid img h-60px ls-is-cached lazyloaded" onerror="this.onerror=null;this.src='/frontend/assets/images/product/placeholder-rect.jpg';">
                                        <div class="text-truncat-2 pl-3 fs-13 fw-500 text-center">Women's Fashion</div>
                                    </div>


                                </div>
                            </a>
                        </div>


                        <div class="col-sm-2 col-6">
                            <a style="min-height: 110px;" href="#" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                                <div class="row align-items-center no-gutters">
                                    <div class="col-12 text-center">
                                        <img src="frontend/assets/images/category/6.png" data-src="frontend/assets/images/category/6.png" alt="Beauty &amp; Lifestyle" class="img-fluid img h-60px ls-is-cached lazyloaded" onerror="this.onerror=null;this.src='/frontend/assets/images/product/placeholder-rect.jpg';">
                                        <div class="text-truncat-2 pl-3 fs-13 fw-500 text-center">Beauty &amp; Lifestyle</div>
                                    </div>


                                </div>
                            </a>
                        </div>


                        <div class="col-sm-2 col-6">
                            <a style="min-height: 110px;" href="#" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                                <div class="row align-items-center no-gutters">
                                    <div class="col-12 text-center">
                                        <img src="frontend/assets/images/category/7.png" data-src="frontend/assets/images/category/7.png" alt="Groceries" class="img-fluid img h-60px ls-is-cached lazyloaded" onerror="this.onerror=null;this.src='/frontend/assets/images/product/placeholder-rect.jpg';">
                                        <div class="text-truncat-2 pl-3 fs-13 fw-500 text-center">Groceries</div>
                                    </div>


                                </div>
                            </a>
                        </div>


                        <div class="col-sm-2 col-6">
                            <a style="min-height: 110px;" href="#" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                                <div class="row align-items-center no-gutters">
                                    <div class="col-12 text-center">
                                        <img src="/frontend/assets/images/category/8.png" data-src="frontend/assets/images/category/8.png" alt="Home Appliance" class="img-fluid img h-60px ls-is-cached lazyloaded" onerror="this.onerror=null;this.src='/frontend/assets/images/product/placeholder-rect.jpg';">
                                        <div class="text-truncat-2 pl-3 fs-13 fw-500 text-center">Home Appliance</div>
                                    </div>


                                </div>
                            </a>
                        </div>


                        <div class="col-sm-2 col-6">
                            <a style="min-height: 110px;" href="#" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                                <div class="row align-items-center no-gutters">
                                    <div class="col-12 text-center">
                                        <img src="/frontend/assets/images/category/3.png" data-src="/frontend/assets/images/category/3.png" alt="Mobile Phone" class="img-fluid img h-60px ls-is-cached lazyloaded" onerror="this.onerror=null;this.src='#';">
                                        <div class="text-truncat-2 pl-3 fs-13 fw-500 text-center">Mobile Phone</div>
                                    </div>


                                </div>
                            </a>
                        </div>


                        <div class="col-sm-2 col-6">
                            <a style="min-height: 110px;" href="#" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                                <div class="row align-items-center no-gutters">
                                    <div class="col-12 text-center">
                                        <img src="frontend/assets/images/category/9.png" data-src="frontend/assets/images/category/9.png" alt="Baby's Gallery" class="img-fluid img h-60px ls-is-cached lazyloaded" onerror="this.onerror=null;this.src='/frontend/assets/images/product/placeholder-rect.jpg';">
                                        <div class="text-truncat-2 pl-3 fs-13 fw-500 text-center">Baby's Gallery</div>
                                    </div>


                                </div>
                            </a>
                        </div>


                        <div class="col-sm-2 col-6">
                            <a style="min-height: 110px;" href="#" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                                <div class="row align-items-center no-gutters">
                                    <div class="col-12 text-center">
                                        <img src="frontend/assets/images/category/10.png" data-src="frontend/assets/images/category/11.png" alt="Gift Items" class="img-fluid img h-60px ls-is-cached lazyloaded" onerror="this.onerror=null;this.src='/frontend/assets/images/product/placeholder-rect.jpg';">
                                        <div class="text-truncat-2 pl-3 fs-13 fw-500 text-center">Gift Items</div>
                                    </div>


                                </div>
                            </a>
                        </div>


                        <div class="col-sm-2 col-6">
                            <a style="min-height: 110px;" href="#" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                                <div class="row align-items-center no-gutters">
                                    <div class="col-12 text-center">
                                        <img src="frontend/assets/images/category/11.png" data-src="frontend/assets/images/category/11.png" alt="Industrial Machineries" class="img-fluid img h-60px ls-is-cached lazyloaded" onerror="this.onerror=null;this.src='/frontend/assets/images/product/placeholder-rect.jpg';">
                                        <div class="text-truncat-2 pl-3 fs-13 fw-500 text-center">Industrial Machineries</div>
                                    </div>


                                </div>
                            </a>
                        </div>


                        <div class="col-sm-2 col-6">
                            <a style="min-height: 110px;" href="#" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                                <div class="row align-items-center no-gutters">
                                    <div class="col-12 text-center">
                                        <img src="frontend/assets/images/category/12.png" data-src="frontend/assets/images/category/12.png" alt="Battery" class="img-fluid img h-60px ls-is-cached lazyloaded" onerror="this.onerror=null;this.src='/frontend/assets/images/product/placeholder-rect.jpg';">
                                        <div class="text-truncat-2 pl-3 fs-13 fw-500 text-center">Battery</div>
                                    </div>


                                </div>
                            </a>
                        </div>


                        <div class="col-sm-2 col-6">
                            <a style="min-height: 110px;" href="#" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                                <div class="row align-items-center no-gutters">
                                    <div class="col-12 text-center">
                                        <img src="frontend/assets/images/category/13.png" data-src="frontend/assets/images/category/13.png" alt="Health Care" class="img-fluid img h-60px lazyloaded" onerror="this.onerror=null;this.src='/frontend/assets/images/product/placeholder-rect.jpg';">
                                        <div class="text-truncat-2 pl-3 fs-13 fw-500 text-center">Health Care</div>
                                    </div>


                                </div>
                            </a>
                        </div>


                        <div class="col-sm-2 col-6">
                            <a style="min-height: 110px;" href="#" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                                <div class="row align-items-center no-gutters">
                                    <div class="col-12 text-center">
                                        <img src="frontend/assets/images/category/14.png" data-src="frontend/assets/images/category/14.png" alt="Sports &amp; Travel" class="img-fluid img h-60px lazyloaded" onerror="this.onerror=null;this.src='/frontend/assets/images/product/placeholder-rect.jpg';">
                                        <div class="text-truncat-2 pl-3 fs-13 fw-500 text-center">Sports &amp; Travel</div>
                                    </div>


                                </div>
                            </a>
                        </div>


                        <div class="col-sm-2 col-6">
                            <a style="min-height: 110px;" href="#" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                                <div class="row align-items-center no-gutters">
                                    <div class="col-12 text-center">
                                        <img src="frontend/assets/images/category/15.png" data-src="frontend/assets/images/category/15.png" alt="Books" class="img-fluid img h-60px ls-is-cached lazyloaded" onerror="this.onerror=null;this.src='/frontend/assets/images/product/placeholder-rect.jpg';">
                                        <div class="text-truncat-2 pl-3 fs-13 fw-500 text-center">Books</div>
                                    </div>


                                </div>
                            </a>
                        </div>


                        <div class="col-sm-2 col-6">
                            <a style="min-height: 110px;" href="#" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                                <div class="row align-items-center no-gutters">
                                    <div class="col-12 text-center">
                                        <img src="frontend/assets/images/category/16.png" data-src="frontend/assets/images/category/16.png" alt="Automobile" class="img-fluid img h-60px lazyloaded" onerror="this.onerror=null;this.src='#';">
                                        <div class="text-truncat-2 pl-3 fs-13 fw-500 text-center">Automobile</div>
                                    </div>


                                </div>
                            </a>
                        </div>


                        <div class="col-sm-2 col-6">
                            <a style="min-height: 110px;" href="#" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                                <div class="row align-items-center no-gutters">
                                    <div class="col-12 text-center">
                                        <img src="frontend/assets/images/category/17.png" data-src="frontend/assets/images/category/17.png" alt="Tools &amp; Instruments" class="img-fluid img h-60px ls-is-cached lazyloaded" onerror="this.onerror=null;this.src='#';">
                                        <div class="text-truncat-2 pl-3 fs-13 fw-500 text-center">Tools &amp; Instruments</div>
                                    </div>


                                </div>
                            </a>
                        </div>


                    </div>
                </div>


            </div>
        </div>
    </section>
    <!-- popular Category end-->


    <!--electronic items start-->
    <section class="mb-4">
        <div class="container">
            <div class="px-2 py-4 px-md-4 py-md-3 bg-white shadow-sm rounded">
                <center><div class="d-flex mb-3 align-items-baseline border-bottom">
                        <div class="col-md-12">
                            <h3 class="h5 fw-700 mb-0" style="text-algin:center">
                                Daily Deal
                            </h3>
                            <h6 class="py-10px"><span class="border-primary border-width-2 pb-0 d-inline-block">Great Saving Every Day</span></h6>
                        </div>
                    </div></center>
                <div class="aiz-carousel gutters-10 half-outside-arrow" data-items="6" data-xl-items="5" data-lg-items="4"  data-md-items="3" data-sm-items="2" data-xs-items="2" data-arrows='true' data-infinite='true'>
                    @foreach($service as $item)
                    <div class="carousel-box">
                        <div class="aiz-card-box border border-light rounded hov-shadow-md my-2 has-transition">
                            <div class="position-relative">
                                <a href="{{route('service_details',$item->slug)}}" class="d-block">
                                    <img class="img-fit lazyload mx-auto h-140px h-md-210px"
                                            src="{{$item->photos}}" data-src="{{$item->photos}}"
                                            alt="{{$item->name}}" onerror="this.onerror=null;this.src='#';">
                                </a>
                                <div class="absolute-top-right aiz-p-hov-icon">


                                    <a href="javascript:void(0)" onclick="showAddToCartModal" data-toggle="tooltip" data-title="Add to cart" data-placement="left">
                                        <i class="las la-shopping-cart"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="p-md-3 p-2 text-left">
                                <div class="fs-15">
                                    <span class="fw-700 text-primary">{{$item->service_charge}}৳</span>
                                </div>
                                <div class="rating rating-sm mt-1">
                                    <i class = 'las la-star'></i><i class = 'las la-star'></i><i class = 'las la-star'></i><i class = 'las la-star'></i><i class = 'las la-star'></i>
                                </div>
                                <h3 class="fw-600 fs-13 text-truncate-2 lh-1-4 mb-0 h-35px">
                                    <a href="{{route('service_details',$item->slug)}}" class="d-block text-reset">{{$item->name}}</a>
                                </h3>

                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!--electronic items end-->

    <!--our product line start-->
    <div class="container py-md-4">
        <div class="col-md-12" style="text-align:center">
            <h2 class="h5 fw-700 mb-3" style="text-algin:center">
                Our Product Line
            </h2>

        </div>
        <div class="px-2 py-4 px-md-4 py-md-3 "  style="border:1px solid #dddd">
            <div class="col-md-12 col-md-12">

                <p style="text-align:justify">
                    <a style="color: #1b1b28;" href="#"><strong> IT Products</strong></a> | <a style="color: #1b1b28;" href="#"><strong> Electronic Items</strong></a> | <a style="color: #1b1b28;" href="#"><strong> Office Equipment</strong></a> |  <a style="color: #1b1b28;" href="#"><strong> Men&#039;s Fashion</strong></a> |    <a style="color: #1b1b28;" href="#"><strong> Women&#039;s Fashion</strong></a> |   <a style="color: #1b1b28;" href="#"><strong> Beauty &amp; Lifestyle</strong></a> |   <a style="color: #1b1b28;" href="#"><strong> Groceries</strong></a> |    <a style="color: #1b1b28;" href="#"><strong> Home Appliance</strong></a> | <a style="color: #1b1b28;" href="#"><strong> Mobile Phone</strong></a> |  <a style="color: #1b1b28;" href="#"><strong> Baby&#039;s Gallery</strong></a> |   <a style="color: #1b1b28;" href="#"><strong> Gift Items</strong></a> |  <a style="color: #1b1b28;" href="#"><strong> Industrial Machineries</strong></a> |   <a style="color: #1b1b28;" href="#"><strong> Battery</strong></a> |  <a style="color: #1b1b28;" href="#"><strong> Health Care</strong></a> |  <a style="color: #1b1b28;" href="#"><strong> Sports &amp; Travel</strong></a> |    <a style="color: #1b1b28;" href="#"><strong> Books</strong></a> |  <a style="color: #1b1b28;" href="#"><strong> Automobile</strong></a> | <a style="color: #1b1b28;" href="#"><strong> Tools &amp; Instruments</strong></a> |  <a style="color: #1b1b28;" href="#"><strong> Spare Parts</strong></a> |  <a style="color: #1b1b28;" href="#"><strong> Pet Care</strong></a> |  </p>

            </div>
        </div>

    </div>
    <!--our product line end-->

@endsection
