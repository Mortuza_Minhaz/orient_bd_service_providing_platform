<!DOCTYPE html>
<html lang="en">
<head>

    <meta name="app-url" content="">
    <meta name="file-base-url" content="">
    <title>Bela Obela | Online marketplace in Bangladesh to buy and sell of all types of products and services</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="index, follow">
    <meta name="description"
          content="Online marketplace in Bangladesh to buy and sell all types of products and services"/>
    <meta name="keywords" content="Online marketplace in Bangladesh to buy and sell all types of products and services">
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Bela O Bela">
    <meta itemprop="description"
          content="Online marketplace in Bangladesh to buy and sell all types of products and services">
    <meta itemprop="image" content="">
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="product">
    <meta name="twitter:site" content="@publisher_handle">
    <meta name="twitter:title" content="Bela O Bela">
    <meta name="twitter:description"
          content="Online marketplace in Bangladesh to buy and sell all types of products and services">
    <meta name="twitter:creator" content="@author_handle">
    <meta name="twitter:image" content="">
    <!-- Open Graph data -->
    <meta property="og:title" content="Bela O Bela"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="https://belaobela.com.bd"/>
    <meta property="og:image" content=""/>
    <meta property="og:description"
          content="Online marketplace in Bangladesh to buy and sell all types of products and services"/>
    <meta property="og:site_name" content="Bela O Bela"/>
    <meta property="fb:app_id" content="732736424282372">
    <!-- Favicon -->
    <link rel="icon" href="https://belaobela.com.bd/public/uploads/all/1ROzFINiBYoE9sQFb60CF8gxkUXoQLv7hQf2bKBF.png">

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap"
        rel="stylesheet">

    <!-- CSS Files -->
    <link rel="stylesheet" href="/frontend/assets/css/vendors.css">
    <link rel="stylesheet" href="/frontend/assets/css/aiz-core.css">
    <link rel="stylesheet" href="/frontend/assets/css/css-linearicons.css">
    <link rel="stylesheet" href="/frontend/assets/css/custom-style.css">

    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <script>
        var AIZ = AIZ || {};
    </script>
    <style>
        body {
            font-family: 'Open Sans', sans-serif;
            font-weight: 400;
        }

        :root {
            --primary: #e3571a;
            --hov-primary: #03275b;
            --soft-primary: rgba(227, 87, 26, 0.15);
        }

        .pre-footer a {
            color: #282828;
        }

        .common-none {
            display: none;
        }
        button.btn.btn-primary.searchbtn {
            padding: 0.67rem 1.2rem;
        }

    </style>
    <link rel="stylesheet" href="/toastr/toastr.min.css">
    @yield('css')

</head>
<body>


<!-- aiz-main-wrapper -->
<div class="aiz-main-wrapper d-flex flex-column">

    <!-- Header -->
    <style>
        @media (max-width: 480px) {
            .d-xs-none {
                display: none !important;
            }
        }

        @media (max-width: 480px) {
            .d-xs-inline-block {
                display: inline-block !important;
            }
        }

        .btn-soft-primary.fw-600 {
            background-color: #ec8d60;
            border-color: #ec8d60;
            color: #ffffff;
        }

        .btn-soft-primary:hover {
            color: #fff !important;
            background-color: #f16522 !important;
            border-color: #f16522 !important;
        }


    </style>

    <script type="text/javascript">
        function showModal() {
            $('#call-for-order').modal('show');
        }

    function doc_keyUp(e) {

    // this would test for whichever key is 40 (down arrow) and the ctrl key at the same time
    if (e.ctrlKey + e.altKey && e.keyCode == '71') {
        alert('hii');
        window.location.href = "{{ route('view_cart')}}";
        // call your function to do the thing
        
    }
}
    // register the handler 
    document.addEventListener('keyup', doc_keyUp, false);
    </script>


    <div class="modal fade" id="call-for-order" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-zoom" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">“আপনি সরাসরি আমাদের কাছে ফোন করে যেকোন পন্যের অর্ডার
                        দিতে 01811-446778 এই নাম্বারে কল করুন”</h3>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

            </div>
        </div>
    </div>

    

    <!-- Header Section Start From Here -->
@include('frontend.include.header')

<!-- Header Section End Here -->

@yield('content')

<!-- footer Start -->
@include('frontend.include.footer')
<!-- footer end -->
    <!--our product line start-->

</div>
<!-- SCRIPTS -->
<script src="/frontend/assets/js/vendor/jquery-3.5.1.min.js"></script>
<script src="/frontend/assets/js/vendors.js"></script>
<script src="/frontend/assets/js/aiz-core.js"></script>
<script src="{{ asset('templateEditor/ckeditor/ckeditor.js') }}"></script>
<!-- Main Activation JS -->

<script src="/toastr/toastr.min.js"></script>
{!! Toastr::message() !!}
@include('backend.include.show_flash_message')
<script>
    $(".menu-content").each(function () {
        var $ul = $(this),
            $lis = $ul.find(".menu-item:gt(4)"),
            isExpanded = $ul.hasClass("expanded");
        $lis[isExpanded ? "show" : "hide"]();

        if ($lis.length > 0) {
            $ul.append(
                $(
                    '<li class="expand">' +
                    (isExpanded ? '<a href="javascript:;"><span><i class="ion-android-remove"></i>Close Categories</span></a>' : '<a href="javascript:;"><span><i class="ion-android-add"></i>More Categories</span></a>') +
                    "</li>"
                ).click(function (event) {
                    var isExpanded = $ul.hasClass("expanded");
                    event.preventDefault();
                    $(this).html(isExpanded ? '<a href="javascript:;"><span><i class="ion-android-add"></i>More Categories</span></a>' : '<a href="javascript:;"><span><i class="ion-android-remove"></i>Close Categories</span></a>');
                    $ul.toggleClass("expanded");
                    $lis.toggle(300);
                })
            );
        }
    });

</script>

@yield('js')
</body>
</html>
