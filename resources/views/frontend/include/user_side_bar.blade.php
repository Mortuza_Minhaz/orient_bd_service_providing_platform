<div class="aiz-user-sidenav-wrap pt-4 position-relative z-1 shadow-sm">
    <div class="absolute-top-right d-xl-none">
        <button class="btn btn-sm p-2" data-toggle="class-toggle" data-target=".aiz-mobile-side-nav" data-same=".mobile-side-nav-thumb">
            <i class="las la-times la-2x"></i>
        </button>
    </div>
    <div class="absolute-top-left d-xl-none">
        <a class="btn btn-sm p-2" href="http://localhost/oriend-active-ecommerce/logout">
            <i class="las la-sign-out-alt la-2x"></i>
        </a>
    </div>
    <div class="aiz-user-sidenav rounded overflow-hidden  c-scrollbar-light">
        <div class="px-4 text-center mb-4">
                  <span class="avatar avatar-md mb-3">
                  <img src="{{Auth::user()->profile_pic}}" class="image rounded-circle" onerror="this.onerror=null;this.src='/assets/images/avatar-place.png';">
                  </span>
            <h4 class="h5 fw-600">Navana Electronics
                <span class="ml-2">
                     <i class="las la-check-circle" style="color:green"></i>
                     </span>
            </h4>
        </div>
        <div class="sidemnenu mb-3">
            <ul class="aiz-side-nav-list metismenu" data-toggle="aiz-side-menu">
                <li class="aiz-side-nav-item">
                    <a href="{{route('user.dashboard')}}" class="aiz-side-nav-link">
                        <i class="las la-home aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">Dashboard</span>
                    </a>
                </li>
                <li class="aiz-side-nav-item">
                    <a href="{{route('service_provider_create')}}" class="aiz-side-nav-link ">
                        <i class="las la-user aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">Manage Profile</span>
                    </a>
                </li>
                <li class="aiz-side-nav-item">
                    <a href="{{route('service_provider.services')}}" class="aiz-side-nav-link">
                        <i class="lab la-sketch aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">My Services</span>
                    </a>
                </li>
                <li class="aiz-side-nav-item">
                    <a href="#" class="aiz-side-nav-link">
                        <i class="las la-hourglass-start aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">Pending Orders</span>
                        <span class="badge badge-inline badge-success">30</span></a>
                </li>
                <li class="aiz-side-nav-item">
                    <a href="{{route('service_provider.orders')}}" class="aiz-side-nav-link">
                        <i class="las la-money-bill aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">Orders</span>
                    </a>
                </li>

                <li class="aiz-side-nav-item">
                    <a href="#" class="aiz-side-nav-link ">
                        <i class="las la-registered aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">Brands</span>
                    </a>
                </li>
                <li class="aiz-side-nav-item">
                    <a href="#" class="aiz-side-nav-link ">
                        <i class="las la-user-edit aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">Requisition for Spare Parts</span>
                    </a>
                </li>
                <li class="aiz-side-nav-item">
                    <a href="#" class="aiz-side-nav-link ">
                        <i class="las la-hand-holding-usd aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">Conveyance bill</span>
                    </a>
                </li>

                <li class="aiz-side-nav-item">
                    <a href="#" class="aiz-side-nav-link ">
                        <i class="lab la-searchengin aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">Investigation</span>
                    </a>
                </li>

                <li class="aiz-side-nav-item">
                    <a href="#" class="aiz-side-nav-link ">
                        <i class="las la-history aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">Payment History</span>
                    </a>
                </li>
                <li class="aiz-side-nav-item">
                    <a href="#" class="aiz-side-nav-link ">
                        <i class="las la-money-bill-wave-alt aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">Money Withdraw</span>
                    </a>
                </li>
                <li class="aiz-side-nav-item">
                    <a href="#" class="aiz-side-nav-link ">
                        <i class="las la-comment aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">Conversations</span>
                    </a>
                </li>
            </ul>
        </div>
        <hr>
        <h4 class="h5 fw-600 text-center">Sold Amount</h4>
        <!-- <div class="sidebar-widget-title py-3">
           <span></span>
           </div> -->
        <div class="widget-balance pb-3 pt-1">
            <div class="text-center">
                <div class="heading-4 strong-700 mb-4">
                    <small class="d-block fs-12 mb-2">Your sold amount (current month)</small>
                    <span class="btn btn-primary fw-600 fs-18">6,600৳</span>
                </div>
                <table class="table table-borderless">
                    <tbody>
                    <tr>
                        <td class="p-1" width="60%">
                            Total Sold:
                        </td>
                        <td class="p-1 fw-600" width="40%">
                            73,600৳
                        </td>
                    </tr>
                    <tr>
                        <td class="p-1" width="60%">
                            Last Month Sold:
                        </td>
                        <td class="p-1 fw-600" width="40%">
                            67,000৳
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <table>
            </table>
        </div>
    </div>
</div>
