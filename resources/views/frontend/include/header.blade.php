
<header class=" sticky-top  z-1020 bg-white border-bottom shadow-sm">
    <div class="position-relative logo-bar-area" style="background: #03275b;">
        <div class="container">
            <div class="col-12 text-right d-none d-xl-block">
                <ul class="list-inline mb-0 list-inline  justify-content-between justify-content-lg-start mb-0">
                    <li class="list-inline-item mr-1 d-lg-inline-block">
                        <a href="" class="btn btn-primary btn-sm shadow-md fs-14 fw-600">
                            Be a Seller
                        </a>
                    </li>
                    <li class="list-inline-item d-lg-inline-block">
                        <a href="{{route('service_provider.registration')}}"
                           class="btn btn-primary btn-sm shadow-md fs-14 fw-600">
                            Be a Service Provider
                        </a>
                    </li>
                </ul>
            </div>
            <div class="d-flex">
                <div class="col-xl-3 col-3  align-items-center d-block d-lg-none"
                     style="padding-right: 0px;padding-left: 0px;">
                    <a href="" class="btn btn-primary btn-xs shadow-md fs-11 fw-600">
                        Be a Seller
                    </a>
                </div>
                <div class="col-xl-4 col-4 ml-2  align-items-center d-block d-lg-none"
                     style="padding-right: 0px;padding-left: 0px;">
                    <a class="d-block py-20 mr-0 text-center" href="{{route('homeView')}}">
                        <img src="/frontend/assets/images/logo/logo.png" alt="logo"
                             class="mw-100 h-50px h-md-72px mb-0 mt-1" height="67">
                    </a>
                </div>
                <div class="col-xl-5 col-5 ml-3 align-items-center d-block d-lg-none"
                     style="padding-right: 0px;padding-left: 0px;">
                    <a href="" class="btn btn-primary btn-xs shadow-md fs-11 fw-600">
                        Be a Service Provider
                    </a>
                </div>
            </div>
            <div class="d-flex align-items-center">
                <div class="col-auto col-xl-3 pl-0 pr-3  d-none d-lg-block align-items-center d-none d-xl-block">
                    <a style="display: none;"><i class="la la-phone la-flip-horizontal fs-16"
                                                 style="font-weight: bold;"></i><strong>
                            Phone +88-02-41031891, For Sales Inquiry +88-01870-728000, For Service Inquiry
                            +88-01870-728001, For Delivery Support +88-01870-728002</strong></a><br>
                    <a style="display: none;"><i class="la la-envelope la-flip-horizontal fs-16" aria-hidden="true"></i>
                        <strong>support@belaobela.com.bd </strong>
                    </a>
                </div>
                <div class="col-xl-5 col-xl-5 col-xs-12  align-items-center d-none d-xl-block">
                    <a class="d-block py-20 mr-1 text-center" href="/">
                        <img src="{{asset('/frontend/assets/images/logo/logo.png')}}" alt="Bela O Bela"
                             class="mw-100 h-60px h-md-62px mb-2 mt-1" height="60">
                    </a>
                </div>
                <div class="col-auto col-xl-5 pl-0 pr-0  col-12 d-flex ">
                    <ul class="list-inline mb-0 list-inline  justify-content-between justify-content-lg-start mb-0 pt-2">
                        @auth
                            @if(isAdmin())
                                <li class="list-inline-item text-white mobileviewcart">
                                    <a href="{{route('home')}}" class="text-reset py-2 d-block">My Panel</a>
                                </li>
                            @else
                                <li class="list-inline-item text-white mobileviewcart">
                                    <a href="{{route('user.dashboard')}}" class="text-reset py-2 d-block">My Panel</a>
                                </li>
                            @endif

                                <li class="list-inline-item text-white mobileviewcart">
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();
                               "
                                       class="text-reset py-2 d-block">Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            @else
                                <li class="list-inline-item text-white mobileviewcart">
                                    <a href="{{route('customer.login')}}" class="text-reset py-2 d-block">Sign in</a>
                                </li>
                                <li class="list-inline-item  text-white mobileviewcart">
                                    <a href="{{route('customer.registration')}}" class="text-reset py-2 d-block">Sign
                                        Up </a>
                                </li>

                                    @endauth
                                <li class="list-inline-item text-white mobileviewcart">
                                    <div class="align-self-stretch" data-hover="dropdown">
                                        <div class="nav-cart-box dropdown h-100" id="cart_items">
                                            <a href="javascript:void(0)"
                                               class="d-flex align-items-center text-reset h-100" data-toggle="dropdown"
                                               data-display="static">
                                                <i class="la la-shopping-cart la-cx"></i>
                                                <span class="badge badge-primary badge-inline badge-pill"
                                                      style="position: relative; top: -5px;right: 5px;">0</span>
                                                <span class="nav-box-text">Cart</span>
                                            </a>
                                            <div
                                                class="dropdown-menu dropdown-menu-right dropdown-menu-lg p-0 stop-propagation">
                                                <div class="text-center p-3">
                                                    <i class="las la-frown la-3x opacity-60 mb-3"></i>
                                                    <h3 class="h6 fw-700">Your Cart is empty</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                    </ul>
                </div>
            </div>
            <div class="d-flex align-items-right">
                <div class="col-auto col-xl-3 pl-0  pr-4 d-flex align-items-center ">
                    <div class="bg-primary  d-none d-xl-block align-self-stretch category-menu-icon-box  ml-0 mr-0"
                         style="width: 100%;">
                        <div class="h-100 d-flex align-items-center " id="category-menu-icon">
                            <a href="#" class="dropdown-toggle   h-40px  pl-2 rounded  c-pointer text-light d-xs-none">
                                <i class="las la-bars" aria-hidden="true"> </i> &nbsp; <b>ALL CATEGORY</b> </a>
                        </div>
                    </div>
                </div>
                <div class="col-10 col-xl-5 pl-0 pr-3 d-flex align-items-center">
                    <ul class="list-inline mb-0 pl-0 mobile-hor-swipe text-center" style="margin: 0 auto;">
                        <li class="list-inline-item mr-0">
                            <a href=""
                               class="btn btn-primary fs-14 px-4 py-2 productservicemobile d-inline-block fw-600 hov-opacity-100 "
                               style="border-top-right-radius: 15px;border-top-left-radius: 15px;">
                                Product
                            </a>
                        </li>
                        <li class="list-inline-item mr-0">
                            <a href=""
                               class="btn fs-14 px-4 py-2 productservicemobile d-inline-block fw-600 hov-opacity-100 "
                               style="background: #fff;border-top-right-radius: 15px;border-top-left-radius: 15px;">
                                Service
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="d-lg-none ml-auto mr-0">
                    <a class="p-2 d-block text-reset" href="javascript:void(0);" data-toggle="class-toggle"
                       data-target=".front-header-search">
                        <i class="las la-search la-flip-horizontal la-2x" style="color: #ffffff"></i>
                    </a>
                </div>
                <div class="flex-grow-1 front-header-search d-flex align-items-center bg-white">
                    <div class="position-relative flex-grow-1">
                        <form action="#" method="GET" class="stop-propagation" style="margin: 0px; padding: 0px">
                            <div class="d-flex position-relative align-items-center">
                                <div class="d-lg-none" data-toggle="class-toggle" data-target=".front-header-search">
                                    {{--sobuj--}}
                                    <button class="btn px-2" type="button"><i class="la la-2x la-long-arrow-left"></i>
                                    </button>
                                </div>
                                <div class="input-group">
                                    <input type="text" class="border-0 border-lg form-control" id="search" name="q"
                                           placeholder="Search Your Product Here....." autocomplete="off">
                                    <div class="input-group-append d-none d-lg-block">
                                        <button class="btn btn-primary searchbtn" type="submit">
                                            <i class="la la-search la-flip-horizontal fs-18" style="color:#ffffff"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div
                            class="typed-search-box stop-propagation document-click-d-none d-none bg-white rounded shadow-lg position-absolute left-0 top-100 w-100"
                            style="min-height: 200px">
                            <div class="search-preloader absolute-top-center">
                                <div class="dot-loader">
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </div>
                            </div>
                            <div class="search-nothing d-none p-3 text-center fs-16">
                            </div>
                            <div id="search-content" class="text-left">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="d-none d-lg-none ml-3 mr-0">
                    <div class="nav-search-box">
                        <a href="#" class="nav-box-link">
                            <i class="la la-search la-flip-horizontal d-inline-block nav-box-icon"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
