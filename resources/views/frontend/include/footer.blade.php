<!-- footer strat-->
<section class=" border-top mt-auto" style="background: #fcf2ed;">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-12">
                <ul class="list-inline mb-0 list-inline  justify-content-between justify-content-lg-start mb-0 pre-footer fs-16">
                    <li class="list-inline-item mr-3 pb-2 pt-2"><a href="#">Terms of Use</a></li>
                    <!-- <li class="list-inline-item mr-3 pb-2 pt-2"><a href="">Return Policy</a></li> -->
                    <li class="list-inline-item mr-3 pb-2 pt-2"><a href="#">Privacy policy</a></li>
                    <li class="list-inline-item mr-3 pb-2 pt-2"><a href="#">FAQ</a></li>

                </ul>

            </div>
        </div>
    </div>
</section>
<section class=" border-top mt-auto pb-1 pt-3" style="background: #fbeae0;">
    <div class="container">
        <div class="row no-gutters">

            <div class="col-lg-2 col-md-6 pb-3">
                <div class="newsletter_text text_white">
                    <h5 style="font-size: 18px;"> Signup to Newsletter</h5>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 pb-2">
                <form method="POST" action="#">
                    <input type="hidden" name="_token" value="">
                    <div class="input-group">
                        <input type="email" class="border-0 border-lg form-control" id="search" name="email" placeholder="Enter you email here..." autocomplete="off">
                        <div class="input-group-append d-none d-lg-block">
                            <button style="color: black;" class="btn btn-primary" type="submit">
                                <b>SUBMIT</b>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-6  col-md-6 pb-2 text-right">
                <img class="img-fit" src="/frontend/assets/images/payment/payment.png" style="width:80%">
            </div>
        </div>
    </div>
</section>
<section class=" py-3 text-white" style="background: #03275b;">
    <div class="container">
        <div class="row">

            <div class="col-lg-4 col-xl-4  text-md-left">
                <div class=" text-md-left mt-4">
                    <h4 class="fs-13 text-uppercase fw-600 border-bottom border-gray-400 pb-2 mb-4 fs-18">
                        About us
                    </h4>
                    <ul class="list-unstyled fs-16">
                        <li class="mb-2">
                            <a class=" text-reset" href="#">
                                Contact Us
                            </a>
                        </li>
                        <li class="mb-2">
                            <a class=" text-reset" href="#">
                                How to Order
                            </a>
                        </li>
                        <li class="mb-2">
                            <a class=" text-reset" href="#">
                                Replace &amp; Refund
                            </a>
                        </li>
                        <li class="mb-2">
                            <a class=" text-reset" href="#">
                                Delivery &amp; Payment
                            </a>
                        </li>
                        <li class="mb-2">
                            <a class=" text-reset" href="#">
                                All Showroom List
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-xl-4  text-md-left">
                <div class="text-md-left mt-4">
                    <h4 class="fs-13 text-uppercase fw-600 border-bottom border-gray-400 pb-2 mb-4 fs-18">
                        Top Categories
                    </h4>
                    <ul class="list-unstyled fs-16">

                        <li class="mb-2">
                            <a class="  text-reset" href="#">
                                IT Products
                            </a>
                        </li>
                        <li class="mb-2">
                            <a class="  text-reset" href="#">
                                Electronic Items
                            </a>
                        </li>
                        <li class="mb-2">
                            <a class="  text-reset" href="#">
                                Office Equipment
                            </a>
                        </li>
                        <li class="mb-2">
                            <a class="  text-reset" href="#">
                                Men&#039;s Fashion
                            </a>
                        </li>
                        <li class="mb-2">
                            <a class="  text-reset" href="#">
                                Women&#039;s Fashion
                            </a>
                        </li>


                    </ul>
                </div>
            </div>
            <div class="col-lg-4 ml-xl-auto col-md-4 mr-0">
                <!--
                <div class="text-center text-md-left mt-4">
                    <h4 class="fs-13 text-uppercase fw-600 border-bottom border-gray-900 pb-2 mb-4">
                        Contact Info
                    </h4>
                    <ul class="list-unstyled">
                        <li class="mb-2">
                            <span class="d-block opacity-30">Address:</span>
                            <span class="d-block opacity-70">8/2, Motalib Tower (1st floor) Flat-2C, Paribag Dhaka-1000, Bangladesh</span>
                        </li>
                        <li class="mb-2">
                            <span class="d-block opacity-30">Phone:</span>
                            <span class="d-block opacity-70">01841-695 -695, 0181-1446778, 0184-7327607</span>
                        </li>
                        <li class="mb-2">
                            <span class="d-block opacity-30">Email:</span>
                            <span class="d-block opacity-70">
                           <a href="mailto:support@orient.com.bd" class="text-reset">support@orient.com.bd</a>
                        </span>
                        </li>
                    </ul>
                </div>-->
                <div class=" text-md-left mt-4">
                    <h4 class="fs-13 text-uppercase fw-600 border-bottom border-gray-400 pb-2 mb-4 fs-18">
                        KEEP IN TOUCH
                    </h4>
                    <ul class="list-inline my-3 my-md-0 social colored fs-16">
                        <li class="list-inline-item">
                            <a href="https://facebook.com/belaobela.com.bd" target="_blank" class="facebook"><i class="lab la-facebook-f"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://twitter.com/Bela_Obela" target="_blank" class="twitter"><i class="lab la-twitter"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.instagram.com/belaobela.com.bd" target="_blank" class="instagram"><i class="lab la-instagram"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.youtube.com/channel/UCTD1TS761RVrX9H9OSJTogg?sub_confirmation=1" target="_blank" class="youtube"><i class="lab la-youtube"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.linkedin.com/company/belaobela" target="_blank" class="linkedin"><i class="lab la-linkedin-in"></i></a>
                        </li>
                    </ul>
                </div>
                <div class=" text-md-left pt-3 fs-16">
                    @2021 BELA OBELA                </div>
                <div class=" text-md-left pt-3 fs-16">
                    <div class="text-center text-md-left mt-4">
                        <h4 class="fs-13 text-uppercase fw-600 border-bottom border-gray-400 pb-2 mb-4">
                            Contact Info
                        </h4>
                        <ul class="list-unstyled">
                            <li class="mb-2">
                                <span class="d-block">Address:</span>
                                <span class="d-block opacity-70">Concord Tower; Suit no: 1401, 113 Kazi Nazrul Islam Avenue, Ramna, Dhaka</span>
                            </li>
                            <li class="mb-2">
                                <span class="d-block ">Phone: <span class="opacity-70"> Phone +88-02-41031891,  For Sales Inquiry +88-01870-728000, For Service Inquiry +88-01870-728001,  For Delivery Support +88-01870-728002</span> </span>

                            </li>
                            <li class="mb-2">
                                <span class="d-block ">Email: <a href="mailto:support@belaobela.com.bd" class="text-reset opacity-70">support@belaobela.com.bd</a></span>



                            </li>
                        </ul>
                    </div>
                </div>



            </div>

        </div>
    </div>
</section>
<div class="aiz-mobile-bottom-nav d-xl-none fixed-bottom bg-white shadow-lg border-top">
    <div class="d-flex justify-content-around align-items-center">
        <a href="https://belaobela.com.bd" class="text-reset flex-grow-1 text-center py-3 border-right bg-soft-primary">
            <i class="las la-home la-2x"></i>
        </a>
        <a href="https://belaobela.com.bd/categories" class="text-reset flex-grow-1 text-center py-3 border-right ">
            <span class="d-inline-block position-relative px-2">
                <i class="las la-list-ul la-2x"></i>
            </span>
        </a>
        <a href="https://belaobela.com.bd/cart" class="text-reset flex-grow-1 text-center py-3 border-right ">
            <span class="d-inline-block position-relative px-2">
                <i class="las la-shopping-cart la-2x"></i>
                                    <span class="badge badge-circle badge-primary position-absolute absolute-top-right" id="cart_items_sidenav">0</span>
                            </span>
        </a>
        <a href="https://belaobela.com.bd/users/login" class="text-reset flex-grow-1 text-center py-2">
                <span class="avatar avatar-sm d-block mx-auto">
                    <img src="https://belaobela.com.bd/public/assets/img/avatar-place.png">
                </span>
        </a>
    </div>
</div>

<section class=" border-top mt-auto" style="background: #101010;">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-12">
                <ul class="list-inline mb-0 list-inline  justify-content-between justify-content-lg-start mb-0 pre-footer fs-16">
                    <li class="list-inline-item mr-3 pb-2 pt-2"><a style="color: #c3bebe; font-size: 15px; font-style: italic;">Developed By</a></li>
                    <!-- <li class="list-inline-item mr-3 pb-2 pt-2"><a href="">Return Policy</a></li> -->
                    <li class="list-inline-item mr-3 pb-2 pt-2"><a style=" font-family: fantasy; background: -webkit-linear-gradient(#17ff00 20%,  red); -webkit-background-clip: text; -webkit-text-fill-color: transparent; font-size: 14px;" href="https://www.amarbebsha.com/" target="_blank">Amar Bebsha Limited</a></li>

                </ul>

            </div>
        </div>
    </div>
</section>

<!-- footer end-->