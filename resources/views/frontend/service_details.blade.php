@extends('frontend.layouts.app')
@section('title', 'Home')
@section('meta_keywords','home,home page')
@section('meta_description', 'home page of test')
@section('css')
    <style>

        .short-description ul {
            list-style: none;
            padding: 3px;
        }

        .btn-soft-primary.fw-600 {
            background-color: #4c9cf3;
            color: #ffffff;
        }

        .btn-soft-primary:hover {
            color: #fff !important;
            background-color: #277af3 !important;
            border-color: #277af3 !important;
        }

        .btn-info {
            color: #fff;
            background-color: #b5e61d !important;
            border-color: #b5e61d !important;
        }

        .btn-info, .btn-soft-info:hover, .btn-outline-info:hover {
            background-color: #b5e61d !important;
            border-color: #b5e61d !important;
            color: var(--white) !important;
        }

        .btn-info:hover {
            color: #fff !important;
            background-color: #8db709 !important;
            border-color: #8db709 !important;
        }

        .btn-info {
            color: #fff;
            background-color: #4fcff1;
        !important;
            border-color: #4fcff1;
        !important;
        }

        .btn-success, .btn-soft-success:hover, .btn-outline-success:hover {
            background-color: #4fcff1;
        !important;
            border-color: #4fcff1;
        !important;
            color: var(--white) !important;
        }

        .btn-success:hover {
            color: #fff !important;
            background-color: #31a1bf !important;
            border-color: #31a1bf !important;
        }

        .btn-warning, .btn-soft-warning:hover, .btn-outline-warning:hover {
            background-color: #efc818;
        !important;
            border-color: #efc818;
        !important;
            color: var(--white) !important;
        }

        .btn-secondary, .btn-soft-secondary:hover, .btn-outline-secondary:hover {
            background-color: #f84581;
        !important;
            border-color: #f84581;
        !important;
            color: var(--white) !important;
        }

        .btn-secondary:hover {
            color: #fff !important;
            background-color: #df2c68 !important;
            border-color: #df2c68 !important;
        }

        .btn-warning:hover {
            color: #fff !important;
            background-color: #dcb80f !important;
            border-color: #dcb80f !important;
        }

        .btn-danger, .btn-soft-danger:hover, .btn-outline-success:hover {
            background-color: #a996e7;
        !important;
            border-color: #a996e7;
        !important;
            color: var(--white) !important;
        }

        .btn-danger:not(:disabled):not(.disabled).active, .btn-danger:not(:disabled):not(.disabled):active, .show > .btn-danger.dropdown-toggle {
            color: #fff;
            background-color: #a996e7 !important;
            border-color: #a996e7 !important;
        }

        .btn-danger:hover {
            color: #fff !important;
            background-color: #846dd0 !important;
            border-color: #846dd0 !important;
        }

        .btn-sm {
            padding: 5px;
            font-size: 12px;
        }

        @media (min-width: 320px) and (max-width: 640px) {
            .btn-sm {
                padding: 5px;
                font-size: 12px;
            }

            button.btn.btn-sm.btn-soft-primary.fw-600 {
                width: 100%;
                margin-bottom: 3px;
            }

            button.btn.btn-sm.btn-info.fw-600 {
                width: 100%;
                margin-bottom: 3px;
            }

            button.btn.btn-sm.btn-success.fw-600 {
                width: 100%;
                margin-bottom: 3px;
            }

            button.btn.btn-sm.btn-danger.add-to-cart.fw-600 {
                width: 100%;
                margin-bottom: 3px;
            }

            button.btn.btn-sm.btn-primary.buy-now.fw-600 {
                width: 100%;
                margin-bottom: 3px;
            }

            button.btn.btn-sm.btn-warning.fw-600 {
                width: 100%;
                margin-bottom: 3px;
            }

            /*nav tab start*/
            a.nav-item.nav-link.btn.btn-sm.btn-danger.fw-600.mr-1.active.text-reset {
                width: 100%;
                margin-bottom: 2px;
            }

            a.nav-item.nav-link.btn.btn-sm.btn-success.fw-600.mr-1.text-reset {
                width: 100%;
                margin-bottom: 2px;
            }

            a.nav-item.nav-link.btn.btn-sm.btn-info.fw-600.mr-1.text-reset {
                width: 100%;
                margin-bottom: 2px;
            }

            a.nav-item.nav-link.btn.btn-sm.btn-warning.fw-600.mr-1.text-reset {
                width: 100%;
                margin-bottom: 2px;
            }

            a.nav-item.nav-link.btn.btn-sm.btn-primary.fw-600.mr-1 {
                width: 100%;
                margin-bottom: 2px;
            }

            a.nav-item.nav-link.btn.btn-sm.btn-secondary.fw-600.mr-1.text-reset {
                width: 100%;
                margin-bottom: 2px;
            }

            .div1, .div2 {
                display: inline-block !important;
            }
        }

        /*@media (min-width: 641px) and (max-width: 1024px) {
            button.btn.btn-sm.btn-warning.buy-now.fw-600 {
                margin-top: 5px;
            }
        }
        @media (min-width: 1030px) and (max-width: 1366px) {
            button.btn.btn-sm.btn-warning.buy-now.fw-600 {
                margin-top: 5px;
            }
        }*/


    </style>
@section('content')
    <section class="pt-3">
        <div class="container">
            <div class="row">
                <div class="col-xl-9">
                    <ul class="breadcrumb bg-transparent p-0">
                        <li class="breadcrumb-item opacity-50">
                            <a class="text-reset" href="/">Home</a>
                        </li>
                        <li class="breadcrumb-item fw-600  text-dark">
                            <a class="text-reset" href="">"All Products"</a>
                        </li>
                        <li class="breadcrumb-item fw-600  text-dark">
                            <a class="text-reset" href="#">Sony KD-65X9500H 65" 4K HDR Full Array LED TV</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="mb-4 pt-1">
        <div class="container">
            <div class="bg-white shadow-sm rounded p-3">
                <div class="row">
                    <div class="col-xl-5 col-lg-6 mb-0">
                        <div class="z-3 row gutters-10">
                            <img class="img-fit lazyload mx-auto h-140px h-md-210px"
                                 src="{{$service->photos}}" data-src="{{$service->photos}}"
                                 alt="{{$service->name}}" onerror="this.onerror=null;this.src='#';">
                        </div>

                    </div>
                    <div class="col-xl-7 col-lg-6">
                        <div class="text-left">
                            <h1 class="mb-2 fs-20 fw-600">
                                {{$service->name}}
                            </h1>
                            <div class="row align-items-center">
                                <div class="col-6">
                        <span class="rating">
                        <i class="las la-star"></i><i class="las la-star"></i><i class="las la-star"></i><i
                                class="las la-star"></i><i class="las la-star"></i>
                        </span>
                                    <span class="ml-1 opacity-50">(0 Reviews)</span>
                                </div>
                                <div class="col-6">
                        <span>
                        <a href="#">
                        <img
                            src="https://belaobela.com.bd/public/uploads/all/ASMS3dcpKUChGdvAIYc2D5rNusYMXIzMrbpfDEV9.png"
                            alt="sony" height="24">
                        </a>
                        </span>
                                    <span class="badge badge-md badge-inline badge-pill badge-success">In stock</span>
                                </div>
                            </div>
                            {{--<div class="row align-items-center">
                                <div class="col-12">
                                    <h1 class="mb-2 fs-16 fw-600">
                                        Product Id: 2021.03.06.132.996
                                    </h1>
                                </div>
                            </div>--}}
                            <div class="row no-gutters mt-3">
                                <div class="col-sm-2">
                                    <div class="my-2">Price:</div>
                                </div>
                                <div class="col-sm-10">
                                    <div class="fs-20 opacity-60">
                                        <del>
                                            {{$service->service_charge}}৳
                                            <span>/pcs</span>
                                        </del>
                                    </div>
                                </div>
                            </div>
                            <div class="row no-gutters my-2">
                                <div class="col-sm-2">
                                    <div class="">Discount Price:</div>
                                </div>
                                <div class="col-sm-10">
                                    <div class="">
                                        <strong class="h5 fw-600 text-primary">
                                            {{$service->service_charge}}৳
                                        </strong>
                                        <span class="opacity-70">/pcs</span>
                                    </div>
                                </div>
                            </div>
                            <form id="option-choice-form">
                                <input type="hidden" name="_token" value="j0VzkysOPmjnNBRuacvEDsNvUcuLYYch3sgEK0bH">
                                <input type="hidden" name="id" value="996">
                                <!-- Quantity + Add to cart -->
                                <div class="row no-gutters pb-3" id="chosen_price_div">
                                    <div class="col-sm-2">
                                        <div class="my-2">Total Price:</div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="product-price mt-1">
                                            <strong id="chosen_price"
                                                    class="h5 fw-600 text-primary">{{$service->service_charge}}৳</strong>
                                        </div>
                                    </div>
                                    <!-- <div class="col-sm-8 col-xs-8 mt-1">
                                       <button type="button" class="btn btn-sm btn-success fw-600">
                                           <span class="d-md-inline-block">Monthly EMI BDT 5,224</span>
                                       </button>
                                       </div> -->
                                </div>

                                <div class="mt-5">
                                    <!-- <div class="col-auto">
                                       <small class="mr-2 opacity-50">Sold By: </small><br>
                                                                   <a href="https://belaobela.com.bd/shop/Good-Electronics-" class="text-reset">Good Electronics</a>
                                                                       </div> -->
                                    <button type="button" class="btn btn-sm btn-warning fw-600">
                                        Call Now
                                    </button>
                                    <button class="btn btn-sm btn-soft-primary fw-600" onclick="show_chat_modal()"> Send
                                        Message
                                    </button>
                                    <button type="button" class="btn btn-sm btn-success fw-600" onclick="addToCompare(996)">
                                        <span class="d-md-inline-block"> Request for investigation</span>
                                    </button>
                                    <button type="button" class="btn btn-sm btn-danger add-to-cart fw-600"
                                            > <a style="color:white;" href="{{route('view_cart')}}">
                                        <span class="d-md-inline-block"> Add to cart</span>
                                    </a></button>
                                    <button type="button" class="btn btn-sm btn-primary buy-now fw-600">
                                        Order Now
                                    </button>
                                
                                </div>
                            </form>
                        </div>

                        <!--  modal start-->

                        <!--  modal start-->


                    </div>

                </div>

            </div>
        </div>
    </section>
@endsection
