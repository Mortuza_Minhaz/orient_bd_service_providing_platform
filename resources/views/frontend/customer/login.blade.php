@extends('frontend.layouts.app')
@section('title', 'Home')
@section('meta_keywords','home,home page')
@section('meta_description', 'home page of test')
@section('js')
    <script src="/frontend/assets/js/jquery.smartCart.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            // Initialize Smart Cart
            $('#smartcart').smartCart();
        });

    </script>
@endsection


@section('css')

    <link rel="stylesheet" type="text/css" href="/frontend/assets/css/smart_cart.min.css">
@endsection


@section('content')


<div class="offcanvas-overlay"></div>
    <!-- Breadcrumb Area Start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcrumb-content">
                            <ul class="nav">
                                <li><a href="index.html">Home</a></li>
                                <li>Login / Register</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- Breadcrumb Area End-->
            <!-- login area start -->
            <div class="login-register-area mtb-60px">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 col-md-12 ml-auto mr-auto">
                            <div class="login-register-wrapper">
                                <div class="login-register-tab-list nav">
                                    <a class="active" data-toggle="tab" href="#lg1">
                                        <h4>login</h4>
                                    </a>
                                    <a data-toggle="tab" href="#lg2">
                                        <h4>register</h4>
                                    </a>
                                </div>
                                <div class="tab-content">
                                    <div id="lg1" class="tab-pane active">
                                        <div class="login-form-container">
                                            <div class="login-register-form">
                                                <form action="{{route('customer_login')}}" method="post">
                                                    @csrf
                                                    <input type="text" name="email" placeholder="Username" />
                                                    <input type="password" name="password" placeholder="Password" />
                                                    <div class="button-box">
                                                        <div class="login-toggle-btn">
                                                            <input type="checkbox" />
                                                            <a class="flote-none" href="javascript:void(0)">Remember me</a>
                                                            <a href="#">Forgot Password?</a>
                                                        </div>
                                                        <button type="submit"><span>Login</span></button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="lg2" class="tab-pane">
                                        <div class="login-form-container">
                                            <div class="login-register-form">
                                                <form action="{{route('customer_register')}}" method="post">
                                                    @csrf
                                                    <input type="text" name="name" placeholder="Username" rquired/>
                                                    <input type="password" name="password" placeholder="Password" rquired />
                                                    <input name="email" placeholder="Email" type="email" rquired />
                                                    <div class="button-box">
                                                        <button type="submit"><span>Register</span></button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- login area end -->






@endsection
