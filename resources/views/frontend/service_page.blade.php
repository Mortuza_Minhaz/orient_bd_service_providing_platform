@extends('frontend.layouts.app')
@section('title', 'Home')
@section('meta_keywords','home,home page')
@section('meta_description', 'home page of test')
@section('js')
<script src="/frontend/assets/js/jquery.smartCart.js" type="text/javascript"></script>
<script>
$(document).ready(function() {
    // Initialize Smart Cart
    $('#smartcart').smartCart();
});
</script>
<script>
$(".add_item").click(function() {
    var id = $(this).attr('id');
    var same = $('#same_' + id).val();

    var category_price_id = $('#category_price_id_' + id).val();
    var title = $('#title_' + id).val();
    var service_price = $('#service_price_' + id).val();
    var service_name = $('#service_name_' + id).val();
    var unit = $('#unit_' + id).val();
    var category_id = $('#category_id_' + id).val();
    if (id != same) {
        // $.ajax({
        //     type:"POST",
        //     url: '{{ route('addto_cart') }}',
        //     data:  { "_token": "{{ csrf_token() }}",'category_id': category_price_id,'price':service_price,'name':title},
        //     success: function(data){
        //         console.log(data);
        //     }
        // });
        tab = '<div class="sc-cart-item list-group-item-servce new_item' + id + '"">' +

            '<button type="button" del_id="' + id + '" class="sc-cart-remove delete_item">' +
            '<i class="fa fa-backspace">' + '</i>' +
            '</button>' +
            '<input type="hidden" name="same" id="same_' + id + '" value="' + id + '"/>' +
            '<input type="hidden" name="category_id[]"  value="' + category_id + '"/>' +
            '<input type="hidden" name="category_price_id[]"  value="' + category_price_id + '"/>' +
            '<input type="hidden" name="service_price[]"  value="' + service_price + '"/>' +
            '<input type="hidden" name="title[]"  value="' + title + '"/>' +
            '<h4 class="list-group-item-heading">' + title +
            '</h4>' +
            '<p class="list-group-item-text">' + service_name +
            '</p>' +

            '<div class="sc-cart-item-summary">' +
            '<span class="sc-cart-item-price" id="price_' + id + '" >' + service_price +
            '</span>' + '*' +
            '<input type="number" min="1" name="qty[]" max="3" onchange="qtyOnchange(this.value,this.id)" class="sc-cart-item-qty qty_up" id="qtyUp_' +
            id + '" value="1" />' + '=' +
            '<span class="sc-cart-item-amount service-price" id="service_price_total_' + id + '">' +
            service_price +
            '</span>' +
            '</div >' +
            '</div >';
        $("#new_item").append(tab);


        calcutate_one_product_total_price();
    } else {
        alert("Already Add this Service");
    }

});

function calcutate_one_product_total_price() {

    var final_anont = 0;
    $.each($('.service-price'), function() {
        var price = parseFloat($(this).html());

        final_anont += price;
    });

    $("#total").html(parseFloat(final_anont).toFixed(2));
}
$(document).on('keyup', '.qty_up', function() {
    var qty = parseFloat($(this).val());
    var id_arrt = $(this).attr('id');
    var id_array = id_arrt.split("_");

    var price = parseFloat($('#price_' + id_array[1]).html());
    var tt_ptice = parseFloat(price * qty).toFixed(2);
    console.log(tt_ptice);
    $('#service_price_total_' + id_array[1]).html(tt_ptice);
    calcutate_one_product_total_price();
});
$(document).on('click', '.delete_item', function() {

    var id = $(this).attr("del_id");

   
    $.ajax({
        type:"POST",
        url: '{{ route('delete_item') }}',
        data:  {"_token": "{{ csrf_token() }}",'id': id},
        success: function(data){
            console.log(data);
        }
    });
    $('.new_item' + id).remove();
    calcutate_one_product_total_price();

});

function qtyOnchange(qty, id) {
    var qty = parseFloat(qty);
    //var id_arrt = $(this).attr('id');
    var id_array = id.split("_");

    var price = parseFloat($('#price_' + id_array[1]).html());
    var tt_ptice = parseFloat(price * qty).toFixed(2);
    console.log(tt_ptice);
    $('#service_price_total_' + id_array[1]).html(tt_ptice);
    calcutate_one_product_total_price();
}
$(document).on('click', '.delete_total', function() {

    $('#new_item').html("");
    $.ajax({
        type:"POST",
        url: '{{ route('delete_cart') }}',
        data:  { "_token": "{{ csrf_token() }}"},
        success: function(data){
            location.reload();
        }
    });
    calcutate_one_product_total_price();

});
</script>
@endsection

@section('css')
<style>
.modal-backdrop {
    position: fixed;
    top: 0;
    left: 0;
    z-index: 1 !important;
    width: 100vw;
    height: 100vh;
    background-color: #000;
}

</style>

<link rel="stylesheet" type="text/css" href="/frontend/assets/css/smart_cart.min.css">
@endsection


@section('content')


<!-- Service Area Start -->
<div class="container-fluid" style="padding-right: 0px; padding-left: 0px;">

    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron bg-cover text-white"
                style="height: 100%; width: auto;background-image: linear-gradient(to bottom, rgba(0,0,0,0.6) 0%,rgba(0,0,0,0.6) 100%), url(/frontend/assets/images/background/.jpg)">
                <div class="container">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                            <li class="breadcrumb-item"><a href="index.html">AC Repair Services</a></li>
                            <li class="breadcrumb-item active" aria-current="page">AC Servicing</li>
                        </ol>
                    </nav>

                    <h3 class="display-4 h3servicing" style="padding-left: 14px;">AC Servicing <span
                            class="badge-img pt-3"><img src="/frontend/assets/images/badge.png"></span></h3>
                    <div class="typo-content typo-buttons pt-4 mb-3">
                        <a href="#" class="btn btn-primary mr-3"><i class="fa fa-star" aria-hidden="true"></i>4.65
                            out of 5 </a><span>(16485 ratings on 5 services)</span>
                    </div>
                    <div class="col-md-5">
                        <div class="typography-box">
                            <div class="typo-content">
                                <ul>
                                    <li class="liststyle"><i class="fa fa-check-circle" aria-hidden="true"
                                            style="margin-right:3px;"></i>Trusted & Reliable AC
                                        Technicians
                                    </li>
                                    <li class="liststyle"><i class="fa fa-check-circle" aria-hidden="true"
                                            style="margin-right:3px;"></i>7 Days Post Service
                                        Warranty
                                    </li>
                                    <li class="liststyle"><i class="fa fa-check-circle" aria-hidden="true"
                                            style="margin-right:3px;"></i>Well-equipped &
                                        Well-prepared Specialists to Prevent COVID-19
                                    </li>

                                </ul>


                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container   -->
            </div>

        </div>

    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-3" id="myScrollspy">
            <div class="list-group">
                <a class="list-group-item list-group-item-action active" href="#section1">Service Overview</a>
                <a class="list-group-item list-group-item-action" href="#section2">FAQ</a>
                <!-- <a class="list-group-item list-group-item-action" href="#section3">Reviews</a> -->
                <a class="list-group-item list-group-item-action" href="#section4">Details</a>

            </div>
        </div>
        <div class="col-sm-7">
            <div id="section1" class="serviceoverview">
                <h3>Overview of AC Repair Service</h3>
                <div class="row">
                    <div class="col-md-5">
                        <h4 class="sub-title">What's Included?</h4>
                        <div class="typography-box">
                            <div class="typo-content">
                                <ul class="list-unordered">
                                    <li class="liststyle2 ">
                                        Only service charge
                                    </li>
                                    <li class="liststyle2">
                                        7 Days service warranty
                                    </li>

                                </ul>

                            </div>
                        </div>


                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <h4 class="sub-title">What's Excluded?</h4>
                        <div class="typography-box">
                            <div class="typo-content">
                                <ul class="list-unordered">
                                    <li class="liststyle2">Price of
                                        materials or parts
                                    </li>
                                    <li class="liststyle2">Transportation
                                        cost for carrying new materials/parts
                                    </li>
                                    <li class="liststyle2">Warranty givenby manufacturer
                                    </li>

                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h4 class="sub-title">Available Services</h4>
                        <div class="typography-box">
                            <div class="typo-content">
                                <ul class="list-unordered">
                                    <li class="liststyle2">AC Basic Servicing</li>
                                    <li class="liststyle2">AC Master Service</li>
                                    <li class="liststyle2">AC Water Drop Solution</li>
                                    <li class="liststyle2">AC Jet Wash</li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div id="section2" class="faq">
                <h3>FAQ</h3>
                <!--section start-->
                <section id="content2" class="gray-area">
                    <div class="col-sm-12 container" style="padding-right: 0px; padding-left: 0px">
                        <div class="container-toggle box" id="accordion1">
                            <div class="panel style1">
                                <h4 class="panel-title"><a href="#acc1" data-toggle="collapse" data-parent="#accordion1"
                                        data-abc="true" aria-expanded="true" class="">Do I have to pay any charge
                                        if I don’t take any service?</a></h4>
                                <div class="panel-collapse collapse show" id="acc1" aria-expanded="true" style="">
                                    <div class="panel-content">
                                        <p>If you don’t avail any services for your AC after our Service
                                            Provider send a technician at your doorstep then you only have to
                                            pay the visiting charge which is BDT 100.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel style1">
                                <h4 class="panel-title"><a class="collapsed" href="#acc2" data-toggle="collapse"
                                        data-parent="#accordion1" data-abc="true" aria-expanded="false">Do I have to pay
                                        advance money
                                        before availing your service?</a></h4>
                                <div class="panel-collapse collapse" id="acc2" aria-expanded="false">
                                    <div class="panel-content">
                                        <p>Of course not! After service completion you will receive a text on your
                                            mobile from Sheba.xyz then you have to pay through Online or Cash on
                                            Delivery.</p>
                                    </div><!-- end content -->
                                </div>
                            </div>
                            <div class="panel style1">
                                <h4 class="panel-title"><a class="collapsed" href="#acc3" data-toggle="collapse"
                                        data-parent="#accordion1" data-abc="true" aria-expanded="false">Is this only
                                        for household AC?</a></h4>
                                <div class="panel-collapse collapse" id="acc3" aria-expanded="false">
                                    <div class="panel-content">
                                        <p>Definitely not! As long as you want to avail this service for your AC
                                            then you can order for your office Air Conditioners too.</p>
                                    </div><!-- end content -->
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- <div class="container mt-5 mb-5">
                    <div class="row">
                        <div class="col-md-8 ">
                            <h3 class="tittle">How to order</h3>
                            <ul class="timeline">
                                <li>
                                    <h4>Select service</h4>
                                    <p>From the category, select the service you are looking for.</p>
                                </li>
                                <li>
                                    <h4>Book your schedule</h4>
                                    <p>Select your convenient time slot.</p>
                                </li>
                                <li>
                                    <h4>Place order</h4>
                                    <p>Confirm your order by clicking ‘Place order’</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div> -->

                <!--Section ends-->
            </div>
            <hr>
            <!-- <div id="section3">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="rating-block">
                                <h4>Average user rating</h4>
                                <h2 class="bold padding-bottom-7">4.3
                                    <small>/ 5</small>
                                </h2>
                                <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                                    <i class="fa fa-star fafarating" aria-hidden="true"></i>
                                </button>
                                <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                                    <i class="fa fa-star fafarating" aria-hidden="true"></i>
                                </button>
                                <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                                    <i class="fa fa-star fafarating" aria-hidden="true"></i>
                                </button>
                                <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                                    <i class="fa fa-star fafarating" aria-hidden="true"></i>
                                </button>
                                <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                                    <i class="fa fa-star fafarating" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <h4>Rating breakdown</h4>
                            <div class="float-left">
                                <div class="float-left" style="width:35px; line-height:1;">
                                    <div style="height:9px; margin:5px 0;">5 <i class="fa fa-star fafaratingblack"
                                            aria-hidden="true"></i></div>
                                </div>
                                <div class="float-left" style="width:180px;">
                                    <div class="progress" style="height:9px; margin:8px 0;">
                                        <div class="progress-bar progress-bar-success" role="progressbar"
                                            aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: 1000%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="float-right" style="margin-left:10px;">1</div>
                            </div>
                            <div class="float-left">
                                <div class="float-left" style="width:35px; line-height:1;">
                                    <div style="height:9px; margin:5px 0;">4 <i class="fa fa-star fafaratingblack"
                                            aria-hidden="true"></i></div>
                                </div>
                                <div class="float-left" style="width:180px;">
                                    <div class="progress" style="height:9px; margin:8px 0;">
                                        <div class="progress-bar progress-bar-primary" role="progressbar"
                                            aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="float-right" style="margin-left:10px;">1</div>
                            </div>
                            <div class="float-left">
                                <div class="float-left" style="width:35px; line-height:1;">
                                    <div style="height:9px; margin:5px 0;">3 <i class="fa fa-star fafaratingblack"
                                            aria-hidden="true"></i></div>
                                </div>
                                <div class="float-left" style="width:180px;">
                                    <div class="progress" style="height:9px; margin:8px 0;">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="3"
                                            aria-valuemin="0" aria-valuemax="5" style="width: 60%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="float-right" style="margin-left:10px;">0</div>
                            </div>
                            <div class="float-left">
                                <div class="float-left" style="width:35px; line-height:1;">
                                    <div style="height:9px; margin:5px 0;">2 <i class="fa fa-star fafaratingblack"
                                            aria-hidden="true"></i></div>
                                </div>
                                <div class="float-left" style="width:180px;">
                                    <div class="progress" style="height:9px; margin:8px 0;">
                                        <div class="progress-bar progress-bar-warning" role="progressbar"
                                            aria-valuenow="2" aria-valuemin="0" aria-valuemax="5" style="width: 40%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="float-right" style="margin-left:10px;">0</div>
                            </div>
                            <div class="float-left">
                                <div class="float-left" style="width:35px; line-height:1;">
                                    <div style="height:9px; margin:5px 0;">1 <i class="fa fa-star fafaratingblack"
                                            aria-hidden="true"></i></div>
                                </div>
                                <div class="float-left" style="width:180px;">
                                    <div class="progress" style="height:9px; margin:8px 0;">
                                        <div class="progress-bar progress-bar-danger" role="progressbar"
                                            aria-valuenow="1" aria-valuemin="0" aria-valuemax="5" style="width: 20%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="float-right" style="margin-left:10px;">0</div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-7">
                            <div class="review-block">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <img src="/frontend/assets/images/review.png" class="img-rounded reviewimage">
                                        <div class="review-block-name">
                                            <p class="reviewsafetyp">Rajat Bhowmik</p>
                                            <p class="safetyp">21 February, 2021<br />1 day ago</p>
                                        </div>
                                        <div class="review-block-date"></div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="review-block-rate">
                                            <button type="button" class="btn btn-warning btn-xs"
                                                aria-label="Left Align">
                                                <i class="fa fa-star fafarating" aria-hidden="true"></i>
                                            </button>
                                            <button type="button" class="btn btn-warning btn-xs"
                                                aria-label="Left Align">
                                                <i class="fa fa-star fafarating" aria-hidden="true"></i>
                                            </button>
                                            <button type="button" class="btn btn-warning btn-xs"
                                                aria-label="Left Align">
                                                <i class="fa fa-star fafarating" aria-hidden="true"></i>
                                            </button>
                                            <button type="button" class="btn btn-default btn-grey btn-xs"
                                                aria-label="Left Align">
                                                <i class="fa fa-star fafarating" aria-hidden="true"></i>
                                            </button>
                                            <button type="button" class="btn btn-default btn-grey btn-xs"
                                                aria-label="Left Align">
                                                <i class="fa fa-star fafarating" aria-hidden="true"></i>
                                            </button>
                                        </div>

                                        <div class="review-block-description">
                                            <p class="safetyp">service was awesome the people came were very well
                                                mannered. Highly Recommended</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div> -->

            <div id="section4">

                <div class="row">
                    <div class="col-md-12 servicedetails">
                        <h3>Details</h3>
                        <p class="safetyp">At Sheba.xyz you can hire expert AC repair service near you. Our
                            professional Service
                            Providers will give you the best AC repair service. From general inspection, to changing
                            AC
                            parts you can avail every AC related service within a few moments.</p>
                        <h4 class="sub-title">About Sheba.xyz's AC Repairing Service</h4>
                        <p class="safetyp">Sheba.xyz is the largest marketplace in Bangladesh where we serve you
                            with every possible
                            service. AC Repairing service is one of our services to repair all types of AC related
                            problems. We deliver expert and AC repair services with integrity from our professional
                            service providers.</p>
                        <h4 class="sub-title">Available Services</h4>

                    </div>

                    <div class="col-md-6">
                        <div class="typography-box serviceoverview">
                            <div class="typo-content">
                                <ul class="list-unordered">
                                    <li class="liststyle2">AC Checkup
                                    </li>
                                    <li class="liststyle2">AC Basic Servicing
                                    </li>
                                    <li class="liststyle2">AC Gas Charge
                                    </li>
                                    <li class="liststyle2">AC Master Service
                                    </li>
                                    <li class="liststyle2">AC Water Drop Solution
                                    </li>
                                    <li class="liststyle2">AC Installation
                                    </li>
                                    <li class="liststyle2">AC Shifting Service
                                    </li>
                                    <li class="liststyle2">AC Compressor Fitting
                                    </li>
                                    <li class="liststyle2">AC Dismantling
                                    </li>
                                    <li class="liststyle2">AC Jet Wash
                                    </li>
                                    <li class="liststyle2">AC Service Repairing
                                    </li>


                                </ul>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h4 class="sub-title">Available Services</h4>
                        <div class="typography-box serviceoverview">
                            <div class="typo-content">
                                <ul class="list-unordered">
                                    <li class="liststyle2">AC Basic Servicing
                                    </li>
                                    <li class="liststyle2">AC Master Service
                                    </li>
                                    <li class="liststyle2">AC Water Drop Solution
                                    </li>
                                    <li class="liststyle2">AC Jet Wash
                                    </li>


                                </ul>


                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-md-12">
                    <p class="safetyp"><strong>AC Checkup Service:</strong> AC Checkup service offers only the
                        diagnosis of your Air
                        Conditioner by an expert technician who performs initial tests for problem identification.
                    </p>

                    <p class="safetyp"><strong>AC Basic Servicing:</strong> AC Basic service offers primary
                        diagnosis, filter cleaning,
                        test and identify problems by an expert AC technician. </p>

                    <p class="safetyp"><strong>AC Gas Charge:</strong> This service offers a performance checkup and
                        post gas refill. If
                        there is a leakage; most of the time AC can be fixed onsite but sometimes it might take a
                        longer
                        time. For that, you have to wait for 1 or 2 days. </p>

                    <p class="safetyp"><strong>AC Master Service:</strong> AC Master Service offers detail cleaning
                        of the indoor and
                        outdoor units including minor problem-fixing (excluding materials and parts). The service
                        charge
                        varies on your AC amount, height, weight and difficulties. </p>

                    <p class="safetyp"><strong>AC Water Drop Solution: </strong>This service offers identification
                        of the source of
                        dripping water from your AC and fixation water drainage system accordingly. Any additional
                        materials/parts will be charged separately. </p>

                    <p class="safetyp"><strong>AC Shifting Service:</strong> This service is to shift your AC unit
                        from one place or
                        floor to the loading truck. Only the service charge is applicable for this service. The
                        service
                        charge varies on your AC amount, height, weight, and difficulties. </p>

                    <p class="safetyp"><strong>AC Compressor Fitting With Gas Charge: </strong>This service offers
                        old Compressor
                        removal and new Compressor installation. Compressor price and warranty differ as per
                        manufacturer. </p>

                    <p class="safetyp"><strong>AC Jet Wash:</strong> AC Jet Wash offers detailed cleaning of the
                        indoor and outdoor
                        units with Jet Wash Machine including minor problem-fixing (excluding materials and parts).
                        The
                        service charge varies on your AC amount, height, weight, and difficulties.v

                    <p class="safetyp"><strong>AC Dismantling:</strong> This service offers dismantling AC from home
                        or workplace and
                        disconnecting all the electrical wiring from the AC unit. </p>

                    <p class="safetyp"><strong>AC Capacitor Replacement:</strong> This service offers to replace the
                        AC capacitor with a
                        new one. Capacitor price and warranty differ as per manufacturer. </p>

                    <p class="safetyp"><strong>AC Circuit Repairing:</strong> This service offers to repair the
                        circuits of your AC.
                        Circuit box price and warranty differ as per manufacturer. </p>

                    <h4 class="sub-title">Why Us?</h4>

                    <p class="safetyp"><strong>Hassle-Free:</strong> Ordering AC repair service from us is simple
                        and easy. You can hire
                        expert Service Providers from us hassle-free to carry your AC here and there. Our Service
                        Provider will come to your doorstep for you.</p>

                    <p class="safetyp"><strong>Budget-Friendly:</strong> You can hire a professional AC repair
                        service in the same
                        budget or less than any other local services near you. Our Service Providers will provide
                        expert
                        AC technicians to inspect problems and fix them.</p>

                    <p class="safetyp"><strong>Well-trained Professionals:</strong> Our professional Service
                        Providers have discreet and
                        skilled AC repair technicians. Their backgrounds are thoroughly checked in detail. Safety
                        Assurance: Our service providers offer a safe AC repairing service for you. This means they
                        will
                        handle repairs with care.</p>

                    <h4>Pricing</h4>

                    <p class="safetyp"> You only have to pay the service charge including materials/parts cost if
                        taken using cost will
                        have to pay if no service is avail payment:</p>

                    <p class="safetyp"> After service completion you will receive a text message on your mobile from
                        Orientbd then you
                        have to pay through Online or Cash on Delivery.</p>
                    <h4>Liability</h4>
                    <p class="safetyp"> Orientbd will not be liable for any pre-existing issues or potential risks
                        reported by the
                        technician but not handled due to the customer’s refusal to repair.</p>
                </div>


                <div class="row">
                    <div class="col-md-4">
                        <h4 class="sub-title">Available Services</h4>
                        <div class="typography-box serviceoverview">
                            <div class="typo-content">
                                <ul class="list-unordered">
                                    <li class="liststyle2">AC Basic Servicing-1
                                    </li>
                                    <li class="liststyle2">AC Master Service
                                    </li>
                                    <li class="liststyle2">AC Water Drop Solution
                                    </li>
                                    <li class="liststyle2">AC Jet Wash
                                    </li>


                                </ul>


                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="sub-title">Night Service: 10 pm to 8 am</h4>
                        <div class="typography-box serviceoverview">
                            <div class="typo-content">
                                <ul class="list-unordered">
                                    <li class="liststyle2">Night
                                        service starts from 10:00 pm to 8:00 am
                                    </li>
                                    <li class="liststyle2">Minimum 4
                                        Hours Lead time after service booking
                                    </li>
                                    <li class="liststyle2">In excess of
                                        BDT 500 will be charged as Emergency Support Service Charge
                                    </li>
                                    <li class="liststyle2">If for any
                                        reason the customer refuses to take service after order confirmation, only
                                        the Emergency Support Service Charge will be applicable
                                    </li>
                                    <li class="liststyle2">Orientbd
                                        will not liable for any direct or incidental loss/damage of the client’s
                                        property or personal security during availing of the service, caused by
                                        accident, theft, burglary, or any other type of incidental damages.
                                    </li>
                                    <li class="liststyle2">The client
                                        is singularly responsible for monitoring, using, and supervising the
                                        activities provided by Service providers.
                                    </li>
                                    <li class="liststyle2">By availing
                                        the service, clients automatically discharge Sheba.xyz from any claims or
                                        legal/moral liabilities other than stated in the Terms of service specified
                                        by Orientbd
                                    </li>


                                </ul>


                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>

        <!-- theme setting -->
        <div class="setting-box open-setting">
            <div class="setting-body" style="width: 310px;">
                <div class="buy_btn ">
                    @foreach($parentCategories as $category)
                    <h3 class="servicingh3">{{$category->name}}</h3>
                    <a href="#" class="btn  mr-3 mb-4" style="background-color: #007b7a; color: #ffff;"><i
                            class="fa fa-star" aria-hidden="true"></i> 4.65 <span style="font-size:12px">out of 5
                        </span></a>
                    @foreach($category->subcategory as $category_wise_prices)
                    <a href="#" data-toggle="modal" data-target="#exampleModal{{$category_wise_prices->id}}"
                        title="Quick View" tabindex="0"
                        class="btn btn-block purchase_btn serviceflex">{{$category_wise_prices->name}} 
                        <i class="fa fa-angle-right servicefastyle" aria-hidden="true"></i></a>
                    <br>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal{{$category_wise_prices->id}}" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content quick-view-modal">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <section class="container">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p style="text-align: center;">
                                                            <strong>{{$category_wise_prices->name}}<i
                                                                    class="fa fa-map-marker" aria-hidden="true"
                                                                    style="color: #ff4c3b;"></i> Gulshan1</strong>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <div style="overflow: hidden;">
                                                                    <p style="float: left;"><strong>Select
                                                                            {{$category_wise_prices->name}}
                                                                        </strong></p>
                                                                    <p style="float: right;"><strong>3 options
                                                                            available</strong></p>
                                                                </div>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <!-- BEGIN PRODUCTS -->
                                                                    @foreach($category_wise_prices->categoryWisePrice as
                                                                    $key=>$category_wise_prices_list)
                                                                    <div class="col-md-12 col-sm-12 cartmargin">
                                                                        <div class="sc-product-item thumbnail">
                                                                            <div class="caption">
                                                                                <!-- <p data-name="product_desc"> {{$category_wise_prices_list->title}}</p> -->
                                                                                <h4 data-name="product_name">
                                                                                    {{$category_wise_prices_list->title}}
                                                                                </h4>
                                                                                <hr style="border-top: none;"
                                                                                    class="line" />
                                                                                <div>
                                                                                    <strong class="price float-left">৳
                                                                                        {{$category_wise_prices_list->price}}
                                                                                        /
                                                                                        {{$category_wise_prices_list->service_type}}
                                                                                    </strong>
                                                                                    <input
                                                                                        id="category_price_id_{{$key+1}}"
                                                                                        name="category_price_id"
                                                                                        value="{{$category_wise_prices_list->id}}"
                                                                                        type="hidden" />
                                                                                    <input id="title_{{$key+1}}"
                                                                                        name="title"
                                                                                        value="{{$category_wise_prices_list->title}}"
                                                                                        type="hidden" />
                                                                                    <input id="service_price_{{$key+1}}"
                                                                                        name="service_price"
                                                                                        value="{{$category_wise_prices_list->price}}"
                                                                                        type="hidden" />
                                                                                    <input id="service_name_{{$key+1}}"
                                                                                        name="service_name"
                                                                                        value="{{$category_wise_prices->name}}"
                                                                                        type="hidden" />
                                                                                    <input id="unit_{{$key+1}}"
                                                                                        name="unit"
                                                                                        value="{{$category_wise_prices_list->service_type}}"
                                                                                        type="hidden" />
                                                                                    <input id="category_id_{{$key+1}}"
                                                                                        name="category_id"
                                                                                        value="{{$category_wise_prices_list->category_id}}"
                                                                                        type="hidden" />
                                                                                    <a id="{{$key+1}}"
                                                                                        class="sc-add-to-cart btn btn-primary btn-sm btn-sm float-right add_item des_button_{{$key+1}}">Add
                                                                                        <i class="fa fa-plus"
                                                                                            aria-hidden="true"></i></a>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    @endforeach

                                                                    <!-- END PRODUCTS -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <form name="form2" id="form2" method="post"
                                                            action="{{ route('addto_cart') }}">
                                                            @csrf
                                                            <!-- Cart submit form -->
                                                            <div class="panel panel-default sc-cart sc-theme-default">
                                                                <input type="hidden" name="cart_list" />
                                                                <div class="panel-heading sc-cart-heading text-center">
                                                                    Shopping Cart <span
                                                                        class="sc-cart-count badge">3</span></div>
                                                                <div class="list-group sc-cart-item-list" id="new_item">
                                                                    <div id="delete_dev"></div>

                                                                    <!-- <div class="sc-cart-item list-group-item-servce">
                                                                                <button type="button" class="sc-cart-remove"></button>
                                                                                <img class="img-responsive pull-left" src="" />
                                                                                <h4 class="list-group-item-heading">1 - 1.5 Ton</h4>
                                                                                <p class="list-group-item-text">AC Basic Service</p>
                                                                                <div class="sc-cart-item-summary">
                                                                                    <span class="sc-cart-item-price">৳500.00</span> <input type="number" min="1" max="1000" class="sc-cart-item-qty" value="1" /> = <span class="sc-cart-item-amount">$500.00</span>
                                                                                </div>
                                                                            </div> -->


                                                                </div>
                                                                <div class="panel-footer sc-toolbar">
                                                                    <div class="sc-cart-summary">
                                                                        <div class="sc-cart-summary-subtotal mt-4">
                                                                            Subtotal: <span class="sc-cart-subtotal"
                                                                                id="total"></span></div>
                                                                    </div>
                                                                    <div class="sc-cart-toolbar">
                                                                        <button type="submit" class="btn btn-info sc-cart-checkout"
                                                                           >Add to Cart</button>
                                                                        <button
                                                                            class="btn btn-danger sc-cart-clear delete_total clear"
                                                                            id="delete_total"
                                                                            type="button">Clear</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    </form>
                                                </div>
                                            </section>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal end -->

                    @endforeach

                    @endforeach
                </div>


            </div>
        </div>
    </div>
</div>
<!-- theme setting -->
<!-- Service Area end -->





<!-- JS
    ============================================ -->


@endsection
