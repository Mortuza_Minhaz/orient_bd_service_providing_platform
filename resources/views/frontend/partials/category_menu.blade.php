<div class="aiz-category-menu bg-white rounded  shadow-sm">
<ul class="list-unstyled categories no-scrollbar py-2 mb-0 text-left">
    <li class="category-nav-element" data-id="13">
        <a href="https://belaobela.com.bd/category/it-products-szvby" class="text-truncate text-reset  d-block" style="padding:7px 15px  8px 15px ">
            <img class="cat-image mr-2 opacity-60 ls-is-cached lazyloaded" src="https://belaobela.com.bd/public/uploads/all/h1iKuGnSKsgCTeRzPKu99lDibAFm0V1R6DEUvGLV.png" data-src="frontend/assets/images/category/1.png" width="16" alt="IT Products" onerror="this.onerror=null;this.src='https://belaobela.com.bd/frontend/assets/img/placeholder.jpg';">
            <span class="cat-name">IT Products</span>
            <i style="float: right;" class="la la-angle-right"></i>
        </a>
        <div class="sub-cat-menu c-scrollbar-light rounded shadow-lg p-4 loaded">
            <div class="card-columns">
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/laptop-and-desktop-j7b8g">Laptop and Desktop</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/laptop-and-notebooks-idji8">Laptop and Notebooks</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/gaming-laptop-yasmf">Gaming Laptop</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/macbook-y4zui">Macbook</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/All-in-One-PC-RTqvS">All in One PC</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Gaming-PC-SyKXE">Gaming PC</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/monitor-rjw9o">Monitor</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/ViewSonic-gsNgg">ViewSonic</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Samsung-uKylA">Samsung</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/LG-3I0mu">LG</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Innovtech-CJKTJ">Innovtech</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/ups-vrmee">UPS</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/online-ups-eujjm">Online UPS</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/offline-ups-mrjdf">Offline UPS</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/avr-sader">AVR</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Relay-DU5IW">Relay</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Servo-IJFeK">Servo</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Air-Cooling-3EVjn">Air Cooling</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Oil-Cooling-QjuQj">Oil Cooling</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/projector-5r0gn">Projector</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/dlp-projector-bgk0k">DLP Projector</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/led-projector-g9uxe">LED Projector</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/mini-projector-6ansn">Mini Projector</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/interactive-flat-panel-7wabe">Interactive Flat Panel</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/ViewSonic-pcXoo">ViewSonic</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Innovtech-5wBBg">Innovtech</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/digital-signage-eposter-4rhqe">Digital Signage (ePoster)</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/ViewSonic-6E8Mc">ViewSonic</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Innovtech-fZhWy">Innovtech</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/computer-accessories-5hki0">Computer Accessories</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/HDD-7gqNW">HDD</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/SSD-Card-CgOv0">SSD Card</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Graphic-Card-OmN89">Graphic Card</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Processor-XbwnM">Processor</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/RAM-N5rur">RAM</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Mother-Board-H4E1H">Mother Board</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Keyboard-Bk98Y">Keyboard</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Mouse-Nqtfx">Mouse</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/PlayStation-Controller-rUc3G">PlayStation Controller</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Router-vxcqa">Router</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Modem-U84wC">Modem</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/computer-case-lhira">Computer Case</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/power-supply-vlvf3">Power Supply</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/laptop-battery-tbrhd">Laptop Battery</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/laptop-bag-yy7ab">Laptop Bag</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Charger-0QngG">Charger</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Connecting-Cable-ggNUa">Connecting Cable</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/speaker-vzuhx">Speaker for PC</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Foldable-laptop-Desk-Iy8rt">Foldable laptop Desk</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Joysticks-jVFEa">Joysticks</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/PC-Case-Fan-C8tRB">PC Case Fan</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Desktop-PC-trolley-tgsK7">Desktop PC trolley</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Network-switch-VR8Mg">Network switch</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/software-and-antivirus">Software and Antivirus</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/ESET-Antivirus-2zniD">ESET Antivirus</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Norton-OJNcQ">Norton</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Kaspersky-xIwiY">Kaspersky</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Microsoft-Office-2019-boOEB">Microsoft Office 2019</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Adobe-Software-2h8Gm">Adobe Software</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Interactive-White-Board-DQBRD">Interactive White Board</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Innovtech-RSAyw">Innovtech</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Projection-Screen-AtMH6">Projection Screen</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Server-hrVIY">Server</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Work-Station-VzEKn">Work Station</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/networking-adopter-mnxaa">Networking Adopter</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </li>
    <li class="category-nav-element" data-id="14">
        <a href="https://belaobela.com.bd/category/electronic-items-wqvbs" class="text-truncate text-reset  d-block" style="padding:7px 15px  8px 15px ">
            <img class="cat-image mr-2 opacity-60 ls-is-cached lazyloaded" src="https://belaobela.com.bd/public/uploads/all/rh754w5yTewC9wrUMY8VlDRolzbhL4PQ17zM0cXr.png" data-src="https://belaobela.com.bd/public/uploads/all/rh754w5yTewC9wrUMY8VlDRolzbhL4PQ17zM0cXr.png" width="16" alt="Electronic Items" onerror="this.onerror=null;this.src='https://belaobela.com.bd/frontend/assets/img/placeholder.jpg';">
            <span class="cat-name">Electronic Items</span>
            <i style="float: right;" class="la la-angle-right"></i>
        </a>
        <div class="sub-cat-menu c-scrollbar-light rounded shadow-lg p-4 loaded">
            <div class="card-columns">
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/air-cooler-ogmak">Air Cooler</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Televisions-XwVk9">Televisions</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Smart-TV-IVNSP">Smart TV</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/LCD-TV-svA9m">LCD TV</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/OLED-TV-Hxb9X">OLED TV</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/QLED-03uFR">QLED</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Others-TV-DbuVW">Others TV</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/tv-box-ulcue">TV Box</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/tv-remote-nzxsl">TV Remote</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/TV-Accessories-04vE4">TV Accessories</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/wall-mount-2yhpc">Wall Mount</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/4k-uhd-tv-thi9s">4K UHD TV</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/led-tv-uqkma">LED TV</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Refrigerator-DAQWU">Refrigerator</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Direct-Cool-Refrigerator-gkcTF">Direct Cool Refrigerator</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Non-Frost-Refrigerator-aAl5v">Non-Frost Refrigerator</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Freezer-8j0Fs">Freezer</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Beverage-Cooler-KQueD">Beverage Cooler</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/air-conditioner-kk7s3">Air Conditioner</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Split-AC-mv1Gm">Split AC</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Cassette--Ceiling-L8vfh">Cassette / Ceiling</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Industrial-HVAC-IRzdC">Industrial HVAC</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Window-AC-U9qQW">Window AC</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Portable-AC-g2fw9">Portable AC</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Floor-Standing-AC-Y9MTQ">Floor Standing AC</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/FAN-6D0kJ">FAN</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Ceiling-FAN-OixGX">Ceiling FAN</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Table-FAN-Oydjr">Table FAN</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Standing-FAN-cCCrd">Standing FAN</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Light-CrzPa">Light</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/LED-Light-Nox16">LED Light</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Tube-Light-EOTsw">Tube Light</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Energy-Bulb-YYgQu">Energy Bulb</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Washing-Machine-nJ7w4">Washing Machine</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Front-Loading-INKHI">Front Loading</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Top-Loading-c26I9">Top Loading</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Blender-qDUTn">Blender</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Hand-Blender-8GGcJ">Hand Blender</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Normal-Blender-8hgTR">Normal Blender</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Camera-taIEN">Camera</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Digital-Camera-1jZS6">Digital Camera</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/DSLR-Camera-JCx4k">DSLR Camera</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Video-Camera-Hdls4">Video Camera</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Camera-Accessories-mCYZ9">Camera Accessories</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Camera-Lens-0sxLf">Camera Lens</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Camera-Bag-XedyA">Camera Bag</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Tripod-g4Ogz">Tripod</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Ring-Light-en78o">Ring Light</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Camera-Battery-GywHC">Camera Battery</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Camera-Charger-NFEYP">Camera Charger</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Water-Purifier-CK7pM">Water Purifier</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Water-Heater-ufpyq">Water Heater</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/ips-qq2z5">IPS</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Iron-ORyS9">Iron</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Dry-Iron-mdfqJ">Dry Iron</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Steam-Iron-dvfYH">Steam Iron</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Blender-1ZFyg">Blender</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Sound-System-9YNTj">Sound System</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Room-Heater-aqWeE">Room Heater</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Electric-Geyser-EdQxo">Electric Geyser</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Dish-Washer-UoILR">Dish Washer</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Vacuum-Cleaner-for-Home--Office-jS4iv">Vacuum Cleaner for Home / Office</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Grinder-Machine-T4MWc">Grinder Machine</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </li>
    <li class="category-nav-element" data-id="15">
        <a href="https://belaobela.com.bd/category/office-equipment-8yjvv" class="text-truncate text-reset  d-block" style="padding:7px 15px  8px 15px ">
            <img class="cat-image mr-2 opacity-60 ls-is-cached lazyloaded" src="https://belaobela.com.bd/public/uploads/all/Mder1oRnBXkaN078oGaJO415OZV4ONIxO6QF3gFj.png" data-src="https://belaobela.com.bd/public/uploads/all/Mder1oRnBXkaN078oGaJO415OZV4ONIxO6QF3gFj.png" width="16" alt="Office Equipment" onerror="this.onerror=null;this.src='https://belaobela.com.bd/frontend/assets/img/placeholder.jpg';">
            <span class="cat-name">Office Equipment</span>
            <i style="float: right;" class="la la-angle-right"></i>
        </a>
        <div class="sub-cat-menu c-scrollbar-light rounded shadow-lg p-4 loaded">
            <div class="card-columns">
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/cctv-system-waez8">CCTV System</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Dome-Camera-eSiKZ">Dome Camera</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Bullet-Camera-pPf9v">Bullet Camera</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/PTZ-Camera-B95Jl">PTZ Camera</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/WiFi-Camera-LjAFI">WiFi Camera</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Analog-Camera-GmARV">Analog Camera</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/IP-Camera-21xUm">IP Camera</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/DVR-Z5et0">DVR</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/NVR-dWnoF">NVR</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/biometric-device-dz5ys">Biometric Device</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/fingerprint-scanner-5vgkv">Fingerprint Scanner</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/access-control-cabk6">Access Control</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/time-attendance-rlfil">Time Attendance</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Fire-Safety-CjnQg">Fire Safety</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Fire-Extinguisher-zjeMj">Fire Extinguisher</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Fire-Alarm-GFPRD">Fire Alarm</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Smoke-Detector-Cl6OA">Smoke Detector</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Printer-4ARvT">Printer</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Leaser-Printer-8QX95">Leaser Printer</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Label-Printer-ajkGt">Label Printer</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Ink-Printer-VpY9R">Ink Printer</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Printer-Accessories-3D8yD">Printer Accessories</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Ink--Tonner-EJ31y">Ink &amp; Tonner</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Printer-Stands-ktEkf">Printer Stands</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Photocopier-Machine-w9oIx">Photocopier Machine</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Paper-Shedder-VPiSe">Paper Shedder</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Office-Furniture-FGQQg">Office Furniture</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Table-UVnxB">Table</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Chair-6xcQz">Chair</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Meeting-Table-hHSWi">Meeting Table</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Office-Stationery-2UTUg">Office Stationery</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Paper-j0dFa">Paper</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Stapler-uqXJG">Stapler</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Pen-Holder-7VG7N">Pen Holder</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/File-Folder-nzJUs">File Folder</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/PABX-System-zBWj6">PABX System</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/IP-Phone-fLiKt">IP Phone</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Land-Phone-dndoP">Land Phone</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/PABX-ysCEX">PABX</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/scanner-f4yxg">Scanner</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </li>
    <li class="category-nav-element" data-id="54">
        <a href="https://belaobela.com.bd/category/industrial-machineries-upsop" class="text-truncate text-reset  d-block" style="padding:7px 15px  8px 15px ">
            <img class="cat-image mr-2 opacity-60 ls-is-cached lazyloaded" src="https://belaobela.com.bd/public/uploads/all/7E78qbo6bv2GRikc084ThmCUqRixtTlT78RViaRD.png" data-src="https://belaobela.com.bd/public/uploads/all/7E78qbo6bv2GRikc084ThmCUqRixtTlT78RViaRD.png" width="16" alt="Industrial Machineries" onerror="this.onerror=null;this.src='https://belaobela.com.bd/frontend/assets/img/placeholder.jpg';">
            <span class="cat-name">Industrial Machineries</span>
            <i style="float: right;" class="la la-angle-right"></i>
        </a>
        <div class="sub-cat-menu c-scrollbar-light rounded shadow-lg p-4">
            <div class="c-preloader text-center absolute-center">
                <i class="las la-spinner la-spin la-3x opacity-70"></i>
            </div>
        </div>
    </li>
    <li class="category-nav-element" data-id="20">
        <a href="https://belaobela.com.bd/category/home-appliance" class="text-truncate text-reset  d-block" style="padding:7px 15px  8px 15px ">
            <img class="cat-image mr-2 opacity-60 ls-is-cached lazyloaded" src="https://belaobela.com.bd/public/uploads/all/us2bRqrDzjtWoX3f31BuSlviLRWPgKrY9q49aGA7.png" data-src="https://belaobela.com.bd/public/uploads/all/us2bRqrDzjtWoX3f31BuSlviLRWPgKrY9q49aGA7.png" width="16" alt="Home Appliance" onerror="this.onerror=null;this.src='https://belaobela.com.bd/frontend/assets/img/placeholder.jpg';">
            <span class="cat-name">Home Appliance</span>
            <i style="float: right;" class="la la-angle-right"></i>
        </a>
        <div class="sub-cat-menu c-scrollbar-light rounded shadow-lg p-4 loaded">
            <div class="card-columns">
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Furniture-6jYQC">Furniture</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Clocks-3B7ak">Clocks</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Night-Lights-9RmdT">Night Lights</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Home-Dcor-Sticker-777B7">Home Décor Sticker</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Decorative-Painting-8an5k">Decorative Painting</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Decorative-Crafts-MhKhN">Decorative Crafts</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Artificial-Flowers--Plants-VgrJB">Artificial Flowers &amp; Plants</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Candles--Candleholders-7ZZt5">Candles &amp; Candleholders</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Curtains-j8b76">Curtains</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Cushions--Covers-xmMRt">Cushions &amp; Covers</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Picture-Frames-K56EL">Picture Frames</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Rugs--Carpets-hCCJs">Rugs &amp; Carpets</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Vases--Vessels-qHAlM">Vases &amp; Vessels</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Tiles-QmZM5">Tiles</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Home-Theater-du4mE">Home Theater</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Audio-Cable-For-Home-Theater-T9MD7">Audio Cable For Home Theater</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Home-Theater-System-1IaFv">Home Theater System</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Kitchen-Equipments-tPOPZ">Kitchen Equipment's</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/electric-mhrqq">Induction Cooker</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Gas-Stove-T11If">Gas Stove</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/microwave-oven-ug3y4">Microwave Oven</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Electric-Rice-Cooker-sK367">Electric Rice Cooker</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Kitchen-Hood-QJWgw">Kitchen Hood</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Electric-Oven-OWKmx">Electric Oven</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Grill-Oven-sIzSY">Grill Oven</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </li>
    <li class="category-nav-element" data-id="23">
        <a href="https://belaobela.com.bd/category/mobile-phone" class="text-truncate text-reset  d-block" style="padding:7px 15px  8px 15px ">
            <img class="cat-image mr-2 opacity-60 ls-is-cached lazyloaded" src="https://belaobela.com.bd/public/uploads/all/ArbUd6JardTKRXfMjY1mn88pe5Rb3htm8ZzDtsnp.png" data-src="https://belaobela.com.bd/public/uploads/all/ArbUd6JardTKRXfMjY1mn88pe5Rb3htm8ZzDtsnp.png" width="16" alt="Mobile Phone" onerror="this.onerror=null;this.src='https://belaobela.com.bd/frontend/assets/img/placeholder.jpg';">
            <span class="cat-name">Mobile Phone</span>
            <i style="float: right;" class="la la-angle-right"></i>
        </a>
        <div class="sub-cat-menu c-scrollbar-light rounded shadow-lg p-4">
            <div class="c-preloader text-center absolute-center">
                <i class="las la-spinner la-spin la-3x opacity-70"></i>
            </div>
        </div>
    </li>
    <li class="category-nav-element" data-id="19">
        <a href="https://belaobela.com.bd/category/groceries" class="text-truncate text-reset  d-block" style="padding:7px 15px  8px 15px ">
            <img class="cat-image mr-2 opacity-60 ls-is-cached lazyloaded" src="https://belaobela.com.bd/public/uploads/all/YygLlf3leLiPiOYQyety3dgD1V9oAcabbpc8wcbW.png" data-src="https://belaobela.com.bd/public/uploads/all/YygLlf3leLiPiOYQyety3dgD1V9oAcabbpc8wcbW.png" width="16" alt="Groceries" onerror="this.onerror=null;this.src='https://belaobela.com.bd/frontend/assets/img/placeholder.jpg';">
            <span class="cat-name">Groceries</span>
            <i style="float: right;" class="la la-angle-right"></i>
        </a>
        <div class="sub-cat-menu c-scrollbar-light rounded shadow-lg p-4 loaded">
            <div class="card-columns">
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/air-fresheners-axpgu">Air Fresheners</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Fruits--Vegetables-XFahI">Fruits &amp; Vegetables</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Fresh-Fruits-qc1wl">Fresh Fruits</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Fresh-Vegetables-QQ5xU">Fresh Vegetables</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/breakfast-qmxbc">Breakfast</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Local-Breakfast-1fgE0">Local Breakfast</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Energy-Boosters-amdnr">Energy Boosters</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Cereals-1RPZz">Cereals</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Jam--Spreads-staG2">Jam &amp; Spreads</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Cornflakes-XVF10">Cornflakes</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Beverages-4QaaR">Beverages</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Tea-2yObx">Tea</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Coffee-Oveaw">Coffee</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Juice-6Bzuw">Juice</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Soft-Drinks-7wQ3U">Soft Drinks</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Water-Jfc6r">Water</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Syrups--Powder-Drinks-VHLGr">Syrups &amp; Powder Drinks</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Meat--Fish-NdDQR">Meat &amp; Fish</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Meat-q2RRB">Meat</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Fresh-Fish-5G8UG">Fresh Fish</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Dried-Fish-t3nIg">Dried Fish</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Tofu-and-Meat-Alternatives-pUXhh">Tofu and Meat Alternatives</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Snacks-O26qh">Snacks</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Noddles-XUkZg">Noddle's</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Soup-0OPeU">Soup</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Pasta--Macaroni-70134">Pasta &amp; Macaroni</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Candy--Chocolate-C5Q8n">Candy &amp; Chocolate</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Local-Snacks-ucTfh">Local Snacks</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Chips--Pretzels-zEoOz">Chips &amp; Pretzels</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Popcorn--Nuts-MIDRp">Popcorn &amp; Nuts</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Biscuits-k6Cv2">Biscuits</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Salad-Dressing-Jnu7s">Salad Dressing</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Sauces-vLHkA">Sauces</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Dairy-dONpy">Dairy</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Liquid--UHT-Milk-aS8xV">Liquid &amp; UHT Milk</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Butter--Sour-Cream-cNHu4">Butter &amp; Sour Cream</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Cheese-QorQQ">Cheese</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Eggs-LoKWN">Eggs</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Powder-Milk--Cream-YMndQ">Powder Milk &amp; Cream</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Yogurt--Sweet-6l2L7">Yogurt &amp; Sweet</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Frozen--Canned-y0lwE">Frozen &amp; Canned</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Frozen-Snacks-jPlL9">Frozen Snacks</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Canned-Food-5xqMr">Canned Food</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Bread--Bakery-DwG8G">Bread &amp; Bakery</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Cookies-xgaCW">Cookies</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Bakery-Snacks-1eIiS">Bakery Snacks</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Breads-tMpWi">Breads</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Dips--Spreads-s6Ijz">Dips &amp; Spreads</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Honey-dGIki">Honey</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Cakes-vUl9j">Cakes</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Baking-Needs-kLPWa">Baking Needs</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Nuts--Dried-Fruits-sKhGy">Nuts &amp; Dried Fruits</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Baking-Tools-vfIGC">Baking Tools</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Baking--Dessert-Mixes-WQ9CS">Baking &amp; Dessert Mixes</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Baking-Ingredients-4kNrl">Baking Ingredients</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Flour-IH7C5">Flour</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Cooking-SBSf0">Cooking</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Rice-wjyWD">Rice</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Premium-Ingredients-kJ60q">Premium Ingredients</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Spices-RmtLf">Spices</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/oil-xsnmd">Cooking Oil</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Ghee-DOnSz">Ghee</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Ready-Mix-qAAwu">Ready Mix</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Salt--Sugar-IasXc">Salt &amp; Sugar</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Dal-or-Lentil-Ho309">Dal or Lentil</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Special-Ingredients-5qgEz">Special Ingredients</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Shemai--Suji-Kw9aq">Shemai &amp; Suji</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Diabetic-Food-sBJ5r">Diabetic Food</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Sugar-free-biscuits-FNVW9">Sugar free biscuits</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Quaker-Oats-s8ZAh">Quaker Oats</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Zero-Cal-Sugar-aOME9">Zero Cal Sugar</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Edible-Oil-ovpY3">Edible Oil</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Oats-Cereal-dydUU">Oats Cereal</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Noodles--Macaroni-C75Lt">Noodles &amp; Macaroni</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Soap--Wash-I5FdD">Soap &amp; Wash</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Cleaning-Supplies-04PLy">Cleaning Supplies</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Laundry-7OnAU">Laundry</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Pickle-R5cKI">Pickle/আঁচার</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Color--Flavor-9EGYb">Color &amp; Flavor</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Nutella-o72HL">Nutella</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Mayonnaise-DiOxT">Mayonnaise</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </li>
    <li class="category-nav-element" data-id="17">
        <a href="https://belaobela.com.bd/category/womens-fashion" class="text-truncate text-reset  d-block" style="padding:7px 15px  8px 15px ">
            <img class="cat-image mr-2 opacity-60 ls-is-cached lazyloaded" src="https://belaobela.com.bd/public/uploads/all/Wk4IO2hNc91romeYtEoeV2MDkzkcctclnjroRccV.png" data-src="https://belaobela.com.bd/public/uploads/all/Wk4IO2hNc91romeYtEoeV2MDkzkcctclnjroRccV.png" width="16" alt="Women's Fashion" onerror="this.onerror=null;this.src='https://belaobela.com.bd/frontend/assets/img/placeholder.jpg';">
            <span class="cat-name">Women's Fashion</span>
            <i style="float: right;" class="la la-angle-right"></i>
        </a>
        <div class="sub-cat-menu c-scrollbar-light rounded shadow-lg p-4 loaded">
            <div class="card-columns">
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/sharee-umgik">Sharee</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/salwar-kameez-h6gsw">Salwar Kameez</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Kurti-8crWx">Kurti</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Jewelry-GEJpp">Jewelry</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Watch-H0mJj">Watch</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/bags-gg5xd">Carrying Bags for Women</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Shopping-Bag-4r8Wm">Shopping Bag</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Lunch-Bag-KIpvw">Lunch Bag</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Seminar-Bag-BLYRg">Seminar Bag</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Corporate-Bag-LjhR6">Corporate Bag</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Chess-Bag-zYKYB">Chess Bag</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Laundry-Bag-k6ahF">Laundry Bag</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Travel-Bag-c8LvK">Travel Bag</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Hand-Parts-Wiyfl">Hand Parts</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Shoes--Sandals-A4MGY">Shoes &amp; Sandals</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Perfume-body-spray-wtVnM">Perfume-body-spray</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Sunglasses--Frames-Mg6sQ">Sunglasses &amp; Frames</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Palazzo--Leggings-phnD5">Palazzo / Leggings</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Western-Collection-EwZUz">Western Collection</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/t-shirt--polo-shirt-57gdn">T-Shirt &amp; Polo Shirt</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Borka--Hijab--scarf-jzouW">Borka , Hijab &amp; scarf</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Kaftan-7i2hE">Kaftan</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/OrnaDupatta-5rxOb">Orna/Dupatta</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Lehenga-ZfheX">Lehenga</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Fotua-zz4d6">Fotua</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Maxi-jczL0">Maxi</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Gauge-Fabric-wfldT">Gauge Fabric</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Accessories-ASk0x">Accessories</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Cupule-Dress-nzg1E">Cupule Dress</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </li>
    <li class="category-nav-element" data-id="16">
        <a href="https://belaobela.com.bd/category/mens-fashion-eufqn" class="text-truncate text-reset  d-block" style="padding:7px 15px  8px 15px ">
            <img class="cat-image mr-2 opacity-60 ls-is-cached lazyloaded" src="https://belaobela.com.bd/public/uploads/all/cvfj7eQWD0JDHqnh2h3JE9f9yQYHB1VB05ihfs3x.png" data-src="https://belaobela.com.bd/public/uploads/all/cvfj7eQWD0JDHqnh2h3JE9f9yQYHB1VB05ihfs3x.png" width="16" alt="Men's Fashion" onerror="this.onerror=null;this.src='https://belaobela.com.bd/frontend/assets/img/placeholder.jpg';">
            <span class="cat-name">Men's Fashion</span>
            <i style="float: right;" class="la la-angle-right"></i>
        </a>
        <div class="sub-cat-menu c-scrollbar-light rounded shadow-lg p-4 loaded">
            <div class="card-columns">
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Shirt-dHfV8">Shirt</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/t-shirt-kr4ks">T-shirt</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Punjabi-gq2g4">Punjabi</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Polo-shirt-6s5VW">Polo shirt</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Pants-A7Qve">Pants</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Watches-a6f1M">Watches</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Shoes--Sandals-M7Rj0">Shoes &amp; Sandals</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Toiletries-w1rnp">Toiletries</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/wallet-zBhbL">wallet</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/perfume-body-spray-MrmTV">perfume/ body-spray</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Fotua-diFC9">Fotua</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Lungi-pQ5Q6">Lungi</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Sunglasses-AMFYC">Sunglasses</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Blazer-aHyWq">Blazer</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Waistcoat-Prince-Coat-V66nS">Waistcoat/ Prince Coat</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Tie--Cufflink-aiw3N">Tie &amp; Cufflink</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Socks-Ppqaq">Socks</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Briefs--undervest-t8Ci7">Briefs &amp; undervest</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/men%27s%20jewelry">Men's Jewelry</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Pajama-BqzcY">Pajama</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Fabrics-xfomr">Fabrics</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Accessories-UpieS">Accessories</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Carrying-Bags-for-Men-2AGFT">Carrying Bags for Men</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Lunch-Bag-0npiM">Lunch Bag</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Seminar-Bag-hrDXJ">Seminar Bag</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Corporate-Bag-7TysA">Corporate Bag</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Chess-Bag-taJSW">Chess Bag</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Laundry-Bag-74yfr">Laundry Bag</a>
                        </li>
                        <li class="mb-2">
                            <a class="text-reset" href="https://belaobela.com.bd/category/Travel-Bag-VQ2Fz">Travel Bag</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow-none border-0">
                    <ul class="list-unstyled mb-3">
                        <li class="fw-600 border-bottom pb-2 mb-3">
                            <a class="text-reset" href="https://belaobela.com.bd/category/beard-grooming-froj3">Men's Grooming</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </li>
    <li class="category-nav-element" data-id="24">
        <a href="https://belaobela.com.bd/category/babys-gallery" class="text-truncate text-reset  d-block" style="padding:7px 15px  8px 15px ">
            <img class="cat-image mr-2 opacity-60 ls-is-cached lazyloaded" src="https://belaobela.com.bd/public/uploads/all/djGDfm4uvhRDNnxkJNWnjUJaIYCdU0eyunqchx6c.png" data-src="https://belaobela.com.bd/public/uploads/all/djGDfm4uvhRDNnxkJNWnjUJaIYCdU0eyunqchx6c.png" width="16" alt="Baby's Gallery" onerror="this.onerror=null;this.src='https://belaobela.com.bd/frontend/assets/img/placeholder.jpg';">
            <span class="cat-name">Baby's Gallery</span>
            <i style="float: right;" class="la la-angle-right"></i>
        </a>
        <div class="sub-cat-menu c-scrollbar-light rounded shadow-lg p-4">
            <div class="c-preloader text-center absolute-center">
                <i class="las la-spinner la-spin la-3x opacity-70"></i>
            </div>
        </div>
    </li>
    <li class="category-nav-element" data-id="58">
        <a href="https://belaobela.com.bd/category/books-m3gfo" class="text-truncate text-reset  d-block" style="padding:7px 15px  8px 15px ">
            <img class="cat-image mr-2 opacity-60 ls-is-cached lazyloaded" src="https://belaobela.com.bd/public/uploads/all/MjmZEdNVFe6AZU8tMRL45yYU1gLuXUJgBtiV8szs.png" data-src="https://belaobela.com.bd/public/uploads/all/MjmZEdNVFe6AZU8tMRL45yYU1gLuXUJgBtiV8szs.png" width="16" alt="Books" onerror="this.onerror=null;this.src='https://belaobela.com.bd/frontend/assets/img/placeholder.jpg';">
            <span class="cat-name">Books</span>
            <i style="float: right;" class="la la-angle-right"></i>
        </a>
        <div class="sub-cat-menu c-scrollbar-light rounded shadow-lg p-4">
            <div class="c-preloader text-center absolute-center">
                <i class="las la-spinner la-spin la-3x opacity-70"></i>
            </div>
        </div>
    </li>
    <li class="category-nav-element" data-id="52">
        <a href="https://belaobela.com.bd/category/gift-item" class="text-truncate text-reset  d-block" style="padding:7px 15px  8px 15px ">
            <img class="cat-image mr-2 opacity-60 ls-is-cached lazyloaded" src="https://belaobela.com.bd/public/uploads/all/ZDOYw6M6eE7NupRV3u9NfSpnARqqrvcqWmWPcO3f.png" data-src="https://belaobela.com.bd/public/uploads/all/ZDOYw6M6eE7NupRV3u9NfSpnARqqrvcqWmWPcO3f.png" width="16" alt="Gift Items" onerror="this.onerror=null;this.src='https://belaobela.com.bd/frontend/assets/img/placeholder.jpg';">
            <span class="cat-name">Gift Items</span>
            <i style="float: right;" class="la la-angle-right"></i>
        </a>
        <div class="sub-cat-menu c-scrollbar-light rounded shadow-lg p-4">
            <div class="c-preloader text-center absolute-center">
                <i class="las la-spinner la-spin la-3x opacity-70"></i>
            </div>
        </div>
    </li>
    <li class="category-nav-element" data-id="60">
        <a href="https://belaobela.com.bd/category/tools--instruments-v3cqq" class="text-truncate text-reset  d-block" style="padding:7px 15px  8px 15px ">
            <img class="cat-image mr-2 opacity-60 ls-is-cached lazyloaded" src="https://belaobela.com.bd/public/uploads/all/YV1yCv9z9RNXIZO6Jn22ywnjIGHBBgJ5pRWXQ8iD.png" data-src="https://belaobela.com.bd/public/uploads/all/YV1yCv9z9RNXIZO6Jn22ywnjIGHBBgJ5pRWXQ8iD.png" width="16" alt="Tools &amp; Instruments" onerror="this.onerror=null;this.src='https://belaobela.com.bd/frontend/assets/img/placeholder.jpg';">
            <span class="cat-name">Tools &amp; Instruments</span>
            <i style="float: right;" class="la la-angle-right"></i>
        </a>
        <div class="sub-cat-menu c-scrollbar-light rounded shadow-lg p-4">
            <div class="c-preloader text-center absolute-center">
                <i class="las la-spinner la-spin la-3x opacity-70"></i>
            </div>
        </div>
    </li>
</ul>

</div>
