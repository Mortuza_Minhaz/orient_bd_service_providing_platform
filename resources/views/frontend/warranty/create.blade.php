@extends('frontend.layouts.app')

@section('title', 'Create Service Provider Form')


@section('js')
<script>
    $(document).ready(function() {
        $('#company_or_individual').on('change', function() {
            if (this.value == '1') {
                $("#individual").show();
                $("#company").hide();
            } else if (this.value == '2') {
                $("#company").show();
                $("#individual").hide();
            } else {
                $("#company").hide();
                $("#individual").hide();
            }
        });
    });
</script>

@endsection

@section('css')

<style>
    #ssd {
        resize: none;
    }

    hr {
        border-top: 1px solid black;
    }

    .separator {
        display: flex;
        align-items: center;
        text-align: center;
        padding-bottom: 20px;
    }

    .separator::before,
    .separator::after {
        content: '';
        flex: 1;
        border-bottom: 1px solid #000;
    }

    .separator::before {
        margin-right: .25em;
    }

    .separator::after {
        margin-left: .25em;
    }
</style>

@endsection

@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->

    <!-- END PAGE HEADER-->
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1 mt-3 mb-3">
                <h3 class="mb-2 text-center">Warranty Claim Form</h3>
                <form action="{{route('warranty')}}" method="POST" enctype="multipart/form-data" class="border-light p-2">
                    @csrf

                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="Name">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Enter Name" value="{{old('name')}}" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="Mobile Number">Mobile Number</label>
                            <input type="text" class="form-control" name="mobile" placeholder="Enter mobile number" value="{{old('name')}}" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="Email">Email</label>
                            <input type="text" class="form-control" name="email" placeholder="Enter Email" value="{{old('name')}}">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="Address">Address</label>
                            <textarea class="form-control" placeholder="Enter Address" name="address">{{old('address')}} </textarea>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="Name">Invoice Number</label>
                            <input type="text" class="form-control" name="invoice_num" placeholder="Enter Invoice Number" value="{{old('name')}}" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="Mobile Number">Serial Number</label>
                            <input type="text" class="form-control" name="serial_no" placeholder="Enter Serial number" value="{{old('name')}}" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="Email">Date</label>
                            <input type="date" class="form-control" name="purchase_date" value="{{old('name')}}" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="For which product purpose">Warranty For</label>
                            <select class="form-control" name="warranty_for" required>
                                <option>Select a Product</option>
                                <option value="1">Product One</option>
                                <option value="2">Product Two</option>
                            </select>
                        </div>

                    </div>


                    <!-- End Company Section -->

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary mb-3">SUBMIT</button>
                    </div>

                </form>

            </div>
        </div>
    </div>

</div>
</div>

@endsection