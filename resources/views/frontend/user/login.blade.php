@extends('frontend.layouts.app')
@section('title', 'Home')
@section('meta_keywords','home,home page')
@section('meta_description', 'home page of test')

@section('content')
<section class="gry-bg py-5">
    <div class="profile">
        <div class="container">
            <div class="row">
                <div class="col-xxl-4 col-xl-5 col-lg-6 col-md-8 mx-auto">
                    <div class="card">
                        <div class="text-center pt-4">
                            <h1 class="h4 fw-600">
                                Login to your account.
                            </h1>
                        </div>
                        <div class="px-4 py-3 py-lg-4">
                            <div class="">
                                <form class="form-default" action="{{route('login')}}" role="form"  method="POST">
                                    @csrf
                                    <div style="color: red; clear: both;">
                                        @if ($errors->all())
                                            <div class="invalid-feedback" role="alert" style="padding-bottom: 10px;">
                                                <strong>{{ $errors->first() }}</strong></div>
                                        @else
                                            <h5>
                                                <br>
                                            </h5>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <input type="text" class="form-control " value="{{old('email')}}" placeholder="Email or Phone" name="email" id="email">
                                        <!-- <span class="opacity-60">Use country code before number</span> -->
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control "  placeholder="password" name="password" id="password">
                                    </div>
                                    <div class="row mb-2">
                                        {{--<div class="col-6">
                                            <label class="aiz-checkbox">
                                                <input type="checkbox" name="remember">
                                                <span class="opacity-60">Remember Me</span>
                                                <span class="aiz-square-check"></span>
                                            </label>
                                        </div>--}}
                                        <div class="col-12 text-right">
                                            <a href="" class="text-reset opacity-60 fs-14">Forgot Password?</a>
                                        </div>
                                    </div>
                                    <div class="mb-5">
                                        <button type="submit" class="btn btn-primary btn-block fw-600">Login</button>
                                    </div>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
