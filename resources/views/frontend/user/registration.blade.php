@extends('frontend.layouts.app')
@section('title', 'Home')
@section('meta_keywords','home,home page')
@section('meta_description', 'home page of test')
@section('js')
@endsection

@section('content')
<section class="gry-bg py-4">
    <div class="profile">
        <div class="container">
            <div class="row">
                <div class="col-xxl-4 col-xl-5 col-lg-6 col-md-8 mx-auto">
                    <div class="card">
                        <div class="text-center pt-4">
                            <h1 class="h4 fw-600">
                                Create an account.
                            </h1>
                        </div>
                        <div class="px-4 py-3 py-lg-4">
                            <div class="">
                                <form id="reg-form" class="form-default" role="form" action="{{ route('customer_register') }}" method="POST">
                                    @csrf


                                    <div class="form-group">
                                        <input type="text" class="form-control" value="{{ old('name') }}" placeholder="Full Name" name="name" >

                                    </div>
                                    <div class="form-group phone-form-group mb-1">
                                        <div class="iti iti--allow-dropdown iti--separate-dial-code">
                                            <div class="iti__flag-container">
                                                <div class="iti__selected-flag" role="combobox" aria-owns="country-listbox" aria-expanded="false" tabindex="0" title="Bangladesh (বাংলাদেশ): +88" aria-activedescendant="iti-item-bd">
                                                    <div class="iti__flag iti__bd"></div>
                                                    <div class="iti__selected-dial-code">+88</div>
                                                    <div class="iti__arrow"></div>
                                                </div>
                                                <ul class="iti__country-list iti__hide" id="country-listbox" role="listbox">
                                                    <li class="iti__country iti__standard iti__active" tabindex="-1" id="iti-item-bd" role="option" data-dial-code="88" data-country-code="bd" aria-selected="true">
                                                        <div class="iti__flag-box">
                                                            <div class="iti__flag iti__bd"></div>
                                                        </div>
                                                        <span class="iti__country-name">Bangladesh (বাংলাদেশ)</span><span class="iti__dial-code">+88</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <input type="tel" id="phone-code" class="form-control" value="{{ old('phone') }}" placeholder="01xxxxxxxxx" name="phone" autocomplete="off" required="" data-intl-tel-input-id="0" style="padding-left: 81px;">
                                        </div>
                                    </div>

                                    <div class="form-group email-form-group mb-1 d-none">
                                        <input type="email" class="form-control " value="{{ old('email') }}" placeholder="Email" name="email" autocomplete="off">


                                    </div>
                                    <!-- <div class="form-group text-right">
                                       <button class="btn btn-link p-0 opacity-50 text-reset" type="button" onclick="toggleEmailPhone(this)">Use Email Instead</button>
                                       </div> -->
                                    <div class="form-group">
                                        <input type="password" class="form-control" placeholder="password" name="password">

                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation">
                                    </div>
                                    <div class="mb-3">
                                        <label class="aiz-checkbox">
                                            <input type="checkbox" name="checkbox_example_1" required="">
                                            <span class="opacity-60">By signing up you agree to our terms and conditions.</span>
                                            <span class="aiz-square-check"></span>
                                        </label>
                                    </div>
                                    <div class="mb-5">
                                        <button type="submit" class="btn btn-primary btn-block fw-600">Create Account</button>
                                    </div>
                                </form>

                            </div>
                            <div class="text-center">
                                <p class="text-muted mb-0">Already have an account?</p>
                                <a href="#">Log In</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
