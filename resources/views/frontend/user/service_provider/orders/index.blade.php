@extends('frontend.layouts.app')
@section('title', 'Home')
@section('meta_keywords','home,home page')
@section('meta_description', 'home page of test')
@section('content')
    <section class="py-5">
        <div class="container">
            <div class="d-flex align-items-start">
                @include('frontend.include.user_side_bar')
                <div class="aiz-user-panel">
                    <div class="aiz-titlebar mt-2 mb-4">
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <h1 class="h3">Service</h1>
                            </div>
                        </div>
                    </div>
                    
                    <div class="card">
                        <div class="card-header row gutters-5">
                            <div class="col">
                                <h5 class="mb-md-0 h6">All Services</h5>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group input-group-sm">
                                    <form class="" action="" method="GET">
                                        <input type="text" class="form-control" id="search" name="search" placeholder="Search by Order Code">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table aiz-table mb-0 footable footable-1 breakpoint-xl" style="">
                                <thead>
                                <tr class="footable-header">

                                    <th class="footable-first-visible" style="display: table-cell;">#</th>
                                    <th width="30%" style="display: table-cell;">Name</th>
                                    <th data-breakpoints="md" style="display: table-cell;">Category</th>
                                  
                                    <th style="display: table-cell;">Service Receiver</th>
                                    <th style="display: table-cell;">Service Charge</th>
                                   
                                    <th data-breakpoints="md" class="text-right footable-last-visible" style="display: table-cell;">Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="footable-first-visible" style="display: table-cell;">1</td>
                                    <td style="display: table-cell;">
                                        <a href="#" target="_blank" class="text-reset">
                                            Samsung 14V Refrigerator
                                        </a>
                                    </td>
                                    <td style="display: table-cell;">
                                        AC Servicing
                                    </td>
                                 
                                    <td style="display: table-cell;">Jabed Alam</td>
                                    <td style="display: table-cell;">400</td>
                               
                                    <td class="text-right footable-last-visible" style="display: table-cell;">
                                        <a class="btn btn-soft-info btn-icon btn-circle btn-sm" href="#" title="Edit">
                                            <i class="las la-edit"></i>
                                        </a>

                                        <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="#" title="Delete">
                                            <i class="las la-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                </tr>
                                </tbody>
                            </table>
                            <div class="aiz-pagination">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection



