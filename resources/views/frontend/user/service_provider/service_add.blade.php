@extends('frontend.layouts.app')
@section('title', 'Home')
@section('meta_keywords','home,home page')
@section('meta_description', 'home page of test')
@section('js')

    <script>
        CKEDITOR.replace('short_description');

    </script>

@endsection
@section('content')

    <section class="py-5">
        <div class="container">
            <div class="d-flex align-items-start">
                @include('frontend.include.user_side_bar')
                <div class="aiz-user-panel">
                    <div class="aiz-titlebar mb-4">
                        <form class="" action="{{route('service_provider_service_store')}}" method="POST"
                              enctype="multipart/form-data" id="choice_form">
                            @csrf
                            <input type="hidden" name="added_by" value="provider">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="mb-0 h6">Add Service</h5></div>
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-from-label">Name of Product for Service </label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="name"
                                                   placeholder="আপনার পণ্যটি নির্বাচন করুন"></div>
                                    </div>
                                    <div class="form-group row" id="category">
                                        <label class="col-md-3 col-from-label">Category</label>
                                        <div class="col-md-8">
                                            <div class="dropdown bootstrap-select form-control aiz-">
                                                <select class="form-control aiz-selectpicker" name="category_id"
                                                        data-live-search="true" tabindex="-98">
                                                    <option disabled="" selected="">আপনি যে সকল সেবা প্রদান করতে চান তা
                                                        যোগ করুন
                                                    </option>
                                                    <option value="13">Monitor &amp; Display</option>


                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="brand">
                                        <label class="col-md-3 col-from-label">Brand</label>
                                        <div class="col-md-8">
                                            <div class="dropdown bootstrap-select form-control aiz-">
                                                <select class="form-control aiz-selectpicker" name="brand_id"
                                                        id="brand_id" data-live-search="true" tabindex="-98">
                                                    <option value="">আপনার ব্র্যান্ড নির্বাচন করুন</option>
                                                    <option value="3">HP</option>


                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-from-label">Model</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="model"
                                                   placeholder="আপনার মডেল নির্বাচন করুন"></div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-from-label">Capacity</label>
                                        <div class="col-md-8">
                                            <input type="number" class="form-control" name="capacity_id"
                                                   placeholder="পণ্যের আকার , সাইজ বা ধারণ ক্ষমতা লিখুন "></div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-from-label">Tag</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="tag"
                                                   placeholder="আপনার পণ্যটি যেসব নামে লোক জন অনলাইনে খুঁজে পাবে সেসব নাম লিখুন ">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-from-label">Short Description of the Service</label>
                                        <div class="col-md-8">
                                            <textarea id="short_description" name="short_description"
                                                      style="display: none;"></textarea>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="">Service Images</label>
                                        <div class="col-md-8">
                                            <div class="input-group" data-toggle="aizuploader" data-type="image"
                                                 data-multiple="true">


                                                <input type="file" name="photos" class="selected-files"></div>
                                            <div class="file-preview box sm"></div>
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-from-label">Unit</label>
                                        <div class="col-md-6">
                                            <div class="dropdown bootstrap-select form-control aiz-">
                                                <select class="form-control aiz-selectpicker" name="unit"
                                                        id="" data-live-search="true" tabindex="-98">
                                                    <option style="background-color:#c2c5cc;" selected disabled>পণ্য
                                                        সংখ্যা ,দিন , সপ্তাহ , মাস ইত্যাদি লিখুন
                                                    </option>
                                                    <option value="3">পণ্য</option>
                                                    <option value="14">দিন</option>
                                                    <option value="15">সপ্তাহ</option>
                                                    <option value="16">মাস</option>


                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="" placeholder="সংখ্যা">
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-from-label">Service Charge</label>
                                        <div class="col-md-8">
                                            <input type="number" class="form-control" name="service_charge"
                                                   placeholder="সেবার বিনিময় মূল্য লিখুন (কোন প্রকার স্পেয়ার পার্টস ছাড়া )">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-from-label">Investigation Charge</label>
                                        <div class="col-md-8">
                                            <input type="number" class="form-control" name="investigation_charge"
                                                   placeholder="যদি পণ্যটি শুধুমাত্র সমস্যা খুঁজে বের করা হয় কিন্তু সার্ভিসিং করানো না হয় ">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-from-label">Belaobela Commission(%)</label>
                                        <div class="col-md-8">
                                            <input type="number" class="form-control" name="commission"></div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-from-label">Meta Title</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="meta_title"
                                                   placeholder="Meta Title"></div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-from-label">Description</label>
                                        <div class="col-md-8">
                                            <textarea name="meta_description" rows="8" class="form-control"></textarea>
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div class="mar-all text-right">
                                <button type="submit"  class="btn btn-primary">Save Product</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

