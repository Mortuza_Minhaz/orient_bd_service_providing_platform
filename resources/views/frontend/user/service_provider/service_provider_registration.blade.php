@extends('frontend.layouts.app')
@section('title', 'Home')
@section('meta_keywords','home,home page')
@section('meta_description', 'home page of test')

@section('content')
<section class="pt-4 mb-4">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 text-center text-lg-left">
                <h1 class="fw-600 h4">Service Provider Sign Up</h1>
            </div>
            <div class="col-lg-6">
                <ul class="breadcrumb bg-transparent p-0 justify-content-center justify-content-lg-end">
                    <li class="breadcrumb-item opacity-50">
                        <a class="text-reset" href="https://belaobela.com.bd">Home</a>
                    </li>
                    <li class="text-dark fw-600 breadcrumb-item">
                        <a class="text-reset" href="">"Service Provider Registration"</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="pt-4 mb-4">
    <div class="container">
        <div class="row">
            <div class="col-xxl-5 col-xl-6 col-md-8 mx-auto">
                <form id="shop" class="" action="{{route('service_provider_register')}}" method="POST" enctype="multipart/form-data" >
                    @csrf
                    <div class="bg-white rounded shadow-sm mb-3">
                        <div class="fs-15 fw-600 p-3 border-bottom">
                            Personal Info
                        </div>
                        <div class="p-3">
                            <div class="form-group">
                                <label>Choose Type<span class="text-primary">*</span></label>
                                <select class="form-control " name="provider_type">
                                    <option value="inhouse">In House</option>
                                    <option value="outside">Outside</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Your name <span class="text-primary">*</span></label>
                                <input autocomplete="off" type="text" class="form-control" value="{{old('name')}}" placeholder="Name" name="name" >
                            </div>
                            <div class="form-group">
                                <label>Your Email <span class="text-primary">*</span></label>
                                <input autocomplete="off" type="email" class="form-control" value="{{old('email')}}" placeholder="Email" name="email" >
                            </div>
                            <div class="form-group">
                                <label>Phone <span class="text-primary">*</span></label>
                                <input autocomplete="off" type="tel" pattern="\+?(88)?0?1[3456789][0-9]{8}\b" title="Please Input a valid Phone Number" class="form-control" value="{{old('pone')}}" placeholder="Phone" name="phone" required="">
                            </div>

                            <div class="form-group">
                                <label>Your Password <span class="text-primary">*</span></label>
                                <input type="password" class="form-control" placeholder="password" name="password" required="">
                            </div>
                            <div class="form-group">
                                <label>Repeat Password <span class="text-primary">*</span></label>
                                <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" required="">
                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary fw-600">Register Your Shop</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection
