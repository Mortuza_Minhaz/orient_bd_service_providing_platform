@extends('frontend.layouts.app')
@section('title', 'Home')
@section('meta_keywords','home,home page')
@section('meta_description', 'home page of test')

@section('js')

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/frontend/assets/css/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="/frontend/assets/css/select2/js/app.min.js" type="text/javascript"></script>
    <script src="/frontend/assets/css/select2/js/components-select2.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <script>

    $(document).ready(function(){
        company_individual_div();
    });

    $("[name=company_or_individual]").on("change", function (){
        company_individual_div();
    });

    function company_individual_div() {

        var val = $("#company_or_individual").val();
                
                if (val == '1') {
                    $("#individual").show();
                    $("#company").hide();
                } else if (val == '2') {
                    $("#company").show();
                    $("#individual").hide();
                } else {
                    $("#company").hide();
                    $("#individual").hide();
                }

    }

    


    
        
    </script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function() {

            $('#service_district').on('change', function(e) {

                var service_district_id = e.target.value;

                $.ajax({

                    url: "{{ route('upazilas_name') }}",
                    type: "POST",
                    data: {
                        service_district_id: service_district_id
                    },

                    success: function(data) {
                        /*console.log(data);*/

                        $('#related_upazila').empty();

                        $.each(data.upazilas_name, function(index, related_upazila) {

                            $('#related_upazila').append('<option value="' + related_upazila.id + '">' + related_upazila.bn_name + '</option>');
                        })

                    }
                })
            });

        });
    </script>

@endsection

@section('css')

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="/frontend/assets/css/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="/frontend/assets/css/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/frontend/assets/css/select2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->

    <style>
        #ssd {
            resize: none;
        }

        hr.new1 {
            border-top: 1px solid black;
        }


        .btn-success {
            color: #fff !important;
            background-color: #28a745 !important;
            border-color: #28a745 !important;
        }

        .more {
            display: none;
        }
    </style>

@endsection

@section('content')
    <section class="py-5">
        <div class="container">
            <div class="d-flex align-items-start">
                @include('frontend.include.user_side_bar')
                <div class="aiz-user-panel">
                       <div class="row gutters-10">
                           <div class="card">
                               <div class="card-header">
                                   <h5 class="mb-0 h6">Basic Info</h5>
                               </div>
                               <div class="card-body">
                                @if (session('success'))
                                    <div class="alert alert-success">
                                        {{ session('success') }}
                                    </div>
                                @endif
                                <h3 class="mb-3 text-center">Service Provider Profile</h3>

                                <form action="{{route('service_provider_profile_store')}}" method="POST" enctype="multipart/form-data" class="border-light p-2">
                                    @csrf
                <input type="hidden" name="provider_id" value="{{Auth::user()->service_provider->id}}">
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label >Select Applicant Type (আবেদনের টাইপ)</label>
                                                <select class="form-control" id="company_or_individual" name="company_or_individual" required>
                                                    <option value="1" {{ Auth::user()->service_provider->company_or_individual == '1' ? 'selected':'' }}>Individual (ব্যক্তিগতভাবে কাজ করতে চাই)</option>
                                                    <option value="2" {{ Auth::user()->service_provider->company_or_individual == '2' ? 'selected':'' }}>Company (প্রতিষ্ঠান হিসাবে কাজ করতে চাই)</option>
                                                </select>

                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>Your Name</label>

                                                <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}" placeholder="আপনার নাম লিখুন, NID কার্ডের নাম অনুযায়ী" required>

                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>Mobile Number</label>
                                                <input type="text" class="form-control" name="phone" value="{{ Auth::user()->phone }}" placeholder="আপনার নামে রেজিস্ট্রেশন করা মোবাইল নম্বর লিখুন" required>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>Email</label>
                                                <input type="text" class="form-control" name="email" value="{{ Auth::user()->email }}" placeholder="আপনার ইমেইল এড্রেস লিখুন">
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label>Password</label>
                                                <input type="password" class="form-control" name="new_password" placeholder="New Password">
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label>Profile Pic</label>
                                                <input type="file" class="form-control" name="profile_pic">
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label>Address</label>
                                            <textarea class="form-control" placeholder="আপনার বর্তমান ঠিকানা লিখুন" name="service_provider_details_address">{{ Auth::user()->service_provider->service_provider_details_address }}</textarea>

                                        </div>

                                        <div class="form-group col-md-12">
                                            <label for="Address">A Short Biography About Yourself</label>
                                            <textarea class="form-control" placeholder="আপনার সার্ভিস সংক্রান্ত সংক্ষিপ্ত বায়োগ্রাফি লিখুন" name="service_provider_description">{{ Auth::user()->service_provider->service_provider_description }} </textarea>
                                        </div>

                                        <!-- start multi select Section -->
                                        <div class="form-group col-md-6">
                                            <label for="multiple" class="control-label">District (যে সকল জেলায় সার্ভিস দিবেন নির্বাচন করুন)</label>
                                            <select id="service_district" class="form-control select2-multiple select2-hidden-accessible" name="service_district" tabindex="-1" aria-hidden="true" required>
                                                <option value="">--Select District Name --</option>
                                                @foreach ($districts as $district)

                                                    <option value="{{ $district->id }}" {{ $district->id == Auth::user()->service_provider->service_district ? 'selected' : '' }} >{{ $district->bn_name }} - {{ $district->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="multiple" class="control-label">Service Area (যে সকল স্থানে সার্ভিস দিবেন নির্বাচন করুন)</label>
                                            <select autocomplete="false" id="related_upazila" class="form-control select2-multiple select2-hidden-accessible" name="service_location[]" multiple="" tabindex="-1" aria-hidden="true">

                                            </select>

                                        </div>
                                        <!-- end multi select Section -->

                                        <hr class="col-md-12 new1" />


                                    </div>
                                    <br>
                                    <!-- Start Individual Section -->
                                    <div class="form-row" style="display: none;" id="individual">

                                        <div class="form-group col-md-4">
                                            <label for="Upload Updated CV">আপডেটেড সিভি</label>
                                            @if(isset(Auth::user()->service_provider->individual_provider_cv))
                                            <a href="{{ asset(Auth::user()->service_provider->individual_provider_cv) }}" target="_blank" class="btn-info">View</a> 
                                            @endif
                                            <input type="file" class="form-control-file" name="individual_provider_cv">
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="Upload Your NID">আপনার NID</label>
                                            @if(isset(Auth::user()->service_provider->individual_provider_nid))
                                            <a href="{{ asset(Auth::user()->service_provider->individual_provider_nid) }}" target="_blank" class="btn-info">View</a>
                                            @endif
                                            <input type="file" class="form-control-file" name="individual_provider_nid">
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="Upload Your TIN (If Any)">TIN (যদি থাকে) </label>
                                            @if(isset(Auth::user()->service_provider->individual_provider_tin))
                                            <a href="{{ asset(Auth::user()->service_provider->individual_provider_tin) }}" target="_blank" class="btn-info">View</a>
                                            @endif
                                            <input type="file" class="form-control-file" name="individual_provider_tin">
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="Testimonial">একাডেমিক প্রশংসাপত্র </label>
                                            @if(isset(Auth::user()->service_provider->individual_provider_testimonial))
                                            <a href="{{ asset(Auth::user()->service_provider->individual_provider_testimonial) }}" target="_blank" class="btn-info">View</a>
                                            @endif
                                            <input type="file" class="form-control-file" name="individual_provider_testimonial">
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="Experience">কাজের অভিজ্ঞতা পত্র </label>
                                            @if(isset(Auth::user()->service_provider->individual_provider_experience))
                                            <a href="{{ asset(Auth::user()->service_provider->individual_provider_experience) }}" target="_blank" class="btn-info">View</a>
                                            @endif
                                            <input type="file" class="form-control-file" name="individual_provider_experience">
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="NOC">স্থানীয় কর্তৃপক্ষের NOC</label>
                                            @if(isset(Auth::user()->service_provider->individual_provider_noc))
                                            <a href="{{ asset(Auth::user()->service_provider->individual_provider_noc) }}" target="_blank" class="btn-info">View</a>
                                            @endif
                                            <input type="file" class="form-control-file" name="individual_provider_noc">
                                        </div>


                                    </div>
                                    <!-- End Individual Section -->
                                    <br><br>
                                    <!-- Start Company Section -->
                                    <div class="form-row" style="display: none;" id="company">
                                        <div class="form-group col-md-6">
                                            <label for="Company Name">Company Name</label>
                                            <input type="text" class="form-control" name="service_provider_company_name" value="{{ Auth::user()->service_provider->service_provider_company_name }}" placeholder="Company Name">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="man_power">Number of man power</label>
                                            <input type="text" class="form-control" name="service_provider_company_manpower" value="{{ Auth::user()->service_provider->service_provider_company_manpower }}" placeholder="Number of man power">
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label for="Company Profile">প্রতিষ্ঠানের প্রোফাইল ( প্রতিষ্ঠানের নাম, প্রতিষ্ঠানের ঠিকানা, প্রতিষ্ঠানের বয়স, কর্মচারী সংখ্যা, কর্মীদের শিক্ষাগত যোগ্যতা, কিকি পণ্য সার্ভিসিং করার অভিজ্ঞতা আছে তার তালিকা )</label>
                                            @if(isset(Auth::user()->service_provider->company_profile_details))
                                            <a href="{{ asset(Auth::user()->service_provider->company_profile_details) }}" target="_blank" class="btn-info">View</a>
                                            @endif
                                            <input type="file" class="form-control-file" name="company_profile_details">
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="Owner_pic">মালিকের ছবি </label>
                                            @if(isset(Auth::user()->service_provider->company_owner_pic))
                                            <a href="{{ asset(Auth::user()->service_provider->company_owner_pic) }}" target="_blank" class="btn-info">View</a>
                                            @endif
                                            <input type="file" class="form-control-file" name="company_owner_pic">
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="Company NID">মালিকের NID</label>
                                            @if(isset(Auth::user()->service_provider->company_nid))
                                            <a href="{{ asset(Auth::user()->service_provider->company_nid) }}" target="_blank" class="btn-info">View</a>
                                            @endif
                                            <input type="file" class="form-control-file" name="company_nid">
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="Business License"> বাণিজ্যিক লাইসেন্স </label>
                                            @if(isset(Auth::user()->service_provider->company_business_license))
                                            <a href="{{ asset(Auth::user()->service_provider->company_business_license) }}" target="_blank" class="btn-info">View</a>
                                            @endif
                                            <input type="file" class="form-control-file" name="company_business_license">
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="TIN">TIN (যদি থাকে)</label>
                                            @if(isset(Auth::user()->service_provider->company_tin))
                                            <a href="{{ asset(Auth::user()->service_provider->company_tin) }}" target="_blank" class="btn-info">View</a>
                                            @endif
                                            <input type="file" class="form-control-file" name="company_tin">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="Contract Paper">অভিজ্ঞতার পত্র / চুক্তিপত্র / ওয়ার্ক অর্ডার</label>
                                            @if(isset(Auth::user()->service_provider->company_contract_paper))
                                            <a href="{{ asset(Auth::user()->service_provider->company_contract_paper) }}" target="_blank" class="btn-info">View</a>
                                            @endif
                                            <input type="file" class="form-control-file" name="company_contract_paper">
                                        </div>



                                    </div>
                                    <!-- End Company Section -->

                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input" id="defaultUnchecked" style="margin-top: 2px;" required>
                                        <label class="form-check-label" for="defaultUnchecked">I
                                            agree to the <a href="" data-toggle="modal" data-target="#exampleModal" style="color:#f16522">terms and conditions</a></label>
                                    </div>


                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary mb-3">SUBMIT</button>
                                    </div>

                                </form>

                            </div>
                        </div>
                        </div>
                        </div>


                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content quick-view-modal">
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <div class="row">

                                            <div class="col-lg-12">
                                                <div class="terms-and-condition">
                                                    <p class="text-center">
                                                        <strong>সেবা প্রদানকারীর জন্য শর্তাবলী</strong>
                                                    </p>
                                                    <p> আমি অঙ্গিকার করছি যে, আমি বেলাঅবেলা এর একজন সক্রিয় সেবা প্রদানকারী হিসাবে নিচের শর্তগুলো মানিয়া চলিব:
                                                    </p>
                                                    <p> ১। আমার নির্বাচিত পন্যগুলোর ইনস্টলেশন বা সার্ভিসিং বা মেনটেনান্স এরজন্য মোবাইল কল বা মেসেজ রিসিভ করার জন্য আমি যে মোবাইল নম্বর দিয়েছি সেটা সার্বক্ষনিক চালু রাখব।

                                                    </p>
                                                    <p> ২। আর্থিক লেনদেনের জন্য আমি অবশ্যই উক্ত নাম্বারে বিকাশ/নগদ অথবা অন্য কোন পেমেন্ট মেথড ব্যবহার করব। অন্য কোন নম্বর এখানে গ্রহনযোগ্য হবে না বা অন্য কোন নাম্বারে লেন-দেন করার জন্য অনুরোধ করব না।
                                                    </p>
                                                    <p> ৩। মোবাইলে কল বা মেসেজ পাবার সাথে সাথে আমি রিপ্লাই দিয়ে জানাব যে, আমি কাজটি করব কি করব না।
                                                    </p>
                                                    <p> ৪। আমি যদি ১৫ মিনিটের মধ্যে কোন রিপ্লাই না দেই, সেক্ষেত্রে অন্য কাউকে কাজটি করার জন্য ডায়ভার্ট করলে আমার কোন আপত্তি থাকবে না।</span>
                                                    </p>
                                                    <p>
                                                        ৫। কাজ কনফার্ম করার পর দ্রুততম সময়ের মধ্যে সেবা গ্রহনকারীর ঠিকানায় পৌঁছে যাব।
                                                    </p>
                                                    <p>
                                                        ৬। গ্রাহকের সাথে সব সময় সৌহার্দপূর্ন আচরন করব। (গ্রাহকের কাছ থেকে আচরনগত কোন কমপ্লেইন পেলে আপনার মেম্বারশীপ বাতিল করা হবে)।</span>
                                                    </p>
                                                    <p>
                                                        ৭। কাজে যাবার সময় অবশ্যই বেলাঅবেলাথেকে দেয়া আই.ডি. কার্ড বা ড্রেস ব্যবহার করব। আই.ডি. কার্ড বা ড্রেস ব্যবহার না করার কারনে সেবা গ্রহনকারী কাজ করতে অনুমতি না দিলে আমার কোন আপত্তি থাকবে না।

                                                    </p>
                                                    <p>
                                                        ৮। সার্ভিস চার্জ এর জন্য নির্ধারিত ফিস বা চার্জ সেবা প্রদান সফল হবার পর গ্রাহকের কাছ থেকে সংগ্রহ করব
                                                    </p>
                                                    <p> ৯। গ্রাহকের কাছ থেকে ফিস বা চার্জ সংগ্রহ করার সাথে সাথে আমার ব্যবহারিত মোবাইল নাম্বার হতে পেমেন্টে মেথড এর মাধ্যমে বেলাঅবেলা এর জন্য নির্ধারিত কমিশন পে করতে বাধ্য থাকব।

                                                    </p>
                                                    <p>১০। বেলাঅবেলা এর জন্য নির্ধারিত কমিশন পে করতে দেরি করলে বা পেমেন্ট বাকি রাখলে কর্তৃপক্ষ যদি আমাকে পরবর্তীতে আর কোন কাজ না দেয় বা আমার মেশ্বারশীপ বাতিল করে তবে আমার কোন আপত্তি থাকবে না। &nbsp;

                                                    </p>
                                                    <p>
                                                        ১১। বেলাঅবেলা থেকে পূর্ব অনুমতি ছাড়া গ্রাহকের সাথে কোন প্রকার আর্থিক লেনদেন করা যাবে না।
                                                    </p>
                                                    <p> ১২। যে কাজের জন্য পাঠানো হয়েছে সে কাজ ছাড়া অন্য কোন অতিরিক্ত কোন কাজ করব না অথবা ব্যাক্তিগত ভাবে অন্য কোন চুক্তি বা ডিল করা থেকে বিরত থাকব।

                                                    </p>
                                                    <p>১৩। সার্ভিসিং এর জন্য প্রয়োজনীয় সকল প্রকার স্পেয়ার পার্টস বেলাঅবেলা থেকে সংগ্রহ করব। অর্থাৎ বেলাঅবেলার বিনা অনুমতিতে নিজে নিজে কোন কাজ করব না।

                                                    </p>
                                                    <p> ১৪। সর্বপরি বেলাঅবেলা এর কর্ম প্রক্রিয়ার সাথে সার্বিক সহযোগীতা করব। কোন প্রকার অসহযোগীতা, নিয়ম ভঙ্গ, গ্রাহকের সাথে ব্যাক্তিগত যোগাযোগ ইত্যাদি বিষয়ে কোন কমপ্লেইন পেলে কর্তৃপক্ষ আমার মেম্বারশীপ বাতিল করলে আমার কোন আপত্তি থাকবে না।

                                                    </p>

                                                    <p class="text-center">
                                                        <strong>Terms and Conditions for Service Provider</strong>
                                                    </p>
                                                    <p> I promise that I, as an active service provider of BelaObella, will comply with the following conditions:
                                                    </p>
                                                    <ol>
                                                        <li>
                                                            I will always keep the number ON that I have given to reply to mobile calls or messages for installation or servicing or maintenance of my selected products.

                                                        </li>
                                                        <li>
                                                            For financial transactions, I must use bKash / Nagad or any other payment method on that number. No other number will be accepted here. Request to transact in any other number will also be rejected.

                                                        </li>
                                                        <li> As soon as I receive a call or message on my mobile for my selected products, I will instantly reply saying whether I will do the job or not.

                                                        </li>
                                                        <li>
                                                            If I don&apos;t reply within 15 minutes after notifying me, I have no objection in diverting the job to someone else.
                                                        </li>
                                                        <li>
                                                            I will reach the address of the service recipient as soon as possible after confirming the work.
                                                        </li>
                                                        <li>
                                                            I will always be friendly with the customer. (My membership will be canceled if I receive any behavioral complaints from the customer).
                                                        </li>
                                                        <li> The ID or dress given from BelaObela must be used while going to work. I have no objection if the service recipient does not allow me to work because I do not use a card or dress.

                                                        </li>
                                                        <li>
                                                            After the service is successful, I will collect the prescribed fee or charge for the service from the customer.
                                                        </li>
                                                        <li>As soon as I collect the fee or charge from the customer, I will be obliged to pay the commission fixed for BelaObela through the selected payment method from my enlisted mobile number.

                                                        </li>
                                                        <li>I will have no objection if the authorities do not give me any more work or cancel my membership if I delay in paying the commission for BelaObela or if the payment is due.

                                                        </li>
                                                        <li>
                                                            No financial transactions can be made with the customer without prior permission from BelaObela.
                                                        </li>
                                                        <li>
                                                            spaI will not do any work other than the work for which I have been sent or refrain from making any other contract or deal in person with the customer.
                                                        </li>
                                                        <li>

                                                            I will collect all kinds of spare parts required for servicing from BelaObela. That means, I will not do any work on my own without the permission of BelaObela.

                                                        </li>
                                                        <li>
                                                            All in all, I will cooperate fully with every process of BelaObela. I will have no objection if the authority cancels my membership upon receiving any complaint regarding any kind of non-cooperation, violation of
                                                            rules or personal contact with the customer etc.

                                                        </li>
                                                    </ol>


                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end Modal  -->

                </div>

        </div>
    </section>

        <!-- END PAGE HEADER-->


    </div>

@endsection