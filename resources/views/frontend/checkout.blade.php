@extends('frontend.layouts.app')
@section('title', 'Home')
@section('meta_keywords','home,home page')
@section('meta_description', 'home page of test')
@section('js')

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/frontend/assets/css/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="/frontend/assets/css/select2/js/app.min.js" type="text/javascript"></script>
    <script src="/frontend/assets/css/select2/js/components-select2.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function() {

            $('#service_district').on('change', function(e) {

                var service_district_id = e.target.value;

                $.ajax({

                    url: "{{ route('upazilas_name') }}",
                    type: "POST",
                    data: {
                        service_district_id: service_district_id
                    },

                    success: function(data) {
                        /*console.log(data);*/

                        $('#subcategory').empty();

                        $.each(data.upazilas_name, function(index, subcategory) {

                            $('#subcategory').append('<option value="' + subcategory.id + '">' + subcategory.bn_name + '</option>');
                        })



                    }
                })
            });

        });
    </script>

@endsection

@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="/frontend/assets/css/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="/frontend/assets/css/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/frontend/assets/css/select2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->

    <style>
        .files input {
            outline: 2px dashed #92b0b3;
            outline-offset: -10px;
            -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
            transition: outline-offset .15s ease-in-out, background-color .15s linear;
            padding: 93px 0px 85px 38%;
            text-align: center !important;
            margin: 0;
            width: 100% !important;
        }
        .files input:focus{     outline: 2px dashed #92b0b3;  outline-offset: -10px;
            -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
            transition: outline-offset .15s ease-in-out, background-color .15s linear; border:1px solid #92b0b3;
        }
        .files{ position:relative}
        .files:after {  pointer-events: none;
            position: absolute;
            top: 60px;
            left: 0;
            width: 50px;
            right: 0;
            height: 56px;
            content: "";
            background-image: url('frontend/assets/images/upload-image-icon.png');
            display: block;
            margin: 0 auto;
            background-size: 100%;
            background-repeat: no-repeat;
        }
        .color input{ background-color:#f1f1f1;}
        .files:before {
            position: absolute;
            bottom: 10px;
            left: 0;  pointer-events: none;
            width: 100%;
            right: 0;
            height: 46px;
            content: " or drag it here. ";
            display: block;
            margin: 0 auto;
            color: #2ea591;
            font-weight: 600;
            text-transform: capitalize;
            text-align: center;
        }

    </style>
@endsection
@section('content')
<section class="mb-4">
    <div class="container text-left">
        <form class="row mt-4 mb-4" action="#" role="form" method="POST" >

               <div class="col-lg-8">
                   <input type="hidden" name="_token" value="" />
                   <div class="card shadow-sm border-0 rounded">
                       <div class="card-header p-3">
                           <h3 class="fs-16 fw-600 mb-0">
                               Billing details
                           </h3>
                       </div>
                       <div class="card-body text-center">
                           <div class="card">
                               <div class="card-body">
                                       <div class="form-group row">
                                           <label class="col-md-3 col-from-label">Distric জেলা নির্বাচন করুন </label>
                                           <div class="col-md-8">
                                               <select id="service_district" class="form-control select2-multiple select2-hidden-accessible" name="service_district" tabindex="-1" aria-hidden="true" required>
                                                   <option value="">--Select District Name --</option>
                                                   @foreach ($districts as $district)

                                                       <option value="{{ $district->id }}">{{ $district->bn_name }} - {{ $district->name }}</option>
                                                   @endforeach
                                               </select>

                                           </div>
                                       </div>
                                       <div class="form-group row">
                                           <label class="col-md-3 col-from-label">Area স্থান নির্বাচন করুন </label>
                                           <div class="col-md-8">
                                               <select autocomplete="false" id="subcategory" class="form-control select2-multiple select2-hidden-accessible" name="service_location[]" multiple="" tabindex="-1" aria-hidden="true">

                                               </select>

                                           </div>
                                       </div>

                                       <div class="form-group row">
                                           <label class="col-md-3 col-from-label">Address</label>
                                           <div class="col-md-8">
                                               <textarea name="address" rows="4" class="form-control"></textarea>
                                           </div>
                                       </div>
                                   <div class="form-group row">
                                       <label class="col-md-3 col-from-label">Problem পণ্যের বিস্তারিত সমস্যা লিখুন </label>
                                       <div class="col-md-8">
                                           <textarea name="description" rows="4" class="form-control"></textarea>
                                       </div>
                                   </div>
                                       <div class="form-group row">
                                           <label class="col-md-3 col-form-label" for="">Service Images</label>
                                           <div class="col-md-8">
                                               <div class="form-group files">
                                                   <label>Upload Your File </label>
                                                   <input type="file" class="form-control" multiple>
                                               </div>
                                           </div>

                                       </div>


                           </div>
                       </div>
                   </div>

               </div>
               </div>
               <div class="col-lg-4 mt-4 mt-lg-0">
                   <div class="card border-0 shadow-sm rounded">
                       <div class="card-header">
                           <h3 class="fs-16 fw-600 mb-0">Summary</h3>
                           <div class="text-right">
                               <span class="badge badge-inline badge-primary">1 Items</span>
                           </div>
                       </div>

                       <div class="card-body">
                           <table class="table">
                               <thead>
                               <tr>
                                   <th class="product-name">Product</th>
                                   <th class="product-total text-right">TOTAL</th>
                               </tr>
                               </thead>
                               <tbody>
                               <tr class="cart_item">
                                   <td class="product-name">
                                       product 1
                                       <strong class="product-quantity">1</strong>
                                   </td>
                                   <td class="product-total text-right">
                                       <span class="pl-4">20,594৳</span>
                                   </td>
                               </tr>
                               </tbody>
                           </table>

                           <table class="table">
                               <tfoot>
                               <tr class="cart-subtotal">
                                   <th>Subtotal</th>
                                   <td class="text-right">
                                       <span class="fw-600">20,594৳</span>
                                   </td>
                               </tr>

                               <tr class="cart-shipping">
                                   <th>Shipping </th>

                                   <td class="text-right">
                                       <span class="font-italic">Free</span>
                                   </td>
                               </tr>

                               <tr class="cart-total">
                                   <th><span class="strong-600">TOTAL</span></th>
                                   <td class="text-right">
                                       <strong><span>20,702৳</span></strong>
                                   </td>
                               </tr>
                               </tfoot>
                           </table>

                           <div class="mt-3">
                               <input type="hidden" name="_token" value="" />
                               <div class="input-group">
                                   <input type="text" class="form-control" name="code" placeholder="Have coupon code? Enter here" required="" />
                                   <div class="input-group-append">
                                       <button type="submit" class="btn btn-primary">Apply</button>
                                   </div>
                               </div>
                               <div class="pt-3">
                                   <label class="aiz-checkbox">
                                       <input type="checkbox" required="" id="agree_checkbox" />
                                       <span class="aiz-square-check"></span>
                                       <span>I agree to the</span>
                                   </label>
                                   <a href="#">Terms and Conditions</a>, <a href="#">Return Policy</a> &amp;
                                   <a href="#">Privacy Policy</a>
                               </div>
                               <div class="row align-items-center" style="margin-top: 50px;">
                                   <div class="col-5">
                                       <a href="#" class="link link--style-3">
                                           <i class="las la-arrow-left"></i>
                                           Return to shop
                                       </a>
                                   </div>
                                   <div class="col-7">
                                       <button type="button"  class="btn btn-primary fw-500 btn-sm">Complete Order</button>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
        </form>
    </div>
</section>
@endsection

