<ul class="sub-menu flex-wrap">
    <li>
        <a href="#">
            <span> <strong> Accessories &amp; Parts</strong></span>
        </a>
        <ul class="submenu-item">
            <li><a href="#">Cables &amp; Adapters</a></li>
            <li><a href="#">Batteries</a></li>
            <li><a href="#">Chargers</a></li>
            <li><a href="#">Bags &amp; Cases</a></li>
            <li><a href="#">Electronic Cigarettes</a></li>
        </ul>
    </li>
    <li>
        <a href="#">
            <span><strong>Camera &amp; Photo</strong></span>
        </a>
        <ul class="submenu-item">
            <li><a href="#">Digital Cameras</a></li>
            <li><a href="#">Camcorders</a></li>
            <li><a href="#">Camera Drones</a></li>
            <li><a href="#">Action Cameras</a></li>
            <li><a href="#">Photo Studio Supplie</a></li>
        </ul>
    </li>
    <li>
        <a href="#">
            <span><strong>Smart Electronics</strong></span>
        </a>
        <ul class="submenu-item">
            <li><a href="#">Wearable Devices</a></li>
            <li><a href="#">Smart Home Appliances</a></li>
            <li><a href="#">Smart Remote Controls</a></li>
            <li><a href="#">Smart Watches</a></li>
            <li><a href="#">Smart Wristbands</a></li>
        </ul>
    </li>
    <li>
        <a href="#">
            <span><strong>Audio &amp; Video</strong></span>
        </a>
        <ul class="submenu-item">
            <li><a href="#">Televisions</a></li>
            <li><a href="#">TV Receivers</a></li>
            <li><a href="#">Projectors</a></li>
            <li><a href="#">Audio Amplifier Boards</a></li>
            <li><a href="#">TV Sticks</a></li>
        </ul>
    </li>
    <li>
        <a href="#">
            <span><strong>Portable Audio &amp; Video</strong></span>
        </a>
        <ul class="submenu-item">
            <li><a href="#">Headphones</a></li>
            <li><a href="#">Speakers</a></li>
            <li><a href="#">MP3 Players</a></li>
            <li><a href="#">VR/AR Devices</a></li>
            <li><a href="#">Microphones</a></li>
        </ul>
    </li>
    <li>
        <img src="assets/images/menu-image/banner-mega1.jpg" alt="">
    </li>
</ul>
