@extends('backend.layouts.app')

@section('title', 'Users Create')


@section('js')

<script>

</script>


@endsection
@section('css')

@endsection


@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->

    @include('backend/include/beginPageHeader')

    {{-- @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
    @endforeach
    </ul>
</div>
@endif--}}
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="tabbable-line boxless tabbable-reversed">

            <div class="tab-content">
                <div class="tab-pane active" id="tab_0">

                    {{-- <div class="portlet box blue-hoki">--}}
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i>User Edit Form
                            </div>
                            <div class="actions">
                                <a class="btn btn-danger btn-icon-only btn-default" href="{{route('users.index')}}">
                                    <i class="fa fa-list bigger-130" style="color:#fff;"></i>
                                </a>
                                <a class="btn btn-danger btn-icon-only btn-default" href="#">
                                    <i class="fa fa-search-plus bigger-130" style="color:#fff;"></i>
                                </a>
                                <a class="btn btn-danger btn-icon-only btn-default" href="#">
                                    <i class="fa fa-pencil" style="color:#fff;"></i>
                                </a>
                                <a class="btn btn-danger btn-icon-only btn-default" href="#">
                                    <i class="fa fa-trash-o bigger-130" style="color:#fff;"></i>
                                </a>
                            </div>

                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->


                            <form action="{{route('users.update',$user->id)}}" method="post" class="form-horizontal"
                                enctype="multipart/form-data">

                                {{ method_field('PATCH') }}
                                {{ csrf_field() }}

                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">User Name</label>
                                        <div class="col-md-4">
                                            <input name="name" value="{{ $user->name }}" type="text"
                                                class="form-control" placeholder="Enter User Name">


                                            {!! $errors->first('name', '<small class="text-danger">:message</small>')
                                            !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">User Email</label>
                                        <div class="col-md-4">
                                            <input name="email" value="{{ $user->email }}" type="email"
                                                class="form-control" placeholder="Enter User Email">


                                            {!! $errors->first('email', '<small class="text-danger">:message</small>')
                                            !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">User Phone</label>
                                        <div class="col-md-4">
                                            <input name="phone" value="{{ $user->phone }}" type="number"
                                                class="form-control" placeholder="Enter User Phone">


                                            {!! $errors->first('phone', '<small class="text-danger">:message</small>')
                                            !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">User Password</label>
                                        <div class="col-md-4">
                                            <input name="password" value="{{ old('password') }}" type="password"
                                                class="form-control" placeholder="Enter User password">


                                            {!! $errors->first('password', '<small
                                                class="text-danger">:message</small>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">User Confirm Password</label>
                                        <div class="col-md-4">
                                            <input name="confirm-password" value="{{ old('confirm-password') }}"
                                                type="password" class="form-control"
                                                placeholder="Enter Confirm Password">


                                            {!! $errors->first('confirm-password', '<small
                                                class="text-danger">:message</small>') !!}
                                        </div>
                                    </div>


                                    <div class="form-group">

                                        <label class="col-md-3 control-label">Role </label>
                                        <div class="col-md-4">
                                            <select id="single" name="roles" class="form-control select2">
                                                <option value=""></option>

                                                @foreach($roles as $item)

                                                @foreach($userRole as $value)
                                                <option value="{{$item}}" {{ $item == $value ? 'selected' : '' }}>
                                                    {{$item}}
                                                </option>
                                                @endforeach

                                                @endforeach


                                            </select>
                                            {!! $errors->first('roles', '<small class="text-danger">:message</small>')
                                            !!}
                                        </div>
                                    </div>


                                </div>

                                <div class="form-actions top">
                                    <div class="row">
                                        <div class="col-md-offset-4 col-md-8">
                                            <button type="submit" class="btn green">Submit</button>
                                            <button type="button" class="btn default">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                            <div class="portlet-body form">
                                <h3 class="text-center"><strong>Address</strong> </h3>
                                <hr>
                                <div class="m-grid m-grid-demo">
                                    <div class="m-grid-row">
                                        @foreach ($address as $key => $address)
                                        <div class="m-grid-col m-grid-col-middle m-grid-col-center">
                                        <div>
                                                <span class="w-50 fw-600"> <strong>Address:</strong> </span>
                                                <span class="ml-2">{{ $address->address }}</span>
                                            </div>
                                            <div>
                                                <span class="w-50 fw-600"><strong>Postal Code:</strong></span>
                                                <span class="ml-2">{{ $address->postal_code }}</span>
                                            </div>
                                            <div>
                                                <span class="w-50 fw-600"><strong>City:</strong></span>
                                                <span class="ml-2">{{ $address->city }}</span>
                                            </div>
                                            <div>
                                                <span class="w-50 fw-600"><strong>Country:</strong></span>
                                                <span class="ml-2">{{ $address->country }}</span>
                                            </div>
                                            <div>
                                                <span class="w-50 fw-600"><strong>Phone:</strong></span>
                                                <span class="ml-2">{{ $address->phone }}</span>
                                            </div>
                                            @if ($address->set_default)
                                                <div class="position-absolute right-0 bottom-0 pr-2 pb-3">
                                                    <span class="badge badge-inline badge-primary">Default</span>
                                                </div>
                                            @endif
                                            <div class="dropdown position-absolute pull-right">
                                                <button class="btn blue btn-xs btn-success" type="button" data-toggle="dropdown">
                                                <i class="fa fa-info" aria-hidden="true"></i> Action
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton" style="margin: 8px;padding: 7px;">
                                                    @if (!$address->set_default)
                                                        <a class="dropdown-item" href="{{ route('addresses.set_default', $address->id) }}">Make This Default</a><br>
                                                    @endif
                                                    <a class="dropdown-item" href="{{ route('addresses_destroy', $address->id) }}">Delete</a>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        <div class="m-grid-col m-grid-col-middle m-grid-col-center"><span
                                                class="input-group-btn" id="add_new_address">
                                                <a data-toggle="modal" data-target="#myModal"
                                                    class="saleAddPermission btn blue btn-xs btn-success"
                                                    style="height:34px;"><i class="fa fa-plus"
                                                        style="margin-top:8px;"></i>Add New Address</a>
                                            </span>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>

                        <div>

                        </div>
                    </div>


                </div>


            </div>
        </div>
    </div>
</div>
<!-- Address -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add New Address</h4>
            </div>
            <div class="modal-body">


                <div class="row">
                    <div class="col-md-12">
                        <form id="publicForm2" action="{{ route('addresses.store') }}" method="post"
                            class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <label class="col-md-2 col-form-label">Address</label>
                                <div class="col-md-10">
                                    <textarea class="form-control  mb-3" placeholder="Your Address" rows="1"
                                        name="address" required></textarea>
                                </div>
                            </div>
                            <div class="row" style="display:none">
                                <label class="col-md-2 col-form-label">Country</label>
                                <div class="col-md-10">
                                    <div class="mb-3">
                                        <select class="form-control aiz-selectpicker" data-live-search="true"
                                            data-placeholder="Select your country" name="country" required>

                                            <option value="Bangladesh">Bangladesh</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <label>City</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control mb-3" placeholder="Your City" name="city"
                                        value="" required>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-md-2 col-form-label">Postal code</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control mb-3" placeholder="Your Postal Code"
                                        name="postal_code" value="" required>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-md-2 col-form-label">Phone</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control mb-3" placeholder="Your phone number"
                                        name="phone" value="" required>
                                </div>
                            </div>

                            <div class="clearfix form-actions">
                                <div class="col-md-offset-3 col-md-9">

                                    <button type="button" class="btn red btn-outline sbold" data-dismiss="modal">Close
                                    </button>
                                    <button type="submit" id="subBtn2" class="btn btn-info" type="button">
                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                        Save
                                    </button>
                                    &nbsp; &nbsp; &nbsp;
                                    <button class=" btn green btn-outline sbold" type="reset" data-dismiss="modal">
                                        <i class="fa fa-undo bigger-110"></i>
                                        Reset
                                    </button>
                                </div>
                            </div>
                            <div class="modal-dialog">

                                <!-- /.modal-content -->
                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal-footer">
        </div>
    </div>
</div>

</div>
<!-- END CONTENT BODY -->
@endsection
