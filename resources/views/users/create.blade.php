@extends('backend.layouts.app')

@section('title', 'Users Create')


@section('js')

    <script>

    </script>


@endsection
@section('css')

@endsection


@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

    @include('backend/include/beginPageHeader')

    {{--        @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif--}}
    <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable-line boxless tabbable-reversed">

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_0">

                            {{-- <div class="portlet box blue-hoki">--}}
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>User Form
                                    </div>
                                    <div class="actions">
                                        <a class="btn btn-danger btn-icon-only btn-default"
                                           href="{{route('users.index')}}">
                                            <i class="fa fa-list bigger-130" style="color:#fff;"></i>
                                        </a>
                                        <a class="btn btn-danger btn-icon-only btn-default" href="#">
                                            <i class="fa fa-search-plus bigger-130" style="color:#fff;"></i>
                                        </a>
                                        <a class="btn btn-danger btn-icon-only btn-default" href="#">
                                            <i class="fa fa-pencil" style="color:#fff;"></i>
                                        </a>
                                        <a class="btn btn-danger btn-icon-only btn-default" href="#">
                                            <i class="fa fa-trash-o bigger-130" style="color:#fff;"></i>
                                        </a>
                                    </div>

                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->



                                    <form action="{{route('users.store')}}" method="post" class="form-horizontal"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">User Name</label>
                                                <div class="col-md-4">
                                                    <input name="name" value="{{ old('name') }}" type="text"
                                                           class="form-control"
                                                           placeholder="Enter User Name">


                                                    {!! $errors->first('name', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">User Email</label>
                                                <div class="col-md-4">
                                                    <input name="email" value="{{ old('email') }}" type="email"
                                                           class="form-control"
                                                           placeholder="Enter User Email">


                                                    {!! $errors->first('email', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">User Phone</label>
                                                <div class="col-md-4">
                                                    <input name="phone" value="{{ old('phone') }}" type="phone"
                                                           class="form-control"
                                                           placeholder="Enter User Phone">


                                                    {!! $errors->first('phone', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">User Password</label>
                                                <div class="col-md-4">
                                                    <input name="password" value="{{ old('password') }}" type="password"
                                                           class="form-control"
                                                           placeholder="Enter User password">


                                                    {!! $errors->first('password', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">User Confirm Password</label>
                                                <div class="col-md-4">
                                                    <input name="confirm-password" value="{{ old('confirm-password') }}" type="password"
                                                           class="form-control"
                                                           placeholder="Enter Confirm Password">


                                                    {!! $errors->first('confirm-password', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>


                                            <div class="form-group">

                                                <label class="col-md-3 control-label">Role</label>
                                                <div class="col-md-4">
                                                    <select id="single" name="roles"
                                                            class="form-control select2">
                                                        <option value=""></option>
                                                        @foreach($roles as $role)
                                                        <option value="{{$role}}">{{$role}}</option>
                                                            @endforeach


                                                    </select>
                                                    {!! $errors->first('roles', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>



                                        </div>

                                        <div class="form-actions top">
                                            <div class="row">
                                                <div class="col-md-offset-4 col-md-8">
                                                    <button type="submit" class="btn green">Submit</button>
                                                    <button type="button" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>


                        </div>


                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- END CONTENT BODY -->
@endsection
