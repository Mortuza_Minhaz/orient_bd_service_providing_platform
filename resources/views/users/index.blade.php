@extends('backend.layouts.app')

@section('title', 'Users List')


@section('js')
    <script type="text/javascript">
        $(function () {

            var table = $('#example').DataTable({
              /*  dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                processing: true,
                serverSide: true,*/



                dom: 'Blfrtip',


                buttons: [
                    {
                        extend: 'colvis',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    'pdf', 'excel', 'print'

                ],
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],

                "processing": true,
                "serverSide": true,
                "responsive": true,


                ajax: {
                    url: "{{ route('users.index') }}",
                    type: 'GET'
                },

                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},

                    {data: 'role', name: 'role'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'active', name: 'active',orderable: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });

        });
    </script>

@endsection
@section('css')
    <style>
        .portlet.box .dataTables_wrapper .dt-buttons {
            margin-top: 0px;
        }

        .dataTables_wrapper .dt-buttons {
            float: left;
        }

        div.dataTables_wrapper div.dataTables_paginate {
            /* margin: 0; */
            white-space: nowrap;
            /* text-align: right; */
            float: right !important;
        }

        .input-group-sm>.input-group-btn>select.btn, .input-group-sm>select.form-control, .input-group-sm>select.input-group-addon, select.input-sm {
            height: 31px;
            line-height: 30px;
        }
    </style>
@endsection


@section('content')

    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->


    @include('backend.include.beginPageHeader')


    <!-- END PAGE HEADER-->

        <div class="row">
            <div class="col-md-12">


                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">

                        <div class="dt-buttons" style="margin-top: 5px;">
                            <a class="dt-button buttons-print btn default" tabindex="0" aria-controls="example"
                               href="{{ route('users.create') }}"><span>  <i
                                            class="fa fa-plus"></i> Create New User</span>

                            </a>
                        </div>

                    </div>


                    <div class="portlet-body">

                        <table class="table table-striped table-bordered table-hover" id="example">

                            <thead>

                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>

                                <th>User Type</th>
                                <th>Created At</th>
                                <th>Active</th>
                                <th width="280px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        {{--<table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Roles</th>
                                <th width="280px">Action</th>
                            </tr>
                            </thead>
                            <tbody>


                            @foreach ($data as $key => $user)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>
                                        @if(!empty($user->getRoleNames()))
                                            @foreach($user->getRoleNames() as $v)
                                                <label class="badge badge-success">{{ $v }}</label>
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a>
                                        <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>
                                        {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>--}}
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>

    </div>
    {{--{!! $data->render() !!}--}}
    <!-- END CONTENT BODY -->
@endsection
