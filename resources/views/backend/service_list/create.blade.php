@extends('backend.layouts.app')

@section('title', ' ')


@section('js')


@endsection
@section('css')
<style>

</style>
@endsection


@section('content')

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('backend.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12" style="margin-top:20px">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>Service create Form
                    </div>
                    <div class="actions">
                        <!-- <div class="btn-set pull-right">
                            <a href="{{route('photos.index')}}"><button type="button" class="btn red">List</button></a>
                        </div> -->
                    </div>

                </div>
                <div class="portlet-body form">

                    <!-- BEGIN FORM-->
                    <form action="{{route('services.store')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <div class="form-body">

                            <div class="form-group">
                                <label class="col-md-3 control-label">Service Title</label>
                                <div class="col-md-4">
                                    <input name="service_title" type="text" class="form-control" placeholder="write service title">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Service Status </label>
                                <div class="col-md-4">
                                    <select id="single" name="service_status" class="form-control">
                                        <option value="">Select Status</option>
                                        <option value="1">Active</option>
                                        <option value="2">In Active</option>

                                    </select>
                                </div>
                            </div>


                        </div>

                        <div class="form-actions top">
                            <div class="row">
                                <div class="col-md-offset-4 col-md-8">
                                    <button type="submit" class="btn green">Submit</button>
                                    <button type="button" class="btn default">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>


</div>
<!-- END CONTENT BODY -->
@endsection