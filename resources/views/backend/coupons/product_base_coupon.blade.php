<div class="card-header mb-2">
    <h3 class="h6">Add Your Product Base Coupon</h3>
</div>
<div class="form-group row">
    <label class="col-lg-3 col-from-label" for="coupon_code"> Coupon code </label>
    <div class="col-lg-5">
        <input type="text" placeholder=" Coupon code " id="coupon_code" name="coupon_code" class="form-control" required>
    </div>
</div>
<div class="product-choose-list">
    <div class="product-choose">
        <div class="form-group row">
            <label class="col-lg-3 col-from-label" for="name"> Category </label>
            <div class="col-lg-5">
                <select name="product_ids[]"  multiple="multiple" class="form-control product_id" data-live-search="true" data-selected-text-format="count" required>
                    @foreach($all_product as $product)
                        <option value="{{$product->id}}">{{ $product->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>
<br>
<div class="form-group row">
    <label class="col-sm-3 control-label" for="start_date" style="text-align: left;"> Date </label>
    <div class="col-sm-5">
      <input type="text" class="form-control aiz-date-range" name="date_range" placeholder="Select Date">
    </div>
</div>
<div class="form-group row">
   <label class="col-lg-3 col-from-label"> Discount </label>
   <div class="col-lg-3">
      <input type="number" lang="en" min="0" step="0.01" placeholder=" Discount " name="discount" class="form-control" required>
   </div>
   <div class="col-lg-2">
       <select class="form-control aiz-selectpicker" name="discount_type">
           <option value="amount"> Amount </option>
           <option value="percent"> Percent </option>
       </select>
   </div>
</div>


<script type="text/javascript">

    $(document).ready(function(){
        $('.aiz-date-range').daterangepicker();
        AIZ.plugins.bootstrapSelect('refresh');
    });

</script>

