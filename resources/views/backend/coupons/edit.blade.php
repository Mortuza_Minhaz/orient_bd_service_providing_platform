@extends('backend.layouts.app')

@section('title', 'Coupon Edit')


@section('js')
<script type="text/javascript">

    function coupon_form(){
        var coupon_type = $('#coupon_type').val();
        
		$.post('{{ route('coupon.get_coupon_form') }}',{_token:'{{ csrf_token() }}', coupon_type:coupon_type}, function(data){
            $('#coupon_form').html(data);

		});
    }

</script>

@endsection
@section('css')
<style>

</style>
@endsection

@section('content')

<div class="page-content">
@include('backend.include.beginPageHeader')
<div class="row">
        <div class="col-md-10">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">

                <div class="portlet-title">

                <div class="caption">
                            <i class="fa fa-globe"></i> Coupon Details</div>
                        <div class="tools"> </div>

                </div>
                <div class="portlet-body">
            <form action="{{ route('coupon.update', $coupon->id) }}" method="POST">
                <input name="_method" type="hidden" value="PATCH">
            	@csrf
                <div class="card-body">
                    <input type="hidden" name="id" value="{{ $coupon->id }}" >
                    <div class="form-group row">
                        <label class="col-lg-3 col-from-label" for="name"> Coupon Type </label>
                        <div class="col-lg-9">
                            <select name="coupon_type" id="coupon_type" class="form-control aiz-selectpicker" onchange="coupon_form()" required>
                                @if ($coupon->type == "product_base"))
                                    <option value="product_base" selected> For Products </option>
                                @elseif ($coupon->type == "cart_base")
                                    <option value="cart_base"> For Total Orders </option>
                                @endif
                            </select>
                        </div>
                    </div>

                    <div id="coupon_form">

                    </div>
                    <div class="form-group mb-0 text-right">
                        <button type="submit" class="btn btn-primary"> Save </button>
                    </div>
            </form>

            </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
</div>

@endsection

