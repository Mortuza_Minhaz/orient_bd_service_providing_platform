@extends('backend.layouts.app')

@section('title', 'Coupon List')


@section('js')
<script type="text/javascript">

    function coupon_form(){
        var coupon_type = $('#coupon_type').val();
        
		$.post('{{ route('coupon.get_coupon_form') }}',{_token:'{{ csrf_token() }}', coupon_type:coupon_type}, function(data){
            $('#coupon_form').html(data);

         //    $('#demo-dp-range .input-daterange').datepicker({
         //        startDate: '-0d',
         //        todayBtn: "linked",
         //        autoclose: true,
         //        todayHighlight: true
        	// });
		});
    }

</script>

@endsection
@section('css')
<style>

</style>
@endsection

@section('content')
<div class="page-content">
@include('backend.include.beginPageHeader')
<div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">

                <div class="portlet-title">

                <div class="caption">
                            <i class="fa fa-globe"></i> Coupon Details</div>
                        <div class="tools"> </div>

                </div>
                <div class="portlet-body">
              <form class="form-horizontal" action="{{ route('coupon.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <label class="col-lg-3 col-from-label" for="name">Coupon Type</label>
                    <div class="col-lg-5">
                        <select name="coupon_type" id="coupon_type" class="form-control aiz-selectpicker" onchange="coupon_form()" required>
                            <option value="">Select One</option>
                            <option value="category_base">For Category</option>
                            <option value="cart_base">For Total Orders</option>
                        </select>
                    </div>
                </div>

                <div id="coupon_form">

                </div>
                <div class="form-group mb-0 text-right">
                    <button type="submit" class="btn btn-primary" style="margin-right: 15px;">Save</button>
                </div>
              </from>
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
</div>
@endsection

