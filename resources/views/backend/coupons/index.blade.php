@extends('backend.layouts.app')

@section('title', 'Coupon List')


@section('js')


@endsection
@section('css')
<style>

</style>
@endsection

@section('content')
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('backend.include.beginPageHeader')




    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">

                <div class="portlet-title">

                    <div class="dt-buttons" style="margin-top: 5px;">
                        <a class="dt-button buttons-print btn default" tabindex="0" aria-controls="example"
                            href="{{ route('coupon.create') }}"><span> <i class="fa fa-plus"></i> Create New
                                Coupon</span>

                        </a>
                    </div>

                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%"
                        id="sample_3" cellspacing="0" width="100%"">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Code</th>
                                <th>Type</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th width=" 10%">Options</th>
                            </tr>
                         </thead>
                        <tbody>
                            @foreach($coupons as $key => $coupon)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$coupon->code}}</td>
                                <td>@if ($coupon->type == 'cart_base')
                                    Cart Base
                                    @elseif ($coupon->type == 'category_base')
                                    Category Base
                                    @endif</td>
                                <td>{{ date('d-m-Y', $coupon->start_date) }}</td>
                                <td>{{ date('d-m-Y', $coupon->end_date) }}</td>
                                <td class="text-right">
                                
                                    <!-- <a href="{{route('coupon.edit', $coupon->id )}}" class="btn btn-icon-pencil blue btn-icon btn-circle btn-sm"
                                         title="Edit">
                                        <i class="ace-icon fa fa-eye bigger-130"></i>
                                    </a> -->
                                    <a href="{{route('coupon_destroy', $coupon->id)}}" class="btn btn-danger btn-icon btn-circle btn-sm confirm-delete" style="border: none;"
                                        data-href="{{route('coupon.destroy', $coupon->id)}}" title="Delete">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
</div>
<!-- END CONTENT BODY -->
@endsection
