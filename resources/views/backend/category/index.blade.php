@extends('backend.layouts.app')

@section('title', 'Category List')


@section('js')
<script>
    function status_change(id) {

        $.ajax({
            type: "GET",
            url: "{{url('/popular-category-status')}}/" + id,
            success: function(data) {
                swal("Category status changed successfully !!!", {
                    icon: "success",
                }).then(function(){
            location.reload();
            });
            }
        });
    }
</script>

@endsection
@section('css')
<style>

</style>
@endsection


@section('content')

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('backend.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-globe"></i> Category List</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_3" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="all">#</th>

                                <th class="min-phone-l">Name</th>
                                <th class="min-tablet">Parent Category</th>
                                <th class="min-tablet">Level</th>
                                <th class="min-tablet">Status</th>
                                <th class="min-tablet">Popular</th>

                                <th class="min-phone-l">Action</th>
                            </tr>
                        </thead>
                        <tbody>


                            @foreach($active_category_list as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{$item->full_name}}</td>
                                <td>@php
                                $parent = \App\Models\Category::where('id', $item->parent_id)->first();
                                @endphp
                                @if ($parent != null)
                                {{ $parent->name }}
                                @else
                                —
                                @endif</td>
                                <td>{{ $item->level }}</td>
                                <td>{{$item->status_string}}</td>
                                <td>
                                    @if($item->is_popular==1)
                                    <label><input onchange="status_change({{$item->id}})" type="checkbox" data-size="mini" class="make-switch" checked data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>"></label>
                                    @else
                                    <label><input onchange="status_change({{$item->id}})" type="checkbox" data-size="mini" class="make-switch" data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>"></label>
                                    @endif
                                </td>

                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Action
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-left" role="menu">
                                            <li>
                                                <a style="border: none;" href="{{ route('categories.edit', $item->id) }}">
                                                    <span class="item">
                                                        <span aria-hidden="true" class="icon-note icons"></span> Edit</a>
                                            </li>

                                            <li>
                                                <form action="{{ route('categories.destroy', $item->id) }}" method="POST">
                                                    @csrf

                                                    @method('DELETE')

                                                    <button type="submit"> <i class="fa fa-trash"></i> Delete</button>
                                                </form>
                                            </li>

                                        </ul>
                                    </div>
                                </td>

                            </tr>
                            @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>

</div>

<!-- END CONTENT BODY -->
@endsection