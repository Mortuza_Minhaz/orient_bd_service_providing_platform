@extends('backend.layouts.app')

@section('title', 'Category Create')


@section('js')

@endsection

@section('css')

@endsection


@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->

    @include('backend/include/beginPageHeader')

<!-- END PAGE HEADER-->

<div class="row">
    <div class="col-md-12" style="margin-top:20px">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Category Form
                </div>

            </div>
            <div class="portlet-body form">


                <!-- BEGIN FORM-->
                <form action="{{route('categories.store')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">


                        <div class="form-group">
                            <label class="col-md-3 control-label">Category Name :</label>
                            <div class="col-md-4">
                                <input name="name" type="text" class="form-control" placeholder="Category name">
                                {!! $errors->first('name', '<small class="text-danger">:message</small>') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Category Parent :</label>
                            <div class="col-md-4">
                                <select id="single" name="parent_id" class="form-control select2">
                                    <option value="0">No Parent</option>

                            @foreach ($active_category_list as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @foreach ($category->childrenCategories as $childCategory)
                                    @include('categories.child_category', ['child_category' => $childCategory])
                                @endforeach
                            @endforeach
                                </select>
                                {!! $errors->first('parent_id', '<small class="text-danger">:message</small>') !!}

                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 control-label">Category Logo :</label>
                            <div class="col-md-4">
                                <input name="category_logo" type="file" class="form-control" placeholder="Category Logo">
                                {!! $errors->first('category_logo', '<small class="text-danger">:message</small>') !!}

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Category Banner Image :</label>
                            <div class="col-md-4">
                                <input name="category_banner_image" type="file" class="form-control" placeholder="Category banner image">
                                {!! $errors->first('category_banner_image', '<small class="text-danger">:message</small>') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Category Commission Rate</label>
                            <div class="col-md-4">
                                <input name="commision_rate" min="0" step="0.01" value="0.00" type="number" class="form-control" placeholder="Commission Rate">
                                {!! $errors->first('commision_rate', '<small class="text-danger">:message</small>') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Meta Title</label>
                            <div class="col-md-4">
                                <input name="meta_title" value="" type="text" class="form-control" placeholder="Input Meta Title">
                                {!! $errors->first('student_residence', '<small class="text-danger">:message</small>') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Meta description</label>
                            <div class="col-md-4">
                                <textarea name="meta_description" value="" rows="4" cols="50"  type="text" class="form-control" placeholder=""></textarea>
                                {!! $errors->first('student_name', '<small class="text-danger">:message</small>') !!}
                            </div>
                        </div>

                        <input type="hidden" name="category_status" value="1">

                    </div>

                    <div class="form-actions top">
                        <div class="row">
                            <div class="col-md-offset-4 col-md-8">
                                <button type="submit" class="btn green">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>

</div>
<!-- END CONTENT BODY -->
@endsection