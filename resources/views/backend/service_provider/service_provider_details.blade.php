<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>

<head>
    <title>Partner Details</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .center {
            margin-left: auto;
            margin-right: auto;
        }

        tr.border_bottom td {
            border-bottom: 1px solid black !important;
        }

        table,
        tr.margin_tb p {
            margin-top: 10px !important;
            margin-bottom: -2px !important;
        }

        table,
        tr,
        td {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 2px
        }

        p {
            margin: 5px 7px 7px 7px;
        }

        @media print {
            .noprint {
                visibility: hidden;
            }
        }

        .GFG {
            text-decoration: none;
            color: blue;
            font-size: 17px;
        }
    </style>

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <table class="center" style="width: 1050px; border:0">
                    <tbody>
                        <tr class="border_bottom noprint" style="border:0; ">
                            <td style="text-align:left; border:0">
                                <p style="left:0px">

                                    <div>

                                        <form action="{{route('service_provider_list_admin')}}">
                                            <i style="color: #00aaaa;" class="fa fa-arrow-circle-left noprint"></i> <input style="color: #00aaaa;" class="noprint" type="submit" value="Service Provider List">


                                        </form>



                                    </div>

                                </p>
                                <p style="left:0px"></p>
                                <p style="left:0px"></p>
                                <p style="left:0px"></p>

                            </td>

                            <td style="text-align:right; border:0;" class="border_bottom">
                                </p>
                                <p style="left:0px">
                                    <div>
                                        @can('edit-partner')
                                        <form action="{{route('editPartner',$partnertData->id)}}">
                                            <input style="color: #00aaaa;" class="noprint" type="submit" value="Edit">@endcan
                                            <span>
                                                <form>
                                                    <input style="color: #00aaaa;" class="noprint" type="button" value="Print" href="" onClick="window.print()">
                                                </form>

                                            </span>
                                        </form>
                                    </div>
                                </p>
                                <p style="left:0px"> </p>
                                <p style="left:0px"></p>

                            </td>
                        </tr>

                    </tbody>
                </table>

                <p style="text-align: center; margin: 15px 15px;"><strong>Service Provider Application @if($service_providers->company_or_individual == 1) (Individual) @elseif($service_providers->company_or_individual == 2)(Company)@endif</strong></p>

                <table class="center table" cellspacing="0" cellpadding="0" border="1" style="width: 1050px;">
                    <tbody>

                        <tr>
                            <td width="319">
                                <center>
                                    @if($service_providers->company_or_individual == 1)
                                    <p style="color: #00aaaa">Service Provider Name</p>
                                    @elseif($service_providers->company_or_individual == 2)
                                    <p style="color: #00aaaa">Name of Contact Person</p>
                                    @endif
                                </center>
                            </td>

                            <td width="319">


                                <p>{{$service_providers->service_provider_name}}</p>
                                <br>

                            </td>

                        </tr>

                        <tr>

                            <td width="319">
                                <center>
                                    <p style="color: #00aaaa">Contact Number </p>
                                </center>

                            </td>


                            <td width="319">

                                <p>{{$service_providers->service_provider_contact}} </p>

                            </td>
                        </tr>




                        <tr>

                            <td width="319">
                                <center>
                                    <p style="color: #00aaaa"><a>Email</a></p>
                                </center>

                            </td>



                            <td width="319">

                                <p>{{$service_providers->service_provider_email}} </p>

                            </td>
                        </tr>




                        <tr>

                            <td width="319">
                                <center>
                                    <p style="color: #00aaaa">Address</p>
                                </center>

                            </td>



                            <td width="319">

                                <p>{{$service_providers->service_provider_details_address}}</p>

                            </td>
                        </tr>

                        <tr>

                            <td width="319">
                                <center>
                                    <p style="color: #00aaaa">Service District</p>
                                </center>

                            </td>



                            <td width="319">

                                @foreach($provider_service_district as $info)
                                <p>{{$info->bn_name}}</p>
                                @endforeach

                            </td>
                        </tr>

                        <tr>

                            <td width="319">
                                <center>
                                    <p style="color: #00aaaa">Service Area</p>
                                </center>

                            </td>



                            <td width="319">

                                @foreach($provider_service_upazila as $info)
                                <p>{{$info->bn_name}}</p>
                                @endforeach

                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>


        </div>
    </div>
    </div>
    </div>


    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>

</html>