@extends('backend.layouts.app')

@section('title', 'Create Service Provider Form')


@section('js')
<script>
    $(document).ready(function() {
        $('#company_or_individual').on('change', function() {
            if (this.value == '1') {
                $("#company").show();
                $("#individual").hide();
            } else if (this.value == '2') {
                $("#individual").show();
                $("#company").hide();
            } else {
                $("#company").hide();
                $("#individual").hide();
            }
        });
    });
</script>

@endsection

@section('css')

<style>
    #ssd {
        resize: none;
    }

    hr {
        border-top: 1px solid black;
    }

    .separator {
        display: flex;
        align-items: center;
        text-align: center;
        padding-bottom: 20px;
    }

    .separator::before,
    .separator::after {
        content: '';
        flex: 1;
        border-bottom: 1px solid #000;
    }

    .separator::before {
        margin-right: .25em;
    }

    .separator::after {
        margin-left: .25em;
    }
</style>

@endsection

@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    @include('backend.include.beginPageHeader')
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="tab-pane" id="tab_1">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"> <i class="fa fa-form"></i>Service Provider Form</div>
                        <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="{{route('service_provider_store_admin')}}" method="POST" class="horizontal-form" enctype="multipart/form-data">

                            @csrf
                            <div class="form-body">
                                <h3 class="form-section">Person Info</h3>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Location Covered</label>
                                            <input name="service_location" value="" type="text" class="form-control" placeholder="Input Sevice Area"> {!! $errors->first('student_name', '<small class="text-danger">:message</small>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Name</label>
                                            <input name="service_provider_name" value="" type="text" class="form-control" placeholder="Input Name"> {!! $errors->first('student_name', '<small class="text-danger">:message</small>') !!}
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Mobile Number</label>
                                            <input name="service_provider_contact" value="" type="text" class="form-control" placeholder="Input Mobile Number"> {!! $errors->first('student_mobile_number', '<small class="text-danger">:message</small>') !!}
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Email</label>
                                            <input name="service_provider_email" value="" type="text" class="form-control" placeholder="Input Email"> {!! $errors->first('student_residence', '<small class="text-danger">:message</small>') !!}
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Select Expertise on Services</label>
                                            <div class="input-group select2-bootstrap-append">
                                                <select name="expertise_on_service[]" id="multi-append" class="form-control select2" multiple>
                                                    <option></option>
                                                    @foreach ($categories as $category)
                                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button" data-select2-open="multi-append"> <i class="fa fa-arrow-down"></i> </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <!--/span-->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Details Address</label>
                                        <div class="col-md-12">
                                            <textarea class="form-control" value="" name="service_provider_details_address" id="ssd" rows="3" placeholder=""></textarea> {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div> <br>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Select</label>
                                            <select name='company_or_individual' class="form-control">
                                                <option value=" ">Select Option</option>
                                                <option value="1">Company</option>
                                                <option value="2">Individual</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style='display:none;' id='company'>
                                    <p style="text-align: center; font-size: 20px;"></p>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Company Name</label>
                                            <input name="service_provider_company_name" value="" type="text" class="form-control" placeholder="Input English Test Name"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Number of manpower to provide service</label>
                                            <input name="service_provider_company_manpower" value="" type="text" class="form-control" placeholder="Input Overall Score"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row" style='display:none;' id='individual'>
                                    <p style="text-align: center; font-size: 20px;"></p>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Academic Background</label>
                                            <input name="service_provider_academic_background" value="" type="text" class="form-control" placeholder="Input English Test Name"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Job Experience (if any)</label>
                                            <input name="service_provider_experience" value="" type="text" class="form-control" placeholder="Input Overall Score"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                            <div class="form-actions right">
                                <button type="submit" class="btn blue"> <i class="fa fa-check"></i>Submit</button>
                            </div>
                        </form> <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> @endsection