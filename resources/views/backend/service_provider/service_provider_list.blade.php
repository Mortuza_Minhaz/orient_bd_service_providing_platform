@extends('backend.layouts.app')

@section('title', 'Service Provider List')


@section('js')

<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "lengthMenu": [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ],
            "pageLength": -1
        });
    });
</script>


@endsection
@section('css')
<style>

</style>
@endsection


@section('content')

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('backend.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title" style="min-height:21px">
                    <div class="caption" style="font-size: 14px;padding:1px 0 1px;">

                    </div>
                </div>
                <div class="portlet-body">

                    <div class="row">
                        <div class="col-md-12">

                            <form class="form-horizontal" method="post" id="report_filter" action="{{route('service_provider_list_admin')}}">

                                @csrf
                                <div class="col-sm-12">
                                    <div style="background-color: grey!important;">

                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">District</label>
                                                <div class="col-md-8">
                                                    <select class="form-control select2" name="service_district" onchange="document.getElementById('report_filter').submit();">
                                                        <option value="">Select District</option>

                                                        @foreach($districts_list as $item)

                                                        <option value="{{$item->id}}">{{$item->name}}</option>

                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i> Service Provider List </div>
                    <div class="tools"><button> <a style="color:#f16522; text-decoration:none;" href="{{route('service_provider_list_admin')}}"><i class="fa fa-list" style="color:#f16522"></i> List ALL
                            </a></button></div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="example" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="all">#</th>
                                <th class="min-phone-l">Service Provider Type</th>
                                <th class="min-phone-l">Provider/Company Name</th>
                                <th class="min-phone-l">Location</th>
                                <th class="min-tablet">Provider Contact Number</th>
                                <!-- <th class="desktop">Approval</th> -->
                                <th class="all">***</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($service_provider_list as $item)
                            <tr>

                                <td>{{$loop->iteration}}</td>
                                @if($item->company_or_individual == 1)
                                <td>Individual</td>
                                @elseif($item->company_or_individual == 2)
                                <td>Company</td>
                                @else
                                <td></td>
                                @endif
                                <td>{{$item->service_provider_name}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->service_provider_contact}}</td>

                                <!-- <td>
                                    <input type="checkbox" data-size="mini" class="make-switch" checked data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>">
                                </td> -->

                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Action
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-left" role="menu">
                                            <li>
                                                <a style="border: none;" href="{{route('service_provider_profile', $item->id)}}">
                                                    <span class="item">
                                                        <span aria-hidden="true" class="icon-user"></span> Profile</a>
                                            </li>

                                            <li>
                                                <a style="border: none;" href="{{route('service_provider_details', $item->id)}}">
                                                    <span class="item">
                                                        <span aria-hidden="true" class="icon-eye"></span> Details</a>
                                            </li>

                                            <li>
                                                <a style="border: none;" href="{{route('deleteProvider', $item->id)}}" onclick="return confirm('Are You Sure?')"><i class="fa fa-trash"></i> Remove</a>
                                            </li>

                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>

</div>

<!-- END CONTENT BODY -->
@endsection