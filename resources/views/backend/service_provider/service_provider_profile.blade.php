@extends('backend.layouts.app')

@section('title', 'Service Provider Profile')


@section('js')


@endsection
@section('css')
<style>

</style>
@endsection


@section('content')

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('backend.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="profile">

        <div class="tab-content">
            <div class="tab-pane active" id="tab_1_1">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="list-unstyled profile-nav">
                            @if(isset($service_providers->individual_provider_prof_pic))
                            <li>
                                <img style="height:390px;width:313px;" src="{{$service_providers->individual_provider_prof_pic}}" class="img-responsive pic-bordered" alt="" />
                                <!-- <a href="javascript:;" class="profile-edit"> edit </a> -->
                            </li>
                            @endif

                            @if(isset($service_providers->company_owner_pic))
                            <li>
                                <img style="height:390px;width:313px;" src="{{$service_providers->company_owner_pic}}" class="img-responsive pic-bordered" alt="" />
                                <!-- <a href="javascript:;" class="profile-edit"> edit </a> -->
                            </li>
                            @endif
                            <!-- <li>
                                <a href="javascript:;"> Messages
                                    <span> 3 </span>
                                </a>
                            </li>

                            <li>
                                <a href="javascript:;"> Settings </a>
                            </li> -->
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-8 profile-info">
                                <h1 class="font-green sbold uppercase">{{$service_providers->service_provider_name}}</h1>


                                <p> {{$service_providers->service_provider_description}} </p>

                                @if(isset($service_providers->service_provider_company_name))
                                <p>
                                    <p class="font-green sbold uppercase"> Company: {{$service_providers->service_provider_company_name}}</p>
                                </p>
                                @endif
                                <ul class="list-inline">
                                    <li>
                                        <i class="fa fa-map-marker"></i> {{$service_providers->service_provider_details_address}} </li>
                                    <li>
                                        <i class="fa fa-phone"></i> {{$service_providers->service_provider_contact}} </li>
                                    <li>
                                        <i class="fa fa-envelope"></i> {{$service_providers->service_provider_email}} </li>
                                </ul>
                            </div>
                            <!--end col-md-8-->
                            <!-- <div class="col-md-4">
                                <div class="portlet sale-summary">
                                    <div class="portlet-title">
                                        <div class="caption font-red sbold"> Service Summary </div>
                                        <div class="tools">
                                            <a class="reload" href="javascript:;"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <ul class="list-unstyled">
                                            <li>
                                                <span class="sale-info"> CURRENT SERVICE
                                                    <i class="fa fa-img-up"></i>
                                                </span>
                                                <span class="sale-num"> 23 </span>
                                            </li>
                                            <li>
                                                <span class="sale-info"> WEEKLY SERVICE
                                                    <i class="fa fa-img-down"></i>
                                                </span>
                                                <span class="sale-num"> 87 </span>
                                            </li>
                                            <li>
                                                <span class="sale-info"> TOTAL ACCOMPLISHED SERVICE </span>
                                                <span class="sale-num"> 2377 </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div> -->
                            <!--end col-md-4-->
                        </div>
                        <!--end row-->
                        <div class="tabbable-line tabbable-custom-profile">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_11" data-toggle="tab"> Uploaded Files </a>
                                </li>
                                <li>
                                    <a href="#tab_1_22" data-toggle="tab"> <i class="icon-wrench"></i> Services </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_11">
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <i class="icon-doc"></i> Documents </th>
                                                    <th class="hidden-xs">
                                                        <i class="fa fa-file-text-o"></i> Description </th>

                                                    <th> </th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @if(isset($service_providers->individual_provider_cv))
                                                <tr>
                                                    @php
                                                    $path_parts = pathinfo($service_providers->individual_provider_cv);
                                                    @endphp
                                                    <td>
                                                        <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{asset($service_providers->individual_provider_cv)}}" download="{{ substr($path_parts['basename'],11) }}">
                                                            {{substr($path_parts['basename'], 11) }} <i class="las la-download"></i>
                                                        </a>

                                                    </td>
                                                    <td class="hidden-xs"> Updated CV </td>
                                                    <!-- <td> 52560.10$
                                                        <span class="label label-success label-sm"> Paid </span>
                                                    </td> -->
                                                    <td>
                                                        <a class="btn purple btn-outline sbold" data-toggle="modal" href="#cv"> View </a>

                                                    </td>
                                                </tr>
                                                @endif
                                                @if(isset($service_providers->individual_provider_nid))
                                                <tr>
                                                    @php
                                                    $path_parts = pathinfo($service_providers->individual_provider_nid);
                                                    @endphp
                                                    <td>
                                                        <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{asset($service_providers->individual_provider_nid)}}" download="{{ substr($path_parts['basename'],11) }}">
                                                            {{substr($path_parts['basename'], 11) }} <i class="las la-download"></i>
                                                        </a>
                                                    </td>
                                                    <td class="hidden-xs"> NID</td>
                                                    <!-- <td> 5760.00$
                                                        <span class="label label-warning label-sm"> Pending </span>
                                                    </td> -->
                                                    <td>
                                                        <a class="btn purple btn-outline sbold" data-toggle="modal" href="#nid"> View </a>

                                                    </td>
                                                </tr>
                                                @endif
                                                @if(isset($service_providers->individual_provider_tin))
                                                <tr>
                                                    @php
                                                    $path_parts = pathinfo($service_providers->individual_provider_tin);
                                                    @endphp
                                                    <td>
                                                        <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{asset($service_providers->individual_provider_tin)}}" download="{{ substr($path_parts['basename'],11) }}">
                                                            {{substr($path_parts['basename'], 11) }} <i class="las la-download"></i>
                                                        </a>
                                                    </td>
                                                    <td class="hidden-xs"> TIN </td>

                                                    <td>
                                                        <a class="btn purple btn-outline sbold" data-toggle="modal" href="#tin"> View </a>

                                                    </td>
                                                </tr>
                                                @endif
                                                @if(isset($service_providers->individual_provider_testimonial))
                                                <tr>
                                                    @php
                                                    $path_parts = pathinfo($service_providers->individual_provider_testimonial);
                                                    @endphp
                                                    <td>
                                                        <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{asset($service_providers->individual_provider_testimonial)}}" download="{{ substr($path_parts['basename'],11) }}">
                                                            {{substr($path_parts['basename'], 11) }} <i class="las la-download"></i>
                                                        </a>
                                                    </td>
                                                    <td class="hidden-xs"> Testimonial </td>
                                                    <!-- <td> 610.50$
                                                        <span class="label label-danger label-sm"> Overdue </span>
                                                    </td> -->
                                                    <td>
                                                        <a class="btn purple btn-outline sbold" data-toggle="modal" href="#testimonial"> View </a>

                                                    </td>
                                                </tr>
                                                @endif
                                                @if(isset($service_providers->individual_provider_experience))
                                                <tr>
                                                    @php
                                                    $path_parts = pathinfo($service_providers->individual_provider_experience);
                                                    @endphp
                                                    <td>
                                                        <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{asset($service_providers->individual_provider_experience)}}" download="{{ substr($path_parts['basename'],11) }}">
                                                            {{substr($path_parts['basename'], 11) }} <i class="las la-download"></i>
                                                        </a>
                                                    </td>
                                                    <td class="hidden-xs"> Working Experience </td>

                                                    <td>
                                                        <a class="btn purple btn-outline sbold" data-toggle="modal" href="#experience"> View </a>

                                                    </td>
                                                </tr>
                                                @endif
                                                @if(isset($service_providers->individual_provider_noc))
                                                <tr>
                                                    @php
                                                    $path_parts = pathinfo($service_providers->individual_provider_noc);
                                                    @endphp
                                                    <td>
                                                        <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{asset($service_providers->individual_provider_noc)}}" download="{{ substr($path_parts['basename'],11) }}">
                                                            {{substr($path_parts['basename'], 11) }} <i class="las la-download"></i>
                                                        </a>
                                                    </td>
                                                    <td class="hidden-xs"> NOC </td>

                                                    <td>
                                                        <a class="btn purple btn-outline sbold" data-toggle="modal" href="#noc"> View </a>

                                                    </td>
                                                </tr>
                                                @endif
                                                @if(isset($service_providers->individual_provider_tender))
                                                <tr>
                                                    @php
                                                    $path_parts = pathinfo($service_providers->individual_provider_tender);
                                                    @endphp
                                                    <td>
                                                        <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{asset($service_providers->individual_provider_tender)}}" download="{{ substr($path_parts['basename'],11) }}">
                                                            {{substr($path_parts['basename'], 11) }} <i class="las la-download"></i>
                                                        </a>
                                                    </td>
                                                    <td class="hidden-xs"> Tender </td>

                                                    <td>
                                                        <a class="btn purple btn-outline sbold" data-toggle="modal" href="#tender"> View </a>

                                                    </td>
                                                </tr>

                                                <!-- start Company documents -->
                                                @endif
                                                @if(isset($service_providers->company_profile_details))
                                                <tr>
                                                    @php
                                                    $path_parts = pathinfo($service_providers->company_profile_details);
                                                    @endphp
                                                    <td>
                                                        <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{asset($service_providers->company_profile_details)}}" download="{{ substr($path_parts['basename'],11) }}">
                                                            {{substr($path_parts['basename'], 11) }} <i class="las la-download"></i>
                                                        </a>
                                                    </td>
                                                    <td class="hidden-xs"> Company Profile Details </td>

                                                    <td>
                                                        <a class="btn purple btn-outline sbold" data-toggle="modal" href="#company_profile"> View </a>

                                                    </td>
                                                </tr>
                                                @endif
                                                @if(isset($service_providers->company_nid))
                                                <tr>
                                                    @php
                                                    $path_parts = pathinfo($service_providers->company_nid);
                                                    @endphp
                                                    <td>
                                                        <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{asset($service_providers->company_nid)}}" download="{{ substr($path_parts['basename'],11) }}">
                                                            {{substr($path_parts['basename'], 11) }} <i class="las la-download"></i>
                                                        </a>
                                                    </td>
                                                    <td class="hidden-xs"> Company NID </td>

                                                    <td>
                                                        <a class="btn purple btn-outline sbold" data-toggle="modal" href="#company_nid"> View </a>

                                                    </td>
                                                </tr>
                                                @endif
                                                @if(isset($service_providers->company_business_license))
                                                <tr>
                                                    @php
                                                    $path_parts = pathinfo($service_providers->company_business_license);
                                                    @endphp
                                                    <td>
                                                        <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{asset($service_providers->company_business_license)}}" download="{{ substr($path_parts['basename'],11) }}">
                                                            {{substr($path_parts['basename'], 11) }} <i class="las la-download"></i>
                                                        </a>
                                                    </td>
                                                    <td class="hidden-xs"> Company Business License </td>

                                                    <td>
                                                        <a class="btn purple btn-outline sbold" data-toggle="modal" href="#company_license"> View </a>

                                                    </td>
                                                </tr>
                                                @endif
                                                @if(isset($service_providers->company_tin))
                                                <tr>
                                                    @php
                                                    $path_parts = pathinfo($service_providers->company_tin);
                                                    @endphp
                                                    <td>
                                                        <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{asset($service_providers->company_tin)}}" download="{{ substr($path_parts['basename'],11) }}">
                                                            {{substr($path_parts['basename'], 11) }} <i class="las la-download"></i>
                                                        </a>
                                                    </td>
                                                    <td class="hidden-xs"> Company TIN </td>

                                                    <td>
                                                        <a class="btn purple btn-outline sbold" data-toggle="modal" href="#company_tin"> View </a>

                                                    </td>
                                                </tr>
                                                @endif
                                                @if(isset($service_providers->company_contract_paper))
                                                <tr>
                                                    @php
                                                    $path_parts = pathinfo($service_providers->company_contract_paper);
                                                    @endphp
                                                    <td>
                                                        <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{asset($service_providers->company_contract_paper)}}" download="{{ substr($path_parts['basename'],11) }}">
                                                            {{substr($path_parts['basename'], 11) }} <i class="las la-download"></i>
                                                        </a>
                                                    </td>
                                                    <td class="hidden-xs"> Company Contract Paper</td>

                                                    <td>
                                                        <a class="btn purple btn-outline sbold" data-toggle="modal" href="#company_contract_paper"> View </a>

                                                    </td>
                                                </tr>
                                                @endif
                                                @if(isset($service_providers->company_tender))
                                                <tr>
                                                    @php
                                                    $path_parts = pathinfo($service_providers->company_tender);
                                                    @endphp
                                                    <td>
                                                        <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{asset($service_providers->company_tender)}}" download="{{ substr($path_parts['basename'],11) }}">
                                                            {{substr($path_parts['basename'], 11) }} <i class="las la-download"></i>
                                                        </a>
                                                    </td>
                                                    <td class="hidden-xs"> Company Tender </td>

                                                    <td>
                                                        <a class="btn purple btn-outline sbold" data-toggle="modal" href="#company_tender"> View </a>
                                                        <!-- <a class="btn btn-sm grey-salsa btn-outline" href="javascript:;"> View </a> -->
                                                    </td>
                                                </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!--tab-pane-->
                                <div class="tab-pane" id="tab_1_22">
                                    <div class="tab-pane active" id="tab_1_1_1">
                                        <div class="scroller" data-height="290px" data-always-visible="1" data-rail-visible1="1">
                                            <ul class="feeds">
                                                @foreach($service_list as $service)

                                                <li>
                                                    <div class="col1">
                                                        <div class="cont">
                                                            <div class="cont-col1">
                                                                <div class="label label-success">
                                                                    <i class="fa fa-bolt"></i>
                                                                </div>
                                                            </div>
                                                            <div class="cont-col2">
                                                                @isset($service->serviceinfo->service_title)
                                                                <div class="desc"> {{$service->serviceinfo->service_title}} </div>
                                                                @endisset
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col2">
                                                        <div class="date"> </div>
                                                    </div>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--tab-pane-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /.modal -->
            <div class="modal fade bs-modal-lg" id="cv" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body" style="height: 600px;"> <iframe src="{{asset($service_providers->individual_provider_cv)}}" height="600px" width="100%"></iframe> </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <!-- /.modal -->
            <div class="modal fade bs-modal-lg" id="nid" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body" style="height: 600px;"> <iframe src="{{asset($service_providers->individual_provider_nid)}}" height="600px" width="100%"></iframe> </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <!-- /.modal -->
            <div class="modal fade bs-modal-lg" id="tin" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body" style="height: 600px;"> <iframe src="{{asset($service_providers->individual_provider_tin)}}" height="100%" width="100%"></iframe> </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <!-- /.modal -->
            <div class="modal fade bs-modal-lg" id="testimonial" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body" style="height: 600px;"> <iframe src="{{asset($service_providers->individual_provider_testimonial)}}" height="600" width="100%"></iframe> </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <!-- /.modal -->
            <div class="modal fade bs-modal-lg" id="experience" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body" style="height: 600px;"> <iframe src="{{asset($service_providers->individual_provider_experience)}}" height="100%" width="100%"></iframe> </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <!-- /.modal -->
            <div class="modal fade bs-modal-lg" id="noc" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body" style="height: 600px;"> <iframe src="{{asset($service_providers->individual_provider_noc)}}" height="100%" width="100%"></iframe> </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <!-- /.modal -->
            <div class="modal fade bs-modal-lg" id="tender" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body" style="height: 600px;"> <iframe src="{{asset($service_providers->individual_provider_tender)}}" height="100%" width="100%"></iframe> </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <!-- /.modal -->
            <div class="modal fade bs-modal-lg" id="company_profile" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body" style="height: 600px;"> <iframe src="{{asset($service_providers->company_profile_details)}}" height="100%" width="100%"></iframe> </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <!-- /.modal -->
            <div class="modal fade bs-modal-lg" id="company_nid" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body" style="height: 600px;"> <iframe src="{{asset($service_providers->company_nid)}}" height="100%" width="100%"></iframe> </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <!-- /.modal -->
            <div class="modal fade bs-modal-lg" id="company_license" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body" style="height: 600px;"> <iframe src="{{asset($service_providers->company_business_license)}}" height="100%" width="100%"></iframe> </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <!-- /.modal -->
            <div class="modal fade bs-modal-lg" id="company_tin" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body" style="height: 600px;"> <iframe src="{{asset($service_providers->company_tin)}}" height="100%" width="100%"></iframe> </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <!-- /.modal -->
            <div class="modal fade bs-modal-lg" id="company_contract_paper" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body" style="height: 600px;"> <iframe src="{{asset($service_providers->company_contract_paper)}}" height="100%" width="100%"></iframe> </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <!-- /.modal -->
            <div class="modal fade bs-modal-lg" id="company_tender" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body" style="height: 600px;"> <iframe src="{{asset($service_providers->company_tender)}}" height="100%" width="100%"></iframe> </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

        </div>
    </div>

    <!-- <iframe src="{{asset($service_providers->individual_provider_cv)}}" width="100%" height="500px"></iframe> -->

</div>

<!-- END CONTENT BODY -->
@endsection