<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <!-- END SIDEBAR TOGGLER BUTTON -->


            <li class="nav-item {{Request::is('home*') ? 'active open' : ''}} ">
                <a href="javascript:;" class="nav-link  nav-toggle">
                    <i class="icon-basket"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item ">
                        <a href="#" class="nav-link ">
                            <i class="icon-home"></i>
                            <span class="title">Home</span>
                            <span class="selected"></span>
                        </a>
                    </li>


                </ul>
            </li>
            <!-- add service -->
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-list-ul"></i>
                    <span class="title">Service</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">


                    <li class="nav-item  ">
                        <a href="" class="nav-link ">
                            <i class="icon-check"></i>
                            <span class="title">Service Add</span>
                        </a>
                    </li>



                </ul>
            </li>
            <!-- end add service -->
            <!-- Start Order Management -->

            <li class="nav-item {{Request::is('orders*') ? 'active open' : ''}} ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-list-ul"></i>
                    <span class="title">Order List</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">


                    <li class="nav-item {{Request::is('orders/pending-list') ? 'active open' : ''}} ">
                        <a href="{{ route('pending_order_list') }}" class="nav-link ">
                            <i class="icon-check"></i>
                            <span class="title">Pending Order List</span>
                        </a>
                    </li>

                    <li class="nav-item {{Request::is('orders/assigned-list') ? 'active open' : ''}} ">
                        <a href="{{ route('assigned_order_list') }}" class="nav-link ">
                            <i class="icon-check"></i>
                            <span class="title">Assigned Order List</span>
                        </a>
                    </li>

                </ul>
            </li>

            <!-- End Order Management -->

            <!-- Start Service Provider Panel -->

            <li class="nav-item {{Request::is('service-providers*') ? 'active open' : ''}} ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-gears"></i>
                    <span class="title">Service Providers</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">


                    <li class="nav-item {{Request::is('service-providers-list') ? 'active open' : ''}} ">
                        <a href="{{ route('service_provider_list_admin') }}" class="nav-link ">
                            <i class="icon-check"></i>
                            <span class="title">All Service Providers</span>
                        </a>
                    </li>

                </ul>
            </li>

            <!-- End Service Provider Panel -->

            <!-- Start Service Engineer Panel -->

            <li class="nav-item {{Request::is('engineers*') ? 'active open' : ''}} ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-gears"></i>
                    <span class="title">In House Engineers</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">

                    <li class="nav-item {{Request::is('engineers') ? 'active open' : ''}} ">
                        <a href="{{ route('engineers.index') }}" class="nav-link ">
                            <i class="icon-check"></i>
                            <span class="title">All Engineers</span>
                        </a>
                    </li>

                </ul>
            </li>

            <!-- End Service Engineer Panel -->

            <li class="nav-item ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-list"></i>
                    <span class="title">Category</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">


                    <li class="nav-item  ">
                        <a href="{{ route('categories.create') }}" class="nav-link ">
                            <i class="icon-check"></i>
                            <span class="title">Add Category</span>
                        </a>
                    </li>

                    <li class="nav-item ">
                        <a href="{{route('categories.index')}}" class="nav-link ">
                            <i class="icon-check"></i>
                            <span class="title">Category List</span>
                        </a>
                    </li>

                </ul>
            </li>

            <li class="nav-item ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-registered"></i>
                    <span class="title">Brand</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">


                    <li class="nav-item  ">
                        <a href="{{ route('brands.create') }}" class="nav-link ">
                            <i class="icon-check"></i>
                            <span class="title">Add Brand</span>
                        </a>
                    </li>

                    <li class="nav-item ">
                        <a href="{{route('brands.index')}}" class="nav-link ">
                            <i class="icon-check"></i>
                            <span class="title">Brand List</span>
                        </a>
                    </li>

                </ul>
            </li>


            <li class="nav-item ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-list"></i>
                    <span class="title">Warranty</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="{{ route('warranty.show') }}" class="nav-link ">
                            <i class="icon-check"></i>
                            <span class="title">Claim List</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-list"></i>
                    <span class="title">Rental</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="{{ route('rental.show') }}" class="nav-link ">
                            <i class="icon-check"></i>
                            <span class="title">Rental List</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-list"></i>
                    <span class="title">Site Visit</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="{{ route('site.visit.show') }}" class="nav-link ">
                            <i class="icon-check"></i>
                            <span class="title">Apply List</span>
                        </a>
                    </li>
                </ul>
            </li>


            <!-- Start Coupon Panel -->
            <li class="nav-item {{Request::is('coupon*') ? 'active open' : ''}} ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-docs"></i>
                    <span class="title">Manage Coupon</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">


                    <li class="nav-item {{Request::is('coupon/create') ? 'active open' : ''}} ">
                        <a href="{{ route('coupon.create') }}" class="nav-link ">
                            <i class="icon-check"></i>
                            <span class="title">Create New Coupon</span>
                        </a>
                    </li>


                    <li class="nav-item {{Request::is('coupon') ? 'active open' : ''}} ">
                        <a href="{{ route('coupon.index') }}" class="nav-link ">
                            <i class="icon-clock"></i>
                            <span class="title">Coupon List</span>
                        </a>
                    </li>

                </ul>
            </li>
            <!-- End Coupon Panel -->


            <!-- Start Uploaded Image Panel -->

            <li class="nav-item {{Request::is('photos') ? 'active open' : ''}} ">
                <a href="{{ route('photos.index') }}" class="nav-link ">
                    <i class="fa fa-image"></i>
                    <span class="title">Uploaded Image Files</span>
                </a>
            </li>


            <!-- End Uploaded Image Panel -->

            <li class="nav-item ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-list"></i>
                    <span class="title">Service List</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">


                    <li class="nav-item  ">
                        <a href="{{ route('services.create') }}" class="nav-link ">
                            <i class="icon-check"></i>
                            <span class="title">Create Service</span>
                        </a>
                    </li>

                    <li class="nav-item ">
                        <a href="{{route('services.index')}}" class="nav-link ">
                            <i class="icon-check"></i>
                            <span class="title">Service List</span>
                        </a>
                    </li>



                </ul>
            </li>



            <li class="nav-item {{Request::is('users*') ? 'active open' : ''}} ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-docs"></i>
                    <span class="title">Manage Users</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">


                    <li class="nav-item {{Request::is('users/create') ? 'active open' : ''}} ">
                        <a href="{{ route('users.create') }}" class="nav-link ">
                            <i class="icon-check"></i>
                            <span class="title">Create New User</span>
                        </a>
                    </li>


                    <li class="nav-item {{Request::is('users') ? 'active open' : ''}} ">
                        <a href="{{ route('users.index') }}" class="nav-link ">
                            <i class="icon-clock"></i>
                            <span class="title">Users List</span>
                        </a>
                    </li>

                </ul>
            </li>

            <li class="nav-item {{Request::is('roles*') ? 'active open' : ''}}  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-docs"></i>
                    <span class="title">Manage Role</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">

                    @can('role-create')
                    <li class="nav-item {{Request::is('roles/create') ? 'active open' : ''}}  ">
                        <a href="{{ route('roles.create') }}" class="nav-link ">
                            <i class="icon-check"></i>
                            <span class="title">Create New Role</span>
                        </a>
                    </li>
                    @endcan

                    <li class="nav-item {{Request::is('roles') ? 'active open' : ''}} ">
                        <a href="{{ route('roles.index') }}" class="nav-link ">
                            <i class="icon-clock"></i>
                            <span class="title">Role List</span>
                        </a>
                    </li>

                </ul>
            </li>

        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
