@extends('backend.layouts.app')

@section('title', ' ')


@section('js')


@endsection
@section('css')
<style>

</style>
@endsection


@section('content')

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('backend.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12" style="margin-top:20px">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>Service create Form
                    </div>
                    <div class="actions">
                        <!-- <div class="btn-set pull-right">
                            <a href="{{route('photos.index')}}"><button type="button" class="btn red">List</button></a>
                        </div> -->
                    </div>

                </div>
                <div class="portlet-body form">

                    <!-- BEGIN FORM-->
                    <form action="{{route('brands.store')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <div class="form-body">

                            <div class="form-group">
                                <label class="col-md-3 control-label">Brand Name</label>
                                <div class="col-md-4">
                                    <input name="name" type="text" class="form-control" placeholder="write Brand name">
                                </div>
                            </div>

                            <div class="form-group">
                            <label class="col-md-3 control-label">Brand Logo</label>
                            <div class="col-md-4">
                                <input name="logo" type="file" class="form-control">
                                {!! $errors->first('logo', '<small class="text-danger">:message</small>') !!}

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Meta Title</label>
                            <div class="col-md-4">
                                <input name="meta_title" value="" type="text" class="form-control" placeholder="Input Meta Title">
                                {!! $errors->first('student_residence', '<small class="text-danger">:message</small>') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Meta description</label>
                            <div class="col-md-4">
                                <textarea name="meta_description" value="" rows="4" cols="50"  type="text" class="form-control" placeholder=""></textarea>
                                {!! $errors->first('student_name', '<small class="text-danger">:message</small>') !!}
                            </div>
                        </div>


                        </div>

                        <div class="form-actions top">
                            <div class="row">
                                <div class="col-md-offset-4 col-md-8">
                                    <button type="submit" class="btn green">Submit</button>
                                   
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>


</div>
<!-- END CONTENT BODY -->
@endsection