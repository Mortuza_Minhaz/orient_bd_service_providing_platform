@extends('backend.layouts.app')

@section('title', 'Service List')


@section('js')


@endsection
@section('css')
<style>
    .modal-dialog {
        padding-top: 10% !important;
    }

    .modal-body .form .form-body .form-group {
        padding: 15px !important;

    }
</style>
@endsection


@section('content')

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('backend.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i> Brand List
                    </div>
                    <div style="color:black" class="tools"><a style="color:black;" href="{{route('brands.create')}}"><i class="fa fa-plus" style="color:black "></i> Add New Brand
                        </a></div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_3" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="all">#</th>
                                <th class="min-phone-l">Name</th>
                                <th class="desktop"> Logo</th>

                                <th class="all">***</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($brand_list as $key=>$item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{$item->name}}</td>
                                <td><img style="height:50px;width:50px;" src="{{$item->logo}}"></td>

                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Action
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-left" role="menu">
                                            <li>
                                                <a style="border: none;" href="" data-toggle="modal" data-target="#myModal{{$key}}">
                                                    <span class="item">
                                                        <span aria-hidden="true" class="icon-note icons"></span> Edit</a>
                                            </li>

                                            <li>
                                                <a style="border: none;" href="{{route('delete_brand_list',$item->id )}}"><i class="fa fa-trash"></i> Remove</a>
                                            </li>

                                        </ul>
                                    </div>
                                </td>
                            </tr>

                            <div class="modal fade" id="myModal{{$key}}" role="dialog">
                                <div class="modal-dialog">


                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;
                                            </button>
                                            <h4 class="modal-title"></h4>
                                        </div>
                                        <div class="modal-body">


                                            <div class="portlet box blue">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-file-text"></i>Brand Edit
                                                    </div>


                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="/admin/brands/{{$item->id}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                                                        @csrf
                                                        @method('PUT')

                                                        <div class="form-body">

                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Name</label>
                                                                <div class="col-md-8">
                                                                    <input name="name" value="{{$item->name}}" type="text" class="form-control" placeholder="Enter Brand Name">


                                                                    {!! $errors->first('service_title', '<small class="text-danger">:message</small>') !!}
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Meta
                                                                    Title</label>
                                                                <div class="col-md-8">
                                                                    <input name="meta_title" value="{{$item->meta_title}}" type="text" class="form-control" placeholder="Enter Brand Name">


                                                                    {!! $errors->first('meta_title', '<small class="text-danger">:message</small>') !!}
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                            <label class="col-md-3 control-label">Brand Logo</label>
                            <div class="col-md-8">
                                <input name="logo" type="file" class="form-control">
                                {!! $errors->first('logo', '<small class="text-danger">:message</small>') !!}

                            </div>
                        </div>

                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Meta
                                                                    Description</label>
                                                                <div class="col-md-8">
                                                                    <textarea name="meta_description" value="" rows="4" cols="50"  type="text" class="form-control">{{$item->meta_description}}</textarea>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="form-actions top">
                                                            <div class="row">
                                                                <div class="form-group">
                                                                    <div class="col-md-8 col-md-offset-4">
                                                                        <button type="submit" class="btn green">Submit
                                                                        </button>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>

                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>

    <!-- END CONTENT BODY -->
    @endsection