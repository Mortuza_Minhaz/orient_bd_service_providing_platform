@extends('backend.layouts.app')

@section('title', 'Users Create')


@section('js')

    <script>
        CKEDITOR.replace('service_overview');
        CKEDITOR.replace('faq');


    </script>


@endsection
@section('css')

@endsection


@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

    @include('backend/include/beginPageHeader')

    {{--        @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif--}}
    <!-- END PAGE HEADER-->

        <div class="row">
            @foreach($parentCategories as $category )
                <li class="treeview">
                    <a href="#"><i class="fa fa-link"></i> <span>{{$category->name}}</span>
                        <i class="fa fa-angle-left pull-right"></i></a>
                    @if(count($category->categoryWisePrice))
                        @include('/pages/include/category_wise_price',['categoryWisePrice' => $category->categoryWisePrice])
                    @endif

                    @if(count($category->subcategory))
                        @foreach($category->subcategory as $subcategory)
                            <ul class="treeview-menu">
                                <li class=""><a href="#">{{$subcategory->name}}</a>
                                    @if(count($subcategory->categoryWisePrice))
                                        @include('/pages/include/category_wise_price',['categoryWisePrice' => $category->categoryWisePrice])
                                    @endif
                                </li>

                            </ul>
                        @endforeach
                    @endif
                </li>
            @endforeach
        </div>

    </div>
    <!-- END CONTENT BODY -->
@endsection
