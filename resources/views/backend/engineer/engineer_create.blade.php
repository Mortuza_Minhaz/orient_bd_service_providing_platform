@extends('backend.layouts.app')

@section('title', 'Create Engineer Form')


@section('js')

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.onload = function(e) {
                $('.image-upload-wrap').hide();

                $('.file-upload-image').attr('src', e.target.result);
                $('.file-upload-content').show();

                $('.image-title').html(input.files[0].name);
            };

            reader.readAsDataURL(input.files[0]);

        } else {
            removeUpload();
        }
    }

    function removeUpload() {
        $('.file-upload-input').replaceWith($('.file-upload-input').clone());
        $('.file-upload-content').hide();
        $('.image-upload-wrap').show();
    }
    $('.image-upload-wrap').bind('dragover', function() {
        $('.image-upload-wrap').addClass('image-dropping');
    });
    $('.image-upload-wrap').bind('dragleave', function() {
        $('.image-upload-wrap').removeClass('image-dropping');
    });
</script>


@endsection


@section('css')

<style>
    #ssd {
        resize: none;
    }

    .file-upload {
        background-color: #ffffff;
        width: 600px;
        margin: 0 auto;
        padding: 20px;
    }

    .file-upload-btn {
        width: 100%;
        margin: 0;
        color: #fff;
        background: #1FB264;
        border: none;
        padding: 10px;
        border-radius: 4px;
        border-bottom: 4px solid #15824B;
        transition: all .2s ease;
        outline: none;
        text-transform: uppercase;
        font-weight: 700;
    }

    .file-upload-btn:hover {
        background: #1AA059;
        color: #ffffff;
        transition: all .2s ease;
        cursor: pointer;
    }

    .file-upload-btn:active {
        border: 0;
        transition: all .2s ease;
    }

    .file-upload-content {
        display: none;
        text-align: center;
    }

    .file-upload-input {
        position: absolute;
        margin: 0;
        padding: 0;
        width: 100%;
        height: 100%;
        outline: none;
        opacity: 0;
        cursor: pointer;
    }

    .image-upload-wrap {
        margin-top: 20px;
        border: 4px dashed #1FB264;
        position: relative;
    }

    .image-dropping,
    .image-upload-wrap:hover {
        background-color: #1FB264;
        border: 4px dashed #ffffff;
    }

    .image-title-wrap {
        padding: 0 15px 15px 15px;
        color: #222;
    }

    .drag-text {
        text-align: center;
    }

    .drag-text h3 {
        font-weight: 100;
        text-transform: uppercase;
        color: #15824B;
        padding: 60px 0;
    }

    .file-upload-image {
        max-height: 200px;
        max-width: 200px;
        margin: auto;
        padding: 20px;
    }

    .remove-image {
        width: 200px;
        margin: 0;
        color: #fff;
        background: #cd4535;
        border: none;
        padding: 10px;
        border-radius: 4px;
        border-bottom: 4px solid #b02818;
        transition: all .2s ease;
        outline: none;
        text-transform: uppercase;
        font-weight: 700;
    }

    .remove-image:hover {
        background: #c13b2a;
        color: #ffffff;
        transition: all .2s ease;
        cursor: pointer;
    }

    .remove-image:active {
        border: 0;
        transition: all .2s ease;
    }
</style>

@endsection



@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('backend.include.beginPageHeader')


    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="tab-pane" id="tab_1">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-form"></i>Engineer Form</div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="{{route('engineers.store')}}" method="POST" class="horizontal-form" enctype="multipart/form-data">
                            @csrf
                            <div class="form-body">
                                <h3 class="form-section">Person Info</h3>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Select Expertise on Services</label>

                                            <div class="input-group select2-bootstrap-append">
                                                <select name="expertise_on_service[]" id="multi-append" class="form-control select2" multiple>
                                                    <option></option>

                                                    <option value="Ac Servic">Ac Servic</option>
                                                    <option value="Ac Water Drop">Ac Water Drop</option>
                                                    <option value="Ac Gas Refill">Ac Gas Refill</option>

                                                </select>

                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button" data-select2-open="multi-append">
                                                        <i class="fa fa-arrow-down"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Engineer Name</label>
                                            <input name="engineer_name" value="" type="text" class="form-control" placeholder="Input Student Name">


                                            {!! $errors->first('engineer_name', '<small class="text-danger">:message</small>') !!}
                                        </div>
                                    </div>

                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">

                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Mobile Number</label>
                                            <input name="engineer_contact" value="" type="text" class="form-control" placeholder="Input Student Mobile Number">

                                            {!! $errors->first('engineer_contact', '<small class="text-danger">:message</small>') !!} </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Date of Birth</label>
                                            <input type="text" class="date-picker form-control" id="datepicker" name="engineer_dob" value="<?php echo date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd" placeholder="">


                                            {!! $errors->first('engineer_dob', '<small class="text-danger">:message</small>') !!} </div>
                                    </div>

                                </div>
                                <!--/row-->
                                <div class="row">

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Location Covered</label>
                                        <div class="col-md-12">

                                            <textarea class="form-control" value="" name="engineer_location" id="ssd" rows="3" placeholder=""></textarea>

                                            {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                        </div>
                                    </div>
                                    <!--/span-->

                                    <!--/span-->
                                </div>
                                <!--/row-->

                                <div class="file-upload">
                                    <div class="image-upload-wrap ">
                                        <input name="engineer_image_path" class="file-upload-input" type='file' onchange="readURL(this);" accept="image/*" />
                                        <div class="drag-text">
                                            <h3>Drag and drop a file or select to add File</h3>
                                        </div>
                                    </div>
                                    <div class="file-upload-content">
                                        <img class="file-upload-image" src="#" alt="" />
                                        <div class="image-title-wrap">
                                            <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="form-actions right">

                                <button type="submit" class="btn blue">
                                    <i class="fa fa-check"></i>Submit</button>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>


@endsection