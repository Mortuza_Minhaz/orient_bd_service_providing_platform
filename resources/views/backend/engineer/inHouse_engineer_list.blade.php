@extends('backend.layouts.app')

@section('title', 'Engineers List')


@section('js')


@endsection
@section('css')
<style>

</style>
@endsection


@section('content')

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('backend.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i> In House Engineers List </div>
                    <div style="color:black" class="tools"> <a style="color:black;" href="{{route('engineers.create')}}"><i class="fa fa-plus" style="color:black "></i> Add New Engineer
                        </a></div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_3" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="all">#</th>
                                <th class="min-phone-l">Engineer Name</th>
                                <th class="min-tablet">Provided Services</th>
                                <th class="desktop">Service Area</th>

                                <th class="all">***</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($in_house_engineers_list as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->engineer_name}}</td>
                                <td>{{$item->expertise_on_service}}</td>
                                <td>{{$item->engineer_location}}</td>


                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Action
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-left" role="menu">
                                            <li>
                                                <a style="border: none;" href="{{route('engineers.show', $item->id)}}">
                                                    <span class="item">
                                                        <span aria-hidden="true" class="icon-user"></span> Profile</a>
                                            </li>

                                            <li>
                                                <a style="border: none;" href=""><i class="fa fa-trash"></i> Remove</a>
                                            </li>

                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>

</div>

<!-- END CONTENT BODY -->
@endsection