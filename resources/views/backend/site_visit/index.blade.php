@extends('backend.layouts.app')


@section('title', 'Site visit')


@section('js')


@endsection
@section('css')
<style>

</style>
@endsection


@section('content')

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('backend.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i> Site Visit Request List </div>
                    <!-- <div style="color:black" class="tools"> <a style="color:black;" href="{{route('photos.create')}}"><i class="fa fa-plus" style="color:black "></i> Add New Image/Banner
                        </a></div> -->
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_3" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="all">#</th>
                                <th class="min-phone-l" style="text-align: center;">Name</th>
                                <th class="min-phone-l">Contact Number</th>
                                <th class="min-tablet">Visit For</th>
                                <th class="all">***</th>
                            </tr>
                        </thead>
                        <tbody>
                          @foreach($site_visit_info as $visit_info)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{$visit_info->name}}</td>
                                    <td>{{$visit_info->mobile}}</td>
                                    <td>{{$visit_info->visit_for}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Action
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-left" role="menu">
                                                <li>
                                                    <a style="border: none;" onclick="delete_f({{$visit_info->id}});" ><i class="fa fa-trash"></i> Remove</a>
                                                </li>

                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>

</div>
<script>
    function delete_f(id){
        swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this information!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
                .then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                
                type:"GET",
                url:"{{url('/site_visit_delete')}}/"+id,
                success: function(data) {
                    swal("Poof! Your data has been deleted!", {
                    icon: "success",
                    });  
                    location.reload();
                }
                });

                } else {
                    swal("Your data file is safe!");
                }
                });
            }


</script>
<!-- END CONTENT BODY -->
@endsection
