@extends('backend.layouts.app')

@section('title', 'Uploaded Image List')


@section('js')


@endsection
@section('css')
<style>

</style>
@endsection


@section('content')

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('backend.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i> Uploaded Image List </div>
                    <div style="color:black" class="tools"> <a style="color:black;" href="{{route('photos.create')}}"><i class="fa fa-plus" style="color:black "></i> Add New Image/Banner
                        </a></div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_3" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="all">#</th>
                                <th class="min-phone-l" style="text-align: center;">Image</th>
                                <th class="min-phone-l">Image Title</th>
                                <th class="min-tablet">Image Type</th>
                                <th class="desktop">Image Status</th>

                                <th class="all">***</th>
                            </tr>
                        </thead>
                        <tbody>

                          @foreach($photo_info as $ph_info)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td style="text-align: center;"><img style="height:100px;width:100px;" src="{{$ph_info->image_path}}"></td>
                                    <td>{{$ph_info->image_title}}</td>
                                    <td>{{$ph_info->image_type}}</td>
                                    <td><?php if($ph_info->image_type ==1){echo "Active";}else{echo "Inactive";} ?></td>


                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Action
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-left" role="menu">
                                                <li>
                                                    <a style="border: none;" href="{{route('photos.edit', $ph_info->id)}}">
                                                        <span class="item">
                                                            <span aria-hidden="true" class="icon-note icons"></span> Edit</a>
                                                </li>

                                                <li>
                                                    <a style="border: none;" href="{{route('deletePhoto', $ph_info->id)}}"><i class="fa fa-trash"></i> Remove</a>
                                                </li>

                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>

</div>

<!-- END CONTENT BODY -->
@endsection
