@extends('backend.layouts.app')

@section('title', 'Uploaded Image')


@section('js')


@endsection
@section('css')
<style>

</style>
@endsection


@section('content')

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('backend.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">
            <div class="col-md-12" style="margin-top:20px">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>Image Upload Form
                        </div>
                        <div class="actions">
                            <div class="btn-set pull-right">
                            <a href="{{route('photos.index')}}"><button type="button" class="btn red">List</button></a>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body form">


                        <!-- BEGIN FORM-->
                        <form action="{{route('photos.index')}}" method="post" class="form-horizontal"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form-body">



                            <div class="form-group">
                                        <label class="col-md-3 control-label">Image Title :</label>
                                        <div class="col-md-4">
                                            <input name="image_title" type="text" class="form-control" value="{{ old('image_title') }}"
                                                placeholder="write image title">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Meta Keywords :</label>
                                        <div class="col-md-4">
                                            <input name="image_meta_keywords" type="text" value="{{ old('image_meta_keywords') }}" class="form-control"
                                                placeholder="write image meta keywords">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Image Type :</label>
                                        <div class="col-md-4">
                                            <select id="single" name="image_type" class="form-control">
                                                <option value="">Select Type</option>
                                                <option value="1" class="active">Type 1</option>
                                                <option value="2">Type 2</option>

                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Image File :</label>
                                        <div class="col-md-4">
                                            <input name="image_file" type="file" class="form-control"
                                                placeholder="Image File Select">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Description :</label>
                                        <div class="col-md-6">
                                        <textarea name="image_meta_description" id="image_meta_description"  class="form-control"
                                                placeholder="Description"></textarea>
                                        </div>
                                    </div> 

                            </div>

                            <div class="form-actions top">
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-8">
                                        <button type="submit" class="btn green">Submit</button>
                                        <button type="button" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>


</div>
    <!-- END CONTENT BODY -->
@endsection