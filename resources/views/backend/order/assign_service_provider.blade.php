@extends('backend.layouts.app')

@section('title', 'Assign Provider Form')


@section('js')



<script>



</script>


@endsection


@section('css')

<style>
    #ssd {
        resize: none;
    }
</style>

@endsection



@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('backend.include.beginPageHeader')

    <!-- END PAGE HEADER-->

    <div class="row">

        <div class="col-md-12">
            <div class="tabbable-line boxless tabbable-reversed">


                <div class="tab-content">
                    <div class="tab-pane active" id="tab_0">


                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class=""></i>Service Provider/In-house Engineers
                                </div>

                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->

                                <form action="{{route('set.assign.provider')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    @csrf

                                    <div class="form-body">

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Service Category</label>
                                            <div class="col-md-6">

                                                <input  value="{{$order_details->id}}" name="order_details_id" type="hidden">
                                                <input class="form-control" value="{{$order_details->category->name}}" name="" rows="3" placeholder="" readonly>

                                                {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Service Area</label>
                                            <div class="col-md-6">

                                                <input class="form-control" value="@php
                                                $address_now = json_decode($address_info, true);
                                                foreach($address_now as $ad){
                                                echo $ad['address'];
                                                }
                                                @endphp
                                                " name="" rows="3" placeholder="" readonly>

                                                {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Unit</label>
                                            <div class="col-md-6">

                                                <input class="form-control" value="" name="" rows="3" placeholder="" readonly>

                                                {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Service Provider</label>
                                            <div class="col-md-6">
                                                <div class="input-group select2-bootstrap-append">
                                                    <select name="provider[]" id="multi-append" class="form-control select2" multiple>
                                                        <option></option>
                                                        @foreach($service_providers as $service_provider)
                                                        <option value="{{$service_provider->id}}">{{$service_provider->service_provider_name}}</option>
                                                        @endforeach
                                                    </select>

                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" type="button" data-select2-open="multi-append">
                                                            <i class="fa fa-arrow-down"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                                {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Note to Provider</label>
                                            <div class="col-md-6">

                                                <textarea class="form-control" value="" name="notes" id="ssd" rows="3" placeholder=""></textarea>

                                                {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>


                                    </div>


                                    <div class="form-actions top">
                                        <div class="row">
                                            <div class="col-md-offset-4 col-md-8">
                                                <button type="submit" class="btn green">Assign</button>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

@endsection