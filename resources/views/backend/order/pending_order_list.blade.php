@extends('backend.layouts.app')

@section('title', 'Pending List')


@section('js')


@endsection
@section('css')
<style>

</style>
@endsection


@section('content')

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('backend.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-globe"></i> Pending List</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_3" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="all">#Id</th>
                                <th class="min-phone-l">Order Code</th>
                                <th class="min-tablet">Service Category</th>
                                <th class="desktop">Unit</th>
                                <th class="none">Amount</th>
                                <th class="none">Customer Area</th>
                                <th class="min-phone-l">***</th>

                            </tr>
                        </thead>
                        <tbody>
                        @foreach($pending_order as $order)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{$order->order->order_code}}</td>
                                <td>{{$order->category->name}}</td>
                                <td></td>
                                <td>{{$order->price}}</td>
                                <td>
                                    <?php 
                                    $address=\App\Models\Order::where(['id' => $order->order_id])->pluck('service_address')->first();
                                    if(!empty($address)){
                                    $address_now = json_decode($address, true);
                                    foreach($address_now as $ad){
                                        echo $ad["address"];
                                        }
                                    }
                                    ?>                                  
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Action
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-left" role="menu">
                                            <li>
                                                <a style="border: none;" href="{{route('assign.provider',$order->id)}}">
                                                    <i class="fa fa-edit"></i> Assign Service Provider</a>
                                            </li>

                                            <li>
                                                <a style="border: none;" href="{{route('assign.provider',$order->id)}}">
                                                    <i class="fa fa-edit"></i> Assign In-house Engineers</a>
                                            </li>

                                            <li>

                                                <a style="border: none;" onclick="return confirm('Are you sure you want to cancel this order?');" href="{{route('cancel.order',$order->id)}}"><i class="fa fa-close"></i>Cancel</a>
                                            </li>

                                        </ul>
                                    </div>
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>

</div>

<!-- END CONTENT BODY -->
@endsection