@extends('backend.layouts.app')

@section('title', 'Order Details')


@section('js')


@endsection
@section('css')
<link href="/admin/invoice/css/invoice.css" rel="stylesheet">

<style>

</style>
@endsection


@section('content')

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('backend.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">

        <div class="container-fluid invoice-container">

            <div class="row invoice-header">
                <div class="invoice-col">

                    <p><img src="/admin/invoice/images/amar-lpg-logo.png" title="dactarbari - Billing Dept." /></p>

                </div>

                <div class="col-md-8">
                    <address class="small-text">
                        <span style="font-size:16px;font-weight:600;text-align:center"> Order Details </span>
                    </address>
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="invoice-col right">
                    <strong>Invoiced To</strong>
                    <address class="small-text">
                        <span><strong>Service Provider: </strong></span><span class="small-text">Jamal Uddin, Aslam Hossain</span><br>
                        <strong>Service Time : </strong><span class="small-text">বুধবার, ১০ ফেব্রুয়ারী, ২০২১. 5:30 PM </span>
                    </address>
                </div>
                <div class="invoice-col">
                    <strong>Service Area:</strong>
                    <address class="small-text">
                        <span>Hassan Plaza (5th Floor)<br />
                            53 Karwan Bazar C/A</span><br>
                        <span><strong>Customer Name:</strong></span>
                        <span>Nahid Islam</span><br>
                        <span><strong>Customer Id.:</strong></span>
                        <span>#214</span><br>
                    </address>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Invoice Item</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-condensed">
                                    <thead>
                                        <tr>
                                            <td class="text-center"><strong>Order-Code</strong></td>
                                            <td class="text-center"><strong>Service category</strong></td>

                                            <td class="text-center"><strong>Qty</strong></td>
                                            <td class="text-center"><strong></strong></td>
                                            <td class="text-center"><strong></strong></td>
                                            <td class="text-center"><strong>Service Charge</strong></td>
                                            <td class="text-center"><strong>Vat</strong></td>
                                            <td class="text-right"><strong>Total Amount</strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>


                                        <tr>
                                            <td class="text-center">00145</td>
                                            <td class="text-center">Ac Jet wash</td>

                                            <td class="text-center">5</td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center">500</td>
                                            <td class="text-center">0%</td>
                                            <td class="text-right">4500</td>
                                        </tr>


                                        <tr>
                                            <td class="thick-line"></td>
                                            <td class="thick-line"></td>
                                            <td class="thick-line"></td>
                                            <td class="thick-line"></td>
                                            <td class="thick-line"></td>
                                            <td class="thick-line"></td>
                                            <td class="thick-line"></td>
                                            <td class="thick-line"></td>
                                        </tr>

                                        <tr>
                                            <td class="no-line"></td>
                                            <td class="no-line"></td>
                                            <td class="no-line"></td>
                                            <td class="no-line"></td>
                                            <td class="no-line"></td>
                                            <td class="no-line"></td>
                                            <td class="no-line text-center"><strong>Total:</strong></td>
                                            <td class="no-line text-right">54200 tk</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <p class="text-center"><strong>NOTE :</strong> This is computer generated receipt and does not require physical signature.</p>
            </div>

            <div class="pull-right btn-group btn-group-sm hidden-print">
                <a href="javascript:window.print()" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                {{--<a href="#" class="btn btn-default"><i class="fas fa-download"></i> Download</a>--}}
            </div>

            <div>
                <p class="text-center">THANK YOU BEING WITH US</p>
            </div>
        </div>


    </div>


</div>

@endsection