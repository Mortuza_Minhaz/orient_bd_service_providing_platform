@extends('backend.layouts.app')

@section('title', 'Assigned Service Order List')


@section('js')


@endsection
@section('css')
<style>

</style>
@endsection


@section('content')

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('backend.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-globe"></i> Assigned Service Order List</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_3" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="all">#</th>
                                <th class="none">Order Code</th>
                                <th class="min-tablet">Service Category</th>
                                <th class="desktop">Unit</th>
                                <th class="min-phone-l">Assigned Providers</th>
                                <th class="none">Customer Area</th>
                                <th class="min-phone-l">***</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>20210127-13325090</td>
                                <td>Ac Jet Wash</td>
                                <td>1.5 Ton</td>
                                <td>Jamal Uddin, Asad Alam</td>
                                <td>Dhanmondi</td>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Action
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-left" role="menu">
                                            <li>
                                                <a style="border: none;" href="{{route('order_details')}}">
                                                    <i class="fa fa-eye"></i> view</a>
                                            </li>

                                            <li>

                                                <a style="border: none;" href=""><i class="fa fa-close"></i>Cancel</a>
                                            </li>

                                        </ul>
                                    </div>
                                </td>

                            </tr>


                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>

</div>

<!-- END CONTENT BODY -->
@endsection