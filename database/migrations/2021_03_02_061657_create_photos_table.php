<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->id();
            $table->string('image_title')->nullable();
            $table->tinyInteger('image_type')->nullable();
            $table->tinyInteger('image_status')->default(1)->nullable();
            $table->string('image')->nullable();
            $table->string('image_path')->nullable();
            $table->string('image_meta_keywords')->nullable();
            $table->string('image_meta_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photos');
    }
}
