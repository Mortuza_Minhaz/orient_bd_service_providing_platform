<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->longText('service_address');
            $table->string('payment_type')->default('unpaid');
            $table->string('payment_status')->default('pendding');
            $table->longText('payment_details')->nullable();
            $table->double('grand_total',20,2)->nullable();
            $table->double('coupon_discount',20,2)->default('0');
            $table->mediumText('code')->nullable();
            $table->string('date');
            $table->string('schedule_time')->nullable();
            $table->string('schedule_date')->nullable();
            $table->string('contact_name')->nullable();
            $table->integer('contact_number')->nullable();
            $table->integer('viewed')->default('0');
            $table->integer('delivery_viewed')->default('1');
            $table->integer('payment_status_viewed')->default('1');
            $table->integer('commission_calculated')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
