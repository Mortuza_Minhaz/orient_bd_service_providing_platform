<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('added_by')->default('admin');
            $table->string('name');
            $table->integer('category_id')->nullable();
            $table->integer('brand_id')->nullable();
            $table->string('model')->nullable();
            $table->integer('capacity_id')->nullable();
            $table->string('tag')->nullable();
            $table->string('photos')->nullable();
            $table->string('unit')->nullable();
            $table->string('short_description')->nullable();
            $table->string('service_charge')->nullable();
            $table->string('investigation_charge')->nullable();
            $table->string('commission')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('created_by')->nullable();

            $table->string('slug')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
