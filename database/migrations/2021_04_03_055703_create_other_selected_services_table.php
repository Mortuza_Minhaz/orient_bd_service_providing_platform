<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtherSelectedServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_selected_services', function (Blueprint $table) {
            $table->id();
            $table->integer('provider_id')->nullable();
            $table->string('other_service_list_title')->nullable();
            $table->integer('other_investigation_charge')->nullable();
            $table->integer('other_repairing_charge')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_selected_services');
    }
}
