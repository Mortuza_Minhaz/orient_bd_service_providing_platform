<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_providers', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('company_or_individual')->nullable();
            $table->string('service_district')->nullable();
            $table->string('service_location')->nullable();
            $table->longText('service_provider_description')->nullable();
            $table->string('service_provider_details_address')->nullable();
            $table->string('service_provider_company_name')->nullable();
            $table->string('service_provider_company_manpower')->nullable();
            $table->string('individual_provider_cv')->nullable();
            $table->string('individual_provider_nid')->nullable();
            $table->string('individual_provider_tin')->nullable();
            $table->string('individual_provider_testimonial')->nullable();
            $table->string('individual_provider_experience')->nullable();
            $table->string('individual_provider_noc')->nullable();
            $table->string('individual_provider_tender')->nullable();
            $table->string('company_profile_details')->nullable();
            $table->string('company_nid')->nullable();
            $table->string('company_business_license')->nullable();
            $table->string('company_tin')->nullable();
            $table->string('company_contract_paper')->nullable();
            $table->string('company_tender')->nullable();
            $table->integer('is_verified_service_provider')->default(0)->comment('auth_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_providers');
    }
}
