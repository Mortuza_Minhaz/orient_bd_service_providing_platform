<?php


use App\Http\Controllers\HomeController;
use App\Http\Controllers\OrderController;

use Illuminate\Support\Facades\Artisan;

use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\AddressController;


use App\Http\Controllers\EngineerController;
use App\Http\Controllers\MinhazController;
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\ServiceProviderController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\WarrantyController;
use App\Http\Controllers\RentalController;
use App\Http\Controllers\SiteVisitController;
use App\Http\Controllers\CouponController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\ServiceListController;
use App\Http\Controllers\BrandController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//








Route::get('/altest', [HomeController::class, 'altest'])->name('altest')->middleware(['auth']);

Route::group(['middleware' => ['is-admin']], function () {



    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('products', ProductController::class);
    Route::resource('engineers', EngineerController::class);
    Route::resource('photos', PhotoController::class);
    Route::get('photos/{photo}', [PhotoController::class, 'delete'])->name('deletePhoto');
    Route::resource('customer', CustomerController::class);
    Route::resource('addresses', AddressController::class);
    Route::resource('coupon', CouponController::class);
    Route::resource('categories', CategoryController::class);
    Route::resource('services', ServiceListController::class);
    Route::resource('brands', BrandController::class);
    Route::get('services/{service}', [ServiceListController::class, 'delete'])->name('delete_service_list');
    Route::get('brands/{brand}', [BrandController::class, 'delete'])->name('delete_brand_list');

    Route::get('orders/pending-list', [OrderController::class, 'pendingList'])->name('pending_order_list');
    Route::get('orders/assigned-list', [OrderController::class, 'assignedList'])->name('assigned_order_list');

    Route::get('orders/assign-provider/{id?}', [OrderController::class, 'assignServiceProvider'])->name('assign.provider');
    Route::post('orders/set-assign-provider', [OrderController::class, 'setAssignServiceProvider'])->name('set.assign.provider');
    Route::get('orders/details', [OrderController::class, 'orderDetails'])->name('order_details');
    Route::get('/cancel/order/{id?}', [OrderController::class, 'cancelOrder'])->name('cancel.order');


    Route::get('users', [UserController::class, 'index'])->name('users.index');

    Route::get('photos/{photo}', [PhotoController::class, 'delete'])->name('deletePhoto');

    Route::any('service-providers/list', [ServiceProviderController::class, 'index'])->name('service_provider_list_admin');
    Route::get('service-providers/create', [ServiceProviderController::class, 'create'])->name('service_provider_create_admin');
    Route::post('service-providers/store', [ServiceProviderController::class, 'store'])->name('service_provider_store_admin');
    Route::get('service-providers/profile/{id}', [ServiceProviderController::class, 'show'])->name('service_provider_profile');
    Route::get('/service-providers/details/{id}', [ServiceProviderController::class, 'details'])->name('service_provider_details');
    Route::get('service-providers/{id}', [ServiceProviderController::class, 'destroy'])->name('deleteProvider');

    //warranty info get start
    Route::get('/warranty_show', [WarrantyController::class, 'show'])->name('warranty.show');
    Route::get('/warranty_delete/{id}', [WarrantyController::class, 'delete'])->name('warranty.delete');
    //warranty info get end

    //rental info get start
    Route::get('/rental_show', [RentalController::class, 'show'])->name('rental.show');
    Route::get('/rental_delete/{id}', [RentalController::class, 'delete'])->name('rental.delete');
    //rental info get end

    //site visit info get start
    Route::get('/site_visit_show', [SiteVisitController::class, 'show'])->name('site.visit.show');
    Route::get('/site_visit_delete/{id}', [SiteVisitController::class, 'delete'])->name('site.visit.delete');
    //site visit info get end

    Route::any('/addresses/{address}/delete', [AddressController::class, 'destroy'])->name('addresses_destroy');
    Route::get('/addresses/set_default/{id}', [AddressController::class, 'set_default'])->name('addresses.set_default');

    //Coupons
    Route::post('/coupon/get_form', [CouponController::class, 'get_coupon_form'])->name('coupon.get_coupon_form');
    Route::post('/coupon/{id}/get_form_edit', [CouponController::class, 'get_coupon_form_edit'])->name('get_coupon_form_edit');
    Route::get('/coupon/{coupon}/delete', [CouponController::class, 'destroy'])->name('coupon_destroy');
    //search
    Route::get('/search', [SearchController::class, 'search'])->name('search');
    Route::get('/search?q={search}', [SearchController::class, 'search'])->name('suggestion.search');

    //Cart or Checkout route start
    Route::match(['GET', 'POST'], '/checkout', [HomeController::class, 'checkout'])->name('checkout');
    Route::get('/search', [SearchController::class, 'search'])->name('search');
    Route::get('/search?q={search}', [SearchController::class, 'search'])->name('suggestion.search');

    Route::get('/sub-categories', 'CategoryController@subCategory')->name('subCategory');
    //for popular category status change

});
