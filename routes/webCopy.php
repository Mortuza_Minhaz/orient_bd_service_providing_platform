<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Artisan;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\AddressController;


use App\Http\Controllers\EngineerController;
use App\Http\Controllers\MinhazController;
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\ServiceProviderController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\WarrantyController;
use App\Http\Controllers\RentalController;
use App\Http\Controllers\SiteVisitController;
use App\Http\Controllers\CouponController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ServiceListController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//

Route::get(
    '/clear',
    function () {
        Artisan::call('cache:clear');
        Artisan::call('config:clear');
        Artisan::call('route:clear');
        Artisan::call('view:clear');
    }
);

Route::get(
    '/setup',
    function () {
        Artisan::call('migrate');
    }
);

Route::get('/min', function () {
    Toastr::success('Categories our customers love to check out', 'Success', ["closeButton" => true, "debug" => false, "newestOnTop" => false, "progressBar" => true, "positionClass" => "toast-bottom-left", "preventDuplicates" => false, "onclick" => null, "showDuration" => "300", "hideDuration" => "1000", "timeOut" => "5000", "extendedTimeOut" => "1000", "showEasing" => "swing", "hideEasing" => "linear", "showMethod" => "fadeIn", "hideMethod" => "fadeOut"]);

    return redirect()->back();
});

Route::get('/', [HomeController::class, 'homeView'])->name('homeView');
Route::match(['GET', 'POST'], '/customer_login', [UserController::class, 'customer_login'])->name('customer_login');
Route::post('/customer_register', [UserController::class, 'customer_register'])->name('customer_register');
Route::get('/logout', [UserController::class, 'customer_logout'])->name('logout');;
Route::match(['GET', 'POST'], '/customer_login', [UserController::class, 'customer_login'])->name('customer_login');
Route::post('/customer_register', [UserController::class, 'customer_register'])->name('customer_register');
Route::get('/logout', [UserController::class, 'customer_logout'])->name('logout');
Route::get('/service/{serviceId}', [HomeController::class, 'service'])->name('service');
Route::post('/service_save', [HomeController::class, 'service_save'])->name('service_save');
Route::post('/addto_cart', [CartController::class, 'addToCart'])->name('addto_cart');
Route::any('/delete_cart', [CartController::class, 'delete_cart'])->name('delete_cart');
Route::post('/delete_item', [CartController::class, 'delete_item'])->name('delete_item');
Route::post('/rowId_delete', [CartController::class, 'rowId_delete'])->name('rowId_delete');
Route::post('/cart_update', [CartController::class, 'cart_update'])->name('cart_update');
Route::get('/cart_view', [CartController::class, 'cart_view'])->name('cart_view');

Auth::routes();

Route::get('/customers/login', [HomeController::class, 'customer_login'])->name('customer.login');
Route::get('/customers/registration', [HomeController::class, 'customer_registration'])->name('customer.registration');
Route::get('/service-provider-registration', [HomeController::class, 'service_provider_registration'])->name('service_provider.registration');
Route::get('/dashboard', [HomeController::class, 'user_dashboard'])->name('customer.dashboard');


Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('products', ProductController::class);
    Route::resource('engineers', EngineerController::class);
    Route::resource('photos', PhotoController::class);
    Route::resource('services', ServiceListController::class);
    Route::resource('customer', CustomerController::class);
    Route::resource('addresses', AddressController::class);
    Route::resource('coupon', CouponController::class);
    Route::resource('categories', CategoryController::class);
    Route::get('/home/view', [HomeController::class, 'homeView2'])->name('home_view');
});

Route::get('service-providers-create', [ServiceProviderController::class, 'serviceProviderCreate'])->name('service_provider_create');
Route::post('upazilas_name', [ServiceProviderController::class, 'upazilas_name'])->name('upazilas_name');
Route::post('service-providers', [ServiceProviderController::class, 'serviceProviderStore'])->name('service_provider_store');

// check coupons
Route::post('/coupon-check', [HomeController::class, 'couponCheck'])->name('coupon.check');

//Warranty route
Route::match(['GET', 'POST'], '/warranty', [WarrantyController::class, 'index'])->name('warranty');

//Rental route start
Route::match(['GET', 'POST'], '/rental', [RentalController::class, 'index'])->name('rental');

//Site Visit route start
Route::match(['GET', 'POST'], '/site_visit', [SiteVisitController::class, 'index'])->name('site.visit');


//Cart or Checkout route start
Route::match(['GET', 'POST'], '/checkout', [HomeController::class, 'checkout'])->name('checkout');


/* sobuj desgin start*/
Route::get('/product-details', function () {
    return view('frontend.product_details_page.product_details');
});
/*desgin end*/
/*desgin start*/
Route::get('/profile', function () {
    return view('frontend.customer.profile');
});
Route::get('/my_service', function () {
    return view('frontend.customer.my_service');
});

Route::get('/service_list', function () {
    return view('frontend.customer.service_list');
});
Route::get('/service_add', function () {
    return view('frontend.customer.service_add');
});

/* sobuj desgin end*/