<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Artisan;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\AddressController;
use App\Http\Controllers\EngineerController;
use App\Http\Controllers\MinhazController;
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\ServiceProviderController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\WarrantyController;
use App\Http\Controllers\RentalController;
use App\Http\Controllers\SiteVisitController;
use App\Http\Controllers\CouponController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ServiceListController;
use App\Http\Controllers\ServiceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//

Auth::routes();

Route::get(
    '/clear',
    function () {
        Artisan::call('cache:clear');
        Artisan::call('config:clear');
        Artisan::call('route:clear');
        Artisan::call('view:clear');

        Toastr::success('Config Cache Cleared Successfully', 'Success', ["closeButton" => true, "debug" => false, "newestOnTop" => false, "progressBar" => true, "positionClass" => "toast-bottom-left", "preventDuplicates" => false, "onclick" => null, "showDuration" => "300", "hideDuration" => "1000", "timeOut" => "5000", "extendedTimeOut" => "1000", "showEasing" => "swing", "hideEasing" => "linear", "showMethod" => "fadeIn", "hideMethod" => "fadeOut"]);

    return redirect()->back();
    }
);

Route::get(
    '/setup',
    function () {
        Artisan::call('migrate');
    }
);

Route::get('/min', function () {
    Toastr::success('Categories our customers love to check out', 'Success', ["closeButton" => true, "debug" => false, "newestOnTop" => false, "progressBar" => true, "positionClass" => "toast-bottom-left", "preventDuplicates" => false, "onclick" => null, "showDuration" => "300", "hideDuration" => "1000", "timeOut" => "5000", "extendedTimeOut" => "1000", "showEasing" => "swing", "hideEasing" => "linear", "showMethod" => "fadeIn", "hideMethod" => "fadeOut"]);

    return redirect()->back();
});

Route::get('/home', [HomeController::class, 'index'])->name('home')->middleware('is-admin');
Route::group(['middleware' => ['auth']], function () {

    //for popular category status change
    Route::get('/popular-category-status/{id}', [CategoryController::class, 'popularStatus'])->name('popular.category.status');


    Route::get('/home/view', [HomeController::class, 'homeView2'])->name('home_view');
});

Route::get('/', [HomeController::class, 'homeView'])->name('homeView');
Route::match(['GET', 'POST'], '/customer_login', [UserController::class, 'customer_login'])->name('customer_login');
Route::post('/customer_register', [UserController::class, 'customer_register'])->name('customer_register');
Route::get('/logout', [UserController::class, 'customer_logout'])->name('logout');
Route::match(['GET', 'POST'], '/customer_login', [UserController::class, 'customer_login'])->name('customer_login');

Route::get('/logout', [UserController::class, 'customer_logout'])->name('logout');

Route::get('/users/login', [HomeController::class, 'customer_login'])->name('customer.login');
Route::get('/users/registration', [HomeController::class, 'customer_registration'])->name('customer.registration');
Route::get('/service-provider-registration', [HomeController::class, 'service_provider_registration'])->name('service_provider.registration');

// all auth url
Route::group(['middleware' => ['auth']], function () {


});

Route::group(['middleware' => ['is-user']], function () {
    Route::get('/dashboard', [HomeController::class, 'user_dashboard'])->name('user.dashboard');
    Route::post('service-provider-registration', [UserController::class, 'service_provider_register'])->name('service_provider_register');

});

//provider


Route::group(['prefix' =>'provider', 'middleware' => ['is-user']], function(){
    Route::get('/service_add', [ServiceController::class, 'create'])->name('service_provider.add_service');
    Route::post('/service_add', [ServiceController::class, 'store'])->name('service_provider_service_store');
});



Route::get('/service-details/{slug}', [ServiceController::class, 'serviceDetails'])->name('service_details');

Route::get('/profile', [ServiceProviderController::class, 'serviceProviderProfileCreate'])->name('service_provider_create');
Route::post('upazilas_name', [ServiceProviderController::class, 'upazilas_name'])->name('upazilas_name');
Route::post('service-providers', [ServiceProviderController::class, 'serviceProviderStore'])->name('service_provider_profile_store');

 Route::get('/my_service', [HomeController::class, 'provider_services_list'])->name('service_provider.services');

    Route::get('/orders', [HomeController::class, 'service_provider_orders'])->name('service_provider.orders');

// check coupons
Route::post('/coupon-check', [HomeController::class, 'couponCheck'])->name('coupon.check');

//Warranty route
Route::match(['GET', 'POST'], '/warranty', [WarrantyController::class, 'index'])->name('warranty');

//Rental route start
Route::match(['GET', 'POST'], '/rental', [RentalController::class, 'index'])->name('rental');

//Site Visit route start
Route::match(['GET', 'POST'], '/site_visit', [SiteVisitController::class, 'index'])->name('site.visit');


//Cart or Checkout route start
Route::match(['GET', 'POST'], '/checkout', [HomeController::class, 'checkout'])->name('checkout');
Route::get('/search', [SearchController::class, 'search'])->name('search');
Route::get('/search?q={search}', [SearchController::class, 'search'])->name('suggestion.search');



/*desgin end*/
/*desgin start*/

Route::get('/service_list', function () {
    return view('frontend.customer.service_list');
});
Route::get('checkout-summery', [ServiceProviderController::class, 'checkout'])->name('checkout_summery');
Route::get('/cart', function () {
    return view('frontend.cart');
})->name('view_cart');
/* sobuj desgin end*/
