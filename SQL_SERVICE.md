INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2021-08-31 01:18:54', '2021-08-31 01:18:54'),
(2, 'inhouse', 'web', '2021-08-31 01:24:53', '2021-08-31 01:24:53'),
(3, 'outside', 'web', '2021-08-31 01:25:30', '2021-08-31 01:25:30'),
(4, 'customer', 'web', '2021-08-31 01:25:50', '2021-08-31 01:25:50');

......................................................
27.09.2021
ALTER TABLE `users` ADD `user_type` VARCHAR(255) NULL DEFAULT NULL AFTER `name`;
